package ru.citeck.ecos.commons.json

import ecos.com.fasterxml.jackson210.databind.JavaType
import ecos.com.fasterxml.jackson210.databind.JsonNode
import ecos.com.fasterxml.jackson210.databind.node.ArrayNode
import ecos.com.fasterxml.jackson210.databind.node.ObjectNode
import ecos.com.fasterxml.jackson210.databind.type.TypeFactory
import ru.citeck.ecos.commons.io.file.EcosFile
import java.io.*
import java.lang.reflect.Type

interface JsonMapper {

    fun applyData(to: Any?, from: Any?)

    fun <T : Any> read(reader: Reader?, type: Class<T>): T?

    fun <T : Any> read(file: File?, type: Class<T>): T?

    fun <T : Any> read(file: EcosFile?, type: Class<T>): T?

    fun <T : Any> read(inputStream: InputStream?, type: Class<T>): T?

    fun <T : Any> read(json: ByteArray?, type: Class<T>): T?

    fun <T : Any> read(value: String?, type: Class<T>): T?

    fun <T : Any> readNotNull(value: String, type: Class<T>): T

    fun <T : Any> readList(value: String?, elementType: Class<T>): List<T>

    fun <K : Any, V : Any> readMap(value: String?, keyType: Class<K>, valueType: Class<V>): Map<K, V>

    fun <T : Any> read(value: String?, type: Class<T>, deflt: T?): T?

    fun <T : Any> read(value: String?, type: JavaType, deflt: T?): T?

    fun read(json: String?): JsonNode?

    fun write(resultFile: File, value: Any)

    fun write(out: OutputStream, value: Any)

    fun write(out: DataOutput, value: Any)

    fun write(w: Writer, value: Any)

    fun <T : Any> convert(value: Any?, type: Class<T>): T?

    fun <T : Any> convert(value: Any?, type: Class<T>, deflt: T?): T?

    fun <T : Any> convert(value: Any?, type: JavaType): T?

    fun <T : Any> convert(value: Any?, type: JavaType, deflt: T?): T?

    fun <T : Any> copy(value: T?): T?

    fun toBytes(value: Any?): ByteArray?

    fun toString(value: Any?): String?

    fun toPrettyString(value: Any?): String?

    fun toJava(node: Any?): Any?

    fun toJson(value: Any?): JsonNode

    fun toNonDefaultJson(value: Any?): JsonNode

    fun isEquals(value0: Any?, value1: Any?): Boolean

    fun newObjectNode(): ObjectNode

    fun newArrayNode(): ArrayNode

    fun getType(type: Type): JavaType

    fun getListType(elementType: Class<*>): JavaType

    fun getSetType(elementType: Class<*>): JavaType

    fun getMapType(keyType: Class<*>, valueType: Class<*>): JavaType

    fun getTypeFactory(): TypeFactory
}
