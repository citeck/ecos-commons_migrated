package ru.citeck.ecos.commons.json.path

interface JsonPaths {

    fun forData(data: Any?): DataContext

    fun isDefinite(path: String): Boolean
}
