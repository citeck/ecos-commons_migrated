package ru.citeck.ecos.commons.utils

object FileUtils {

    @JvmStatic
    fun getValidName(name: String): String {
        val extension = name.substringAfterLast(".", "")
        val filename = name.substringBeforeLast(".", name)
        return getValidName(filename, extension)
    }

    @JvmStatic
    fun getValidName(name: String, extension: String): String {
        var result = name.replace("[^a-zA-Z0-9\\-_.]".toRegex(), "_")
        if (!extension.isEmpty()) {
            result += ".$extension"
        }
        return result
    }
}
