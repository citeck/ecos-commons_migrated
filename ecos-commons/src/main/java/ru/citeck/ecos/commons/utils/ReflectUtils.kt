package ru.citeck.ecos.commons.utils

import ecos.com.fasterxml.jackson210.databind.type.TypeFactory
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.commons.utils.MandatoryParam.check
import java.lang.reflect.Type
import java.util.stream.Collectors

object ReflectUtils {

    @JvmStatic
    fun getGenericArg(type: Class<*>, genericType: Class<*>): Class<*>? {
        val args = getGenericArgs(type, genericType)
        return if (args.isNotEmpty()) {
            args[0]
        } else {
            null
        }
    }

    @JvmStatic
    fun getGenericArgs(type: Class<*>, genericType: Class<*>): List<Class<*>> {
        return getGenericTypeArgs(type, genericType)
            .stream()
            .map { t: Type? -> TypeFactory.rawClass(t) }
            .collect(Collectors.toList())
    }

    private fun getGenericTypeArgs(type: Class<*>, genericType: Class<*>): List<Type> {

        check("type", type)
        check("genericType", genericType)

        return if (type == genericType) {
            emptyList()
        } else {
            getGenericArguments(type, genericType)
        }
    }

    private fun getGenericArguments(type: Type?, genericType: Class<*>): List<Type> {

        if (type == null) {
            return emptyList()
        }

        return Json.mapper.getType(type).findTypeParameters(genericType).toList()
    }
}
