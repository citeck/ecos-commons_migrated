package ru.citeck.ecos.commons.data

import ecos.com.fasterxml.jackson210.annotation.JsonCreator
import ecos.com.fasterxml.jackson210.annotation.JsonValue
import ru.citeck.ecos.commons.json.Json
import java.io.Serializable
import java.util.*
import kotlin.collections.HashMap

class MLText : Serializable {

    companion object {

        private const val serialVersionUID = 1L

        @JvmField
        val EMPTY = MLText()
        @JvmField
        val DEFAULT_LOCALE: Locale = Locale.ENGLISH

        @JvmStatic
        @JvmOverloads
        fun getClosestValue(text: MLText?, locale: Locale?, dflt: String = ""): String {
            text ?: return dflt
            val result = text.getClosestValue(locale ?: DEFAULT_LOCALE)
            return if (result.isBlank()) {
                dflt
            } else {
                result
            }
        }

        @JvmStatic
        fun isEmpty(text: MLText?): Boolean {
            return text == null || text.values.isEmpty()
        }

        @JvmStatic
        @Deprecated(message = "MLText is immutable")
        fun copy(text: MLText?): MLText? {
            text ?: return null
            return MLText(text.values)
        }

        @JvmStatic
        @Deprecated(message = "MLText is immutable")
        fun copyOrDefault(text: MLText?): MLText {
            text ?: return EMPTY
            return MLText(text.values)
        }
    }

    /**
     * String values of same text in different locales.
     */
    private val values: Map<Locale, String>

    constructor() {
        values = emptyMap()
    }

    /**
     * Store text with EN locale by default.
     * Note: Jackson use this constructor for mapping simple string to our object.
     *
     * @param content value for default locale
     */
    @JsonCreator
    @com.fasterxml.jackson.annotation.JsonCreator
    constructor(content: String) {
        values = if (content.isBlank()) {
            emptyMap()
        } else {
            mapOf(DEFAULT_LOCALE to content)
        }
    }

    constructor(pair: Pair<Locale, String?>) {
        val value = pair.second
        values = if (value.isNullOrBlank()) {
            emptyMap()
        } else {
            mapOf(pair.first to value)
        }
    }

    constructor(vararg pairs: Pair<Locale, String?>) : this(mapOf(*pairs))

    /**
     * Note: Jackson use this constructor for mapping {key:value} strings to our object.
     */
    @JsonCreator
    @com.fasterxml.jackson.annotation.JsonCreator
    constructor(values: Map<Locale, String?>) {
        val result = HashMap<Locale, String>()
        values.forEach { (k, v) ->
            if (!v.isNullOrBlank()) {
                result[k] = v
            }
        }
        this.values = result
    }

    @JsonValue
    @com.fasterxml.jackson.annotation.JsonValue
    fun getValues(): Map<Locale, String> {
        return values
    }

    fun toMutableMap(): MutableMap<Locale, String> {
        return HashMap(values)
    }

    fun has(locale: Locale?): Boolean {
        locale ?: return false
        return values.containsKey(locale)
    }

    fun withValue(locale: Locale, value: String): MLText {
        val map = toMutableMap()
        map[locale] = value
        return MLText(map)
    }

    /**
     * Get value in default locale.
     */
    @JvmOverloads
    fun get(locale: Locale? = DEFAULT_LOCALE): String {
        return values[locale ?: DEFAULT_LOCALE] ?: ""
    }

    /**
     * Alias for getClosestValue
     */
    @JvmOverloads
    fun getClosest(locale: Locale? = DEFAULT_LOCALE): String {
        return getClosestValue(locale)
    }

    @JvmOverloads
    fun getClosestValue(locale: Locale? = DEFAULT_LOCALE): String {
        var value = values[locale ?: DEFAULT_LOCALE]
        if (value.isNullOrBlank() && locale != DEFAULT_LOCALE) {
            value = values[DEFAULT_LOCALE]
        }
        if (value.isNullOrBlank()) {
            value = values.values.firstOrNull() ?: ""
        }
        return value
    }

    override fun toString(): String {
        return Json.mapper.toString(values)!!
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (javaClass != other?.javaClass) {
            return false
        }
        other as MLText
        return values == other.values
    }

    override fun hashCode(): Int {
        return values.hashCode()
    }
}
