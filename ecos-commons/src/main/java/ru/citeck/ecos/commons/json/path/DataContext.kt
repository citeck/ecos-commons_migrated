package ru.citeck.ecos.commons.json.path

import ecos.com.jayway.jsonpath.JsonPath
import ru.citeck.ecos.commons.data.DataValue

interface DataContext {

    fun get(path: JsonPath): DataValue

    fun get(path: String): DataValue

    fun getPaths(path: String): List<String>

    fun add(path: String, element: Any?)

    fun addAll(path: String, elements: Iterable<Any>)

    fun put(path: String, key: String, value: Any?)

    fun renameKey(path: String, oldKey: String, newKey: String)

    fun set(path: String, value: Any?)

    fun set(path: JsonPath, value: Any?)

    fun delete(path: String)
}
