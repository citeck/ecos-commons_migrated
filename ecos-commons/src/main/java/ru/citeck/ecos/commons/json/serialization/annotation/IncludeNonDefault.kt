package ru.citeck.ecos.commons.json.serialization.annotation

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class IncludeNonDefault
