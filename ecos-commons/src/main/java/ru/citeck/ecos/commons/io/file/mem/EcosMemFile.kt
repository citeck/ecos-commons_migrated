package ru.citeck.ecos.commons.io.file.mem

import ru.citeck.ecos.commons.io.file.AbstractEcosFile
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.filter.FileFilter
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.lang.UnsupportedOperationException
import java.time.Instant

private fun writerToBytes(outWriter: (OutputStream) -> Unit): ByteArray {
    val stream = ByteArrayOutputStream()
    stream.use(outWriter)
    return stream.toByteArray()
}

class EcosMemFile(
    parent: EcosMemDir?,
    name: String,
    private val content: ByteArray
) : MemFileBase(parent, name) {

    private var lastModified: Long = Instant.now().toEpochMilli()
    private var createdTime: Long = lastModified
    private var lastAccessTime: Long = lastModified

    constructor(outWriter: (OutputStream) -> Unit) : this(null, "", writerToBytes(outWriter))

    constructor(other: EcosFile) : this(null, "", other.readAsBytes())

    override fun <T : Any> read(handler: (InputStream) -> T?): T? {
        val input = ByteArrayInputStream(content)
        return handler.invoke(input)
    }

    override fun createFileImpl(name: String, outWriter: (OutputStream) -> Unit): EcosFile {
        throw UnsupportedOperationException("Create file")
    }

    override fun createDirImpl(name: String): EcosMemDir {
        throw UnsupportedOperationException("Create dir")
    }

    override fun isDirectory(): Boolean = false
    override fun getChild(name: String): AbstractEcosFile? = null
    override fun getChildren(): List<EcosFile> = emptyList()
    override fun findFiles(filter: FileFilter, recursive: Boolean): List<EcosFile> = emptyList()

    override fun getLastAccessTime(): Long {
        return lastAccessTime
    }

    override fun setLastAccessTime(time: Long) {
        this.lastAccessTime = time
    }

    override fun getLastModifiedTime(): Long {
        return lastModified
    }

    override fun setLastModifiedTime(time: Long) {
        this.lastModified = time
    }

    override fun getCreationTime(): Long {
        return createdTime
    }

    override fun setCreationTime(time: Long) {
        this.createdTime = time
    }
}
