package ru.citeck.ecos.commons.json

import ecos.com.fasterxml.jackson210.databind.SerializationFeature

enum class SerialFeature(val feature: SerializationFeature) {
    WRAP_ROOT_VALUE(SerializationFeature.WRAP_ROOT_VALUE),
    INDENT_OUTPUT(SerializationFeature.INDENT_OUTPUT),
    CLOSE_CLOSEABLE(SerializationFeature.CLOSE_CLOSEABLE)
}
