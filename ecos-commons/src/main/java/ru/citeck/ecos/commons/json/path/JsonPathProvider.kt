package ru.citeck.ecos.commons.json.path

import ecos.com.fasterxml.jackson210.databind.JavaType
import ecos.com.jayway.jsonpath.Configuration
import ecos.com.jayway.jsonpath.TypeRef
import ecos.com.jayway.jsonpath.spi.json.JsonProvider
import ecos.com.jayway.jsonpath.spi.json.JsonProvider.UNDEFINED
import ecos.com.jayway.jsonpath.spi.mapper.MappingProvider
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.json.JsonMapper
import java.io.InputStream
import java.io.InputStreamReader
import java.nio.charset.Charset

class JsonPathProvider(
    val mapper: JsonMapper,
    private val createMapOnGet: Boolean = false
) : JsonProvider, MappingProvider {

    override fun setArrayIndex(array: Any?, idx: Int, newValue: Any?) {
        if (array !is DataValue) {
            return
        }
        if (array.size() == idx) {
            array.add(newValue)
        } else {
            array.set(idx, newValue)
        }
    }

    override fun length(obj: Any?): Int {
        if (obj !is DataValue) {
            return 0
        }
        return if (obj.isTextual()) {
            obj.asText().length
        } else {
            obj.size()
        }
    }

    override fun parse(json: String): Any {
        return mapper.read(json, DataValue::class.java) ?: DataValue.NULL
    }

    override fun parse(jsonStream: InputStream, charset: String): Any {
        val input = InputStreamReader(jsonStream, Charset.forName(charset))
        return mapper.read(input, DataValue::class.java) ?: DataValue.NULL
    }

    override fun setProperty(obj: Any?, key: Any?, value: Any?) {
        if (obj !is DataValue) {
            return
        }
        when (key) {
            is String -> obj.set(key, value)
            is Number -> obj.set(key.toInt(), value)
        }
    }

    override fun removeProperty(obj: Any?, key: Any) {
        if (obj !is DataValue) {
            return
        }
        when (key) {
            is String -> obj.remove(key)
            is Number -> obj.remove(key.toInt())
        }
    }

    override fun unwrap(obj: Any?): Any? {

        if (obj !is DataValue) {
            return obj
        }

        if (obj.isValueNode()) {
            return mapper.toJava(obj)
        }

        return obj
    }

    override fun getArrayIndex(obj: Any?, idx: Int): Any {
        return (obj as? DataValue)?.get(idx) ?: UNDEFINED
    }

    override fun getArrayIndex(obj: Any?, idx: Int, unwrap: Boolean): Any {
        return (obj as? DataValue)?.get(idx) ?: UNDEFINED
    }

    override fun getMapValue(obj: Any?, key: String?): Any? {
        if (key == null || obj !is DataValue) {
            return UNDEFINED
        }
        val result = obj.get(key)
        return if (result.isNull()) {
            if (createMapOnGet) {
                obj.set(key, DataValue.createObj())
                return obj.get(key)
            } else {
                UNDEFINED
            }
        } else {
            unwrap(result)
        }
    }

    override fun toIterable(obj: Any?): Iterable<*> {
        val result = ArrayList<Any?>()
        if (obj is DataValue) {
            for (value in obj) {
                result.add(unwrap(value))
            }
        }
        return result
    }

    override fun createArray(): Any {
        return DataValue.create(mapper.newArrayNode())
    }

    override fun toJson(obj: Any?): String {
        return mapper.toString(obj) ?: ""
    }

    override fun createMap(): Any {
        return DataValue.createObj()
    }

    override fun getPropertyKeys(obj: Any?): Collection<String> {
        return (obj as? DataValue)?.fieldNamesList() ?: emptyList()
    }

    override fun isMap(obj: Any?): Boolean {
        return obj is DataValue && obj.isObject() || obj is Map<*, *>
    }

    override fun isArray(obj: Any?): Boolean {
        return obj is DataValue && obj.isArray() || obj is List<*>
    }

    override fun <T : Any> map(source: Any?, targetType: Class<T>, configuration: Configuration?): T? {
        return mapper.convert(source, targetType)
    }

    override fun <T : Any> map(source: Any?, targetType: TypeRef<T>, configuration: Configuration?): T? {
        val javaType: JavaType = mapper.getType(targetType.type)
        return mapper.convert<T>(source, javaType, null)
    }
}
