package ru.citeck.ecos.commons.data

import ecos.com.fasterxml.jackson210.annotation.JsonCreator
import ecos.com.fasterxml.jackson210.annotation.JsonValue
import ecos.com.fasterxml.jackson210.databind.JsonNode
import ecos.com.fasterxml.jackson210.databind.node.*
import ecos.com.jayway.jsonpath.JsonPath
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.commons.json.Json.mapper
import ru.citeck.ecos.commons.json.path.DataContext
import ru.citeck.ecos.commons.utils.IterUtils
import java.lang.RuntimeException
import java.math.BigDecimal
import java.math.BigInteger
import java.util.function.BiConsumer

class DataValue private constructor(
    val value: JsonNode,
    private val unmodifiable: Boolean = false
) : Iterable<DataValue> {

    companion object {

        @JvmField
        val NULL = DataValue(NullNode.getInstance(), true)

        @JvmField
        val TRUE = DataValue(BooleanNode.TRUE, true)

        @JvmField
        val FALSE = DataValue(BooleanNode.FALSE, true)

        @JvmStatic
        @JsonCreator
        @com.fasterxml.jackson.annotation.JsonCreator
        fun create(content: Any?): DataValue {
            val jsonValue = mapper.toJson(content)
            return if (jsonValue.isNull || jsonValue.isMissingNode) {
                NULL
            } else {
                DataValue(jsonValue)
            }
        }

        @JvmStatic
        fun createObj(): DataValue {
            return create(mapper.newObjectNode())
        }

        @JvmStatic
        fun createArr(): DataValue {
            return create(mapper.newArrayNode())
        }

        @JvmStatic
        fun createStr(value: Any?): DataValue {
            if (value == null) {
                return NULL
            }
            val strValue = if (value is String) {
                value
            } else {
                value.toString()
            }
            return DataValue(TextNode.valueOf(strValue))
        }
    }

    private val jsonPathCtx: DataContext by lazy { Json.paths.forData(this) }

    fun remove(path: String): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        when {
            isJsonPath(path) -> jsonPathCtx.delete(path)
            else -> (this.value as? ObjectNode)?.remove(path)
        }
        return this
    }

    fun remove(idx: Int): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        (this.value as? ArrayNode)?.remove(idx)
        return this
    }

    fun fieldNames(): Iterator<String> {
        return value.fieldNames()
    }

    fun fieldNamesList(): List<String> {
        return IterUtils.toList(fieldNames())
    }

    fun has(fieldName: String): Boolean {
        return value.has(fieldName)
    }

    fun has(index: Int): Boolean {
        return value.has(index)
    }

    fun copy(): DataValue {
        return create(value.deepCopy())
    }

    fun isObject() = value.isObject

    fun isValueNode() = value.isValueNode

    fun isTextual() = value.isTextual

    fun isBoolean() = value.isBoolean

    fun isNotNull() = !isNull()

    fun isNull() = value.isNull || value.isMissingNode

    fun isBinary() = value.isBinary

    fun isPojo() = value.isPojo

    fun isNumber() = value.isNumber

    fun isIntegralNumber() = value.isIntegralNumber

    fun isFloatingPointNumber() = value.isFloatingPointNumber

    fun isShort() = value.isShort

    fun isInt() = value.isInt

    fun isLong() = value.isLong

    fun isFloat() = value.isFloat

    fun isDouble() = value.isDouble

    fun isBigDecimal() = value.isBigDecimal

    fun isBigInteger() = value.isBigInteger

    fun isArray() = value.isArray

    fun renameKey(path: String, oldKey: String, newKey: String): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        when {
            isJsonPath(path) -> jsonPathCtx.renameKey(path, oldKey, newKey)
            else -> {
                val map = get(path).value
                if (map is ObjectNode && map.has(oldKey)) {
                    val value = map.get(oldKey)
                    map.remove(oldKey)
                    map.set<JsonNode>(newKey, value)
                }
            }
        }
        return this
    }

    // ====== set =======

    fun set(path: String, key: String, value: Any?): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        when {
            isJsonPath(path) -> jsonPathCtx.put(path, key, value)
            else -> {
                val obj = get(path)
                if (obj.isObject()) {
                    obj.set(key, value)
                }
            }
        }
        return this
    }

    fun set(path: JsonPath, value: Any?): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        jsonPathCtx.set(path, value)
        return this
    }

    fun set(path: String, value: Any?): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        when {
            isJsonPath(path) -> jsonPathCtx.set(path, value)
            else -> {
                if (this.value is ObjectNode) {
                    this.value.set<JsonNode>(path, mapper.toJson(value))
                }
            }
        }
        return this
    }

    fun set(idx: Int, value: Any?): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        val fieldValue = this.value
        if (fieldValue is ArrayNode) {
            fieldValue.set(idx, mapper.toJson(value))
        }
        return this
    }

    // ====== /set =======
    // ===== set str =====

    fun setStr(path: String, value: Any?): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        return set(path, createStr(value))
    }

    fun setStr(idx: Int, value: Any?): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        return set(idx, createStr(value))
    }

    fun setStr(path: JsonPath, value: Any?): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        return set(path, createStr(value))
    }

    fun setStr(path: String, key: String, value: Any?): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        return set(path, key, createStr(value))
    }

    // ===== /set str =====
    // ======= get ========

    fun get(i: Int): DataValue {
        return DataValue(value[i])
    }

    fun <T : Any> get(field: String, type: Class<T>, orElse: T?): T? {
        return mapper.convert(get(field), type, orElse)
    }

    fun get(path: String): DataValue {
        val res = when {
            path[0] == '/' -> value.at(path)
            isJsonPath(path) -> jsonPathCtx.get(path)
            else -> value[path]
        }
        return create(res)
    }

    fun get(path: JsonPath): DataValue {
        return jsonPathCtx.get(path)
    }

    fun getFirst(path: String): DataValue {
        val value = get(path)
        if (value.isArray() && value.size() > 0) {
            return value.get(0)
        }
        return NULL
    }

    fun getPaths(path: String): List<String> {
        return jsonPathCtx.getPaths(path)
    }

    // ====== /get ======
    // ===== insert =====

    fun insert(path: String, idx: Int, value: Any): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        if (isJsonPath(path)) {
            val jsonPath = JsonPath.compile(path)
            if (jsonPath.isDefinite) {
                getOrSetNewArray(jsonPath).insert(idx, value)
            } else {
                get(jsonPath).forEach { it.insert(idx, value) }
            }
        } else {
            getOrSetNewArray(path).insert(idx, value)
        }
        return this
    }

    fun insert(idx: Int, value: Any?): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        val fieldValue = this.value
        if (fieldValue is ArrayNode) {
            fieldValue.insert(idx, mapper.toJson(value))
        }
        return this
    }

    fun insertAll(path: String, idx: Int, values: Iterable<Any>): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        if (isJsonPath(path)) {
            val jsonPath = JsonPath.compile(path)
            if (jsonPath.isDefinite) {
                getOrSetNewArray(jsonPath).insertAll(idx, values)
            } else {
                get(jsonPath).forEach { it.insertAll(idx, values) }
            }
        } else {
            getOrSetNewArray(path).insertAll(idx, values)
        }
        return this
    }

    fun insertAll(idx: Int, values: Iterable<Any>): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        val fieldValue = this.value
        if (fieldValue is ArrayNode) {
            val it = values.iterator()
            var mutIdx = idx
            while (it.hasNext()) {
                val value = it.next()
                fieldValue.insert(mutIdx++, mapper.toJson(value))
            }
        }
        return this
    }

    // ===== /insert =====
    // ======= add =======

    fun add(path: String, value: Any?): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        if (isJsonPath(path)) {
            val jsonPath = JsonPath.compile(path)
            if (jsonPath.isDefinite) {
                getOrSetNewArray(jsonPath).add(value)
            } else {
                get(jsonPath).forEach { it.add(value) }
            }
        } else {
            getOrSetNewArray(path).add(value)
        }
        return this
    }

    fun add(value: Any?): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        val fieldValue = this.value
        if (fieldValue is ArrayNode) {
            fieldValue.add(mapper.toJson(value))
        }
        return this
    }

    fun addAll(path: String, values: Iterable<Any>): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        if (isJsonPath(path)) {
            val jsonPath = JsonPath.compile(path)
            if (jsonPath.isDefinite) {
                getOrSetNewArray(jsonPath).addAll(values)
            } else {
                get(jsonPath).forEach { it.addAll(values) }
            }
        } else {
            getOrSetNewArray(path).addAll(values)
        }
        return this
    }

    fun addAll(values: Iterable<Any>): DataValue {
        if (unmodifiable) {
            throw RuntimeException("Unmodifiable")
        }
        values.forEach { add(it) }
        return this
    }

    // ======= /add =======

    private fun getOrSetNewArray(path: String): DataValue {
        return if (isJsonPath(path)) {
            getOrSetNewArray(JsonPath.compile(path))
        } else {
            var array = get(path)
            if (array.isNull()) {
                array = create(mapper.newArrayNode())
                set(path, array)
            }
            array
        }
    }

    private fun getOrSetNewArray(path: JsonPath): DataValue {
        var value = get(path)
        if (value.isNull() && path.isDefinite) {
            value = create(mapper.newArrayNode())
            set(path, value)
        }
        return value
    }

    fun size(): Int {
        return value.size()
    }

    override fun iterator(): Iterator<DataValue> {
        return Iter(value)
    }

    fun <T> mapKV(func: (String, DataValue) -> T): List<T> {
        val result = ArrayList<T>()
        forEach { k, v -> result.add(func.invoke(k, v)) }
        return result
    }

    fun <T> map(func: (DataValue) -> T): List<T> {
        val result = ArrayList<T>()
        forEach { v -> result.add(func.invoke(v)) }
        return result
    }

    fun forEach(consumer: (String, DataValue) -> Unit) {
        val names = value.fieldNames()
        while (names.hasNext()) {
            val name = names.next()
            consumer.invoke(name, DataValue(value[name]))
        }
    }

    fun forEachJ(consumer: BiConsumer<String, DataValue>) {
        forEach { k, v -> consumer.accept(k, v) }
    }

    fun canConvertToInt(): Boolean {
        return value.canConvertToInt()
    }

    fun canConvertToLong(): Boolean {
        return value.canConvertToLong()
    }

    fun textValue(): String {
        return value.textValue()
    }

    fun binaryValue(): ByteArray {
        return value.binaryValue()
    }

    fun booleanValue(): Boolean {
        return value.booleanValue()
    }

    fun numberValue(): Number {
        return value.numberValue()
    }

    fun shortValue(): Short {
        return value.shortValue()
    }

    fun intValue(): Int {
        return value.intValue()
    }

    fun longValue(): Long {
        return value.longValue()
    }

    fun floatValue(): Float {
        return value.floatValue()
    }

    fun doubleValue(): Double {
        return value.doubleValue()
    }

    fun decimalValue(): BigDecimal {
        return value.decimalValue()
    }

    fun bigIntegerValue(): BigInteger {
        return value.bigIntegerValue()
    }

    fun asText(): String {
        return if (isNull()) {
            ""
        } else {
            value.asText()
        }
    }

    fun asText(defaultValue: String?): String? {
        return value.asText(defaultValue)
    }

    fun asInt(): Int {
        return value.asInt()
    }

    fun asInt(defaultValue: Int): Int {
        return value.asInt(defaultValue)
    }

    fun asLong(): Long {
        return value.asLong()
    }

    fun asLong(defaultValue: Long): Long {
        return value.asLong(defaultValue)
    }

    fun asDouble(): Double {
        return value.asDouble()
    }

    fun asDouble(defaultValue: Double): Double {
        return value.asDouble(defaultValue)
    }

    fun asBoolean(): Boolean {
        return value.asBoolean()
    }

    fun asBoolean(defaultValue: Boolean): Boolean {
        return value.asBoolean(defaultValue)
    }

    fun <T : Any> getAs(type: Class<T>): T? {
        return mapper.convert(value, type)
    }

    fun toStrList(): List<String> {
        return toList(String::class.java)
    }

    /**
     * Convert internal value and return a new mutable list.
     * If internal value is not an array-like object then list with single element will be returned.
     */
    fun <T : Any> toList(elementType: Class<T>): MutableList<T> {
        if (value.isNull) {
            return ArrayList()
        }
        val arrValue = if (value.isArray) {
            value
        } else {
            val array = mapper.newArrayNode()
            array.add(value)
            array
        }
        return ArrayList(mapper.convert<List<T>>(arrValue, mapper.getListType(elementType)) ?: emptyList())
    }

    fun asStrList(): MutableList<String> {
        return asList(String::class.java)
    }

    /**
     * Convert internal value and return a new mutable list.
     * If internal value is not an array-like object then empty list will be returned.
     */
    fun <T : Any> asList(elementType: Class<T>): MutableList<T> {
        if (value.isArray) {
            return ArrayList(mapper.convert<List<T>>(value, mapper.getListType(elementType)) ?: emptyList())
        }
        return ArrayList()
    }

    /**
     * Convert internal value and return a new mutable map.
     * If internal value is not a map-like object then empty list will be returned.
     */
    fun <K : Any, V : Any> asMap(keyType: Class<K>, valueType: Class<V>): MutableMap<K, V> {
        return LinkedHashMap(
            if (!value.isNull) {
                mapper.convert<Map<K, V>>(value, mapper.getMapType(keyType, valueType)) ?: emptyMap()
            } else {
                emptyMap()
            }
        )
    }

    @com.fasterxml.jackson.annotation.JsonValue
    fun asJavaObj(): Any? {
        return mapper.toJava(value)
    }

    fun asObjectData(): ObjectData {
        return ObjectData.create(value)
    }

    @JsonValue
    fun asJson(): JsonNode {
        return value
    }

    override fun toString(): String {
        return value.toString()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (javaClass != other?.javaClass) {
            return false
        }

        other as DataValue

        return value == other.value
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

    private fun isJsonPath(path: String): Boolean {
        return path[0] == '$' && (path[1] == '.' || path[1] == '[')
    }

    fun asUnmodifiable(): DataValue {
        return DataValue(value.deepCopy(), true)
    }

    fun isUnmodifiable(): Boolean {
        return unmodifiable
    }

    private class Iter internal constructor(iterable: Iterable<JsonNode>) : Iterator<DataValue> {

        private val iterator: Iterator<JsonNode> = iterable.iterator()

        override fun hasNext(): Boolean {
            return iterator.hasNext()
        }

        override fun next(): DataValue {
            return DataValue(iterator.next())
        }
    }
}
