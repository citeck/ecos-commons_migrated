package ru.citeck.ecos.commons.utils

object LibsUtils {

    private const val JACKSON_CHECK_CLASS = "com.fasterxml.jackson.databind.JsonNode"

    private val isJacksonPresent = isClassPresent(JACKSON_CHECK_CLASS)

    @JvmStatic
    fun isJacksonPresent() = isJacksonPresent

    @JvmStatic
    fun isClassPresent(className: String?): Boolean {
        return try {
            Class.forName(className)
            true
        } catch (e: Exception) {
            false
        }
    }
}
