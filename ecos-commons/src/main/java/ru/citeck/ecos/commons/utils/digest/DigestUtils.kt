package ru.citeck.ecos.commons.utils.digest

import ru.citeck.ecos.commons.utils.io.IOUtils
import java.io.BufferedInputStream
import java.io.InputStream
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

object DigestUtils {

    @JvmStatic
    fun getSha256(value: String): Digest {
        return getSha256(value.toByteArray(Charsets.UTF_8))
    }

    @JvmStatic
    fun getSha256(value: ByteArray): Digest {
        return getSha256(IOUtils.toInStream(value))
    }

    @JvmStatic
    fun getSha256(input: InputStream): Digest {
        return getDigest(input, DigestAlgorithm.SHA256)
    }

    @JvmStatic
    fun getMD5(value: String): Digest {
        return getMD5(value.toByteArray(Charsets.UTF_8))
    }

    @JvmStatic
    fun getMD5(value: ByteArray): Digest {
        return getMD5(IOUtils.toInStream(value))
    }

    @JvmStatic
    fun getMD5(input: InputStream): Digest {
        return getDigest(input, DigestAlgorithm.MD5)
    }

    @JvmStatic
    fun getDigest(input: ByteArray, algorithm: DigestAlgorithm): Digest {
        return getDigest(IOUtils.toInStream(input), algorithm)
    }

    @JvmStatic
    fun getDigest(input: InputStream, algorithm: DigestAlgorithm): Digest {

        val digest: MessageDigest
        digest = try {
            MessageDigest.getInstance(algorithm.code)
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException(e)
        }

        val buffer = ByteArray(8192)
        var readCount: Int
        var fullSize: Long = 0

        BufferedInputStream(input).use { bis ->
            readCount = bis.read(buffer)
            while (readCount > 0) {
                fullSize += readCount.toLong()
                digest.update(buffer, 0, readCount)
                readCount = bis.read(buffer)
            }
        }

        return Digest(bytesToHex(digest.digest()), fullSize, algorithm)
    }

    private fun bytesToHex(hash: ByteArray): String {
        val hexString = StringBuilder()
        for (b in hash) {
            val hex = Integer.toHexString(0xFF and b.toInt())
            if (hex.length == 1) {
                hexString.append('0')
            }
            hexString.append(hex)
        }
        return hexString.toString()
    }
}
