package ru.citeck.ecos.commons.utils

import ecos.com.fasterxml.jackson210.databind.util.ClassUtil
import java.util.*
import kotlin.collections.ArrayList

object IterUtils {

    private val EMPTY_ITERABLE = EmptyIterable<Any>()

    @JvmStatic
    fun <T : Any> toList(iterator: Iterator<T>): List<T> {
        return Collections.unmodifiableList(toMutableList(iterator))
    }

    @JvmStatic
    fun <T : Any> toList(iterable: Iterable<T>): List<T> {
        return Collections.unmodifiableList(toMutableList(iterable.iterator()))
    }

    @JvmStatic
    fun <T : Any> toMutableList(iterable: Iterable<T>): MutableList<T> {
        return toMutableList(iterable.iterator())
    }

    @JvmStatic
    fun <T : Any> toMutableList(iterator: Iterator<T>): MutableList<T> {
        val result = ArrayList<T>()
        while (iterator.hasNext()) {
            result.add(iterator.next())
        }
        return result
    }

    @JvmStatic
    fun <T : Any> first(iterable: Iterable<T>): Optional<T> {
        return first(iterable.iterator())
    }

    @JvmStatic
    fun <T : Any> first(iterator: Iterator<T>): Optional<T> {
        if (iterator.hasNext()) {
            return Optional.ofNullable(iterator.next())
        }
        return Optional.empty()
    }

    fun <T : Any> empty(): Iterable<T> {
        @Suppress("UNCHECKED_CAST")
        return EMPTY_ITERABLE as Iterable<T>
    }

    fun <T : Any> emptyIt(): Iterator<T> {
        return ClassUtil.emptyIterator()
    }

    private class EmptyIterable<T : Any> : Iterable<T> {
        override fun iterator() = emptyIt<T>()
    }
}
