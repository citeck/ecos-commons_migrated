package ru.citeck.ecos.commons.io.file.filter

import ru.citeck.ecos.commons.io.file.EcosFile

class CompositeFileFilter @JvmOverloads constructor(
    val filters: List<FileFilter>,
    val joinBy: JoinBy = JoinBy.AND
) : FileFilter {

    companion object {

        fun and(vararg filters: FileFilter): CompositeFileFilter {
            return CompositeFileFilter(filters.toList(), JoinBy.AND)
        }

        fun or(vararg filters: FileFilter): CompositeFileFilter {
            return CompositeFileFilter(filters.toList(), JoinBy.OR)
        }
    }

    override fun accept(file: EcosFile): Boolean {
        return when (joinBy) {
            JoinBy.AND -> filters.all { it.accept(file) }
            JoinBy.OR -> filters.any { it.accept(file) }
        }
    }

    enum class JoinBy {
        AND, OR
    }
}
