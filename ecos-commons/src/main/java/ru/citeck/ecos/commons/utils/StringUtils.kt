package ru.citeck.ecos.commons.utils

object StringUtils {

    const val EMPTY = ""

    fun getLastNotEmptyCharIdx(str: String): Int {

        if (str.isEmpty()) {
            return -1
        }

        var idx = str.length - 1
        while (idx >= 0) {
            val char = str[idx]
            if (!char.isWhitespace()) {
                break
            }
            idx--
        }
        return idx
    }

    fun getNextNotEmptyCharIdx(str: String, startIdx: Int): Int {
        if (startIdx >= str.length) {
            return -1
        }
        var idx = startIdx
        var char = str[idx++]
        while (char.isWhitespace() && idx < str.length) {
            char = str[idx++]
        }
        return if (idx > str.length) {
            -1
        } else {
            idx - 1
        }
    }

    @JvmStatic
    fun defaultString(str: String?): String {
        return defaultString(str, EMPTY)
    }

    @JvmStatic
    fun defaultString(str: String?, defaultStr: String): String {
        return str ?: defaultStr
    }

    /**
     *
     * Checks if a CharSequence is whitespace, empty ("") or null.
     *
     * <pre>
     * StringUtils.isBlank(null)      = true
     * StringUtils.isBlank("")        = true
     * StringUtils.isBlank(" ")       = true
     * StringUtils.isBlank("bob")     = false
     * StringUtils.isBlank("  bob  ") = false
     </pre> *
     *
     * @param cs  the CharSequence to check, may be null
     * @return `true` if the CharSequence is null, empty or whitespace
     * @since 2.0
     * @since 3.0 Changed signature from isBlank(String) to isBlank(CharSequence)
     */
    @JvmStatic
    fun isBlank(cs: CharSequence?): Boolean {
        return cs?.isBlank() ?: true
    }

    /**
     *
     * Checks if a CharSequence is not empty (""), not null and not whitespace only.
     *
     * <pre>
     * StringUtils.isNotBlank(null)      = false
     * StringUtils.isNotBlank("")        = false
     * StringUtils.isNotBlank(" ")       = false
     * StringUtils.isNotBlank("bob")     = true
     * StringUtils.isNotBlank("  bob  ") = true
     </pre> *
     *
     * @param cs  the CharSequence to check, may be null
     * @return `true` if the CharSequence is not empty and not null and not whitespace
     * @since 2.0
     * @since 3.0 Changed signature from isNotBlank(String) to isNotBlank(CharSequence)
     */
    @JvmStatic
    fun isNotBlank(cs: CharSequence?): Boolean {
        return !isBlank(cs)
    }

    // Empty checks
    // -----------------------------------------------------------------------
    /**
     *
     * Checks if a CharSequence is empty ("") or null.
     *
     * <pre>
     * StringUtils.isEmpty(null)      = true
     * StringUtils.isEmpty("")        = true
     * StringUtils.isEmpty(" ")       = false
     * StringUtils.isEmpty("bob")     = false
     * StringUtils.isEmpty("  bob  ") = false
     </pre> *
     *
     *
     * NOTE: This method changed in Lang version 2.0.
     * It no longer trims the CharSequence.
     * That functionality is available in isBlank().
     *
     * @param cs  the CharSequence to check, may be null
     * @return `true` if the CharSequence is empty or null
     * @since 3.0 Changed signature from isEmpty(String) to isEmpty(CharSequence)
     */
    @JvmStatic
    fun isEmpty(cs: CharSequence?): Boolean {
        return cs == null || cs.isEmpty()
    }

    /**
     * Checks if a CharSequence contains only specified character.
     */
    @JvmStatic
    fun containsOnly(cs: CharSequence, character: Char): Boolean {
        if (cs.isEmpty()) {
            return false
        }
        for (element in cs) {
            if (element != character) {
                return false
            }
        }
        return true
    }

    // Count matches
    // -----------------------------------------------------------------------
    /**
     *
     * Counts how many times the substring appears in the larger string.
     *
     *
     * A `null` or empty ("") String input returns `0`.
     *
     * <pre>
     * StringUtils.countMatches(null, *)       = 0
     * StringUtils.countMatches("", *)         = 0
     * StringUtils.countMatches("abba", null)  = 0
     * StringUtils.countMatches("abba", "")    = 0
     * StringUtils.countMatches("abba", "a")   = 2
     * StringUtils.countMatches("abba", "ab")  = 1
     * StringUtils.countMatches("abba", "xxx") = 0
     </pre> *
     *
     * @param str  the CharSequence to check, may be null
     * @param sub  the substring to count, may be null
     * @return the number of occurrences, 0 if either CharSequence is `null`
     * @since 3.0 Changed signature from countMatches(String, String) to countMatches(CharSequence, CharSequence)
     */
    @JvmStatic
    fun countMatches(str: StringBuilder, sub: String): Int {
        if (isEmpty(str) || isEmpty(sub)) {
            return 0
        }
        var count = 0
        var idx = 0
        while (str.indexOf(sub, idx).also { idx = it } != -1) {
            count++
            idx += sub.length
        }
        return count
    }

    @JvmStatic
    fun escapeDoubleQuotes(name: String): String {
        var quoteIdx = name.indexOf('"')
        if (quoteIdx == -1) {
            return name
        }
        val sb = StringBuilder()
        var beforeIdx = 0
        while (quoteIdx >= 0) {
            sb.append(name, beforeIdx, quoteIdx)
            if (quoteIdx == 0 || name[quoteIdx - 1] != '\\') {
                sb.append('\\')
            }
            beforeIdx = quoteIdx
            quoteIdx = name.indexOf('"', quoteIdx + 1)
        }
        sb.append(name, beforeIdx, name.length)
        return sb.toString()
    }
}
