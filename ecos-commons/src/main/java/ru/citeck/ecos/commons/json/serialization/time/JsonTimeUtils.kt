package ru.citeck.ecos.commons.json.serialization.time

object JsonTimeUtils {

    private const val SEC_MS_THRESHOLD = 1000000000000

    fun isNotTimeSeconds(value: Double): Boolean {
        return value > SEC_MS_THRESHOLD
    }
}
