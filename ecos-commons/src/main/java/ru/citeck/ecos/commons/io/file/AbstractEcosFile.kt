package ru.citeck.ecos.commons.io.file

import mu.KotlinLogging
import ru.citeck.ecos.commons.io.file.filter.AlwaysTrueFileFilter
import ru.citeck.ecos.commons.io.file.filter.FileFilter
import ru.citeck.ecos.commons.io.file.filter.NotDirectoryFileFilter
import ru.citeck.ecos.commons.io.file.filter.PathMatcherFilter
import ru.citeck.ecos.commons.utils.func.UncheckedConsumer
import ru.citeck.ecos.commons.utils.func.toFunc
import ru.citeck.ecos.commons.utils.io.IOUtils
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.io.OutputStream
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import java.nio.file.Path
import java.nio.file.Paths

abstract class AbstractEcosFile(
    parent: AbstractEcosFile? = null,
    name: String = ""
) : EcosFile {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    private val nameField = name
    private val parentField = parent

    private val pathField: Path = when {
        parent == null -> getPathFromStr("/")
        name.isNotBlank() -> parent.getPath().resolve(name)
        else -> {
            throw IllegalArgumentException("File with parent should has name. Parent: '${parent.getPath()}'")
        }
    }

    init {
        if (name.contains("/") || name.contains("\\")) {
            throw IllegalArgumentException(
                "Child name should not contain delimiters. " +
                    "Name: '$name' Parent: '${parent?.getPath()}'"
            )
        }
    }

    override fun getParent(): AbstractEcosFile? = parentField
    override fun getName(): String = nameField
    override fun getPath(): Path = pathField

    override fun copyFilesFrom(file: EcosFile) {
        if (!isDirectory()) {
            log.warn {
                "Content copying can't be performed to non directory. " +
                    "Current: ${getPath()} Other: ${file.getPath()}"
            }
            return
        }
        if (!file.isDirectory()) {
            log.warn {
                "Content copying can't be performed from non directory. " +
                    "Current: ${getPath()} Other: ${file.getPath()}"
            }
            return
        }
        file.getChildren().forEach {
            if (it.isDirectory()) {
                val dir = getOrCreateDir(it.getName())
                dir.copyFilesFrom(it)
            } else {
                getChild(it.getName())?.delete()
                createFile(it.getName()) { output ->
                    it.read { input -> IOUtils.copy(input, output) }
                }
            }
        }
    }

    override fun findFiles(pattern: String): List<EcosFile> {
        return findFiles(PathMatcherFilter(getPath(), pattern))
    }

    override fun findFiles(): List<EcosFile> {
        return findFiles(NotDirectoryFileFilter.INSTANCE)
    }

    override fun findFilesWithDirs(): List<EcosFile> {
        return findFiles(AlwaysTrueFileFilter.INSTANCE)
    }

    override fun findFiles(filter: FileFilter): List<EcosFile> {
        return findFiles(filter, true)
    }

    override fun createFile(path: String, data: String): EcosFile {
        return createFile(path, ByteArrayInputStream(data.toByteArray(Charsets.UTF_8)))
    }

    override fun createFile(path: String, data: ByteArray): EcosFile {
        return createFile(path, ByteArrayInputStream(data))
    }

    override fun createFile(path: String, data: EcosFile): EcosFile {
        return data.read { createFile(path, it) }!!
    }

    override fun createFile(path: String, data: InputStream): EcosFile {
        return createFile(path) { IOUtils.copy(data, it) }
    }

    /**
     * Create file with unchecked consumer
     */
    override fun createFileUch(path: String, outWriter: UncheckedConsumer<OutputStream>): EcosFile {
        return createFile(path, outWriter.toFunc())
    }

    override fun createFile(path: String, outWriter: (OutputStream) -> Unit): EcosFile {
        val filePath = getPathFromStr(path)
        val parentDir = getParentForPath(filePath, true)!!
        return parentDir.tryToCreateFile(filePath.fileName.toString(), outWriter)
    }

    override fun createDir(path: String): EcosFile {
        val dirPath = getPathFromStr(path)
        val parentDir = getParentForPath(dirPath, true)!!
        return parentDir.tryToCreateDir(dirPath.fileName.toString())
    }

    override fun getOrCreateDir(path: String): AbstractEcosFile {

        val dirPath = getPathFromStr(path)
        val parentDir = getParentForPath(dirPath, true)!!
        val childName = dirPath.fileName.toString()

        val file = parentDir.getChild(childName)
        return if (file != null) {
            if (file.isDirectory()) {
                file
            } else {
                error("File type mismatch. Expected: Directory, Actual: File")
            }
        } else {
            parentDir.tryToCreateDir(childName)
        }
    }

    private fun getParentForPath(path: Path, createDirIfNotExists: Boolean): AbstractEcosFile? {

        if (path.nameCount == 0) {
            throw IllegalArgumentException("Incorrect path: '$path'")
        }

        val relativePath = getPath().relativize(getPath().resolve(path)).normalize()

        var parent = this
        for (i in 0 until (relativePath.nameCount - 1)) {
            val pathElem = relativePath.getName(i).toString()
            parent = if (pathElem == "..") {
                parent.parentField!!
            } else {
                if (createDirIfNotExists) {
                    parent.getOrCreateDir(pathElem)
                } else {
                    parent.getDir(pathElem) ?: return null
                }
            }
        }

        return parent
    }

    override fun getRoot(): AbstractEcosFile {
        var rootFile = this
        while (rootFile.parentField != null) {
            rootFile = rootFile.parentField!!
        }
        return rootFile
    }

    override fun getFile(path: String): AbstractEcosFile? {
        val result = getFileOrDir(path)
        if (result != null && result.isDirectory()) {
            throw IllegalStateException("Incorrect file type. Expected: 'File' Actual: 'Directory'")
        }
        return result
    }

    override fun getDir(path: String): AbstractEcosFile? {
        val result = getFileOrDir(path)
        if (result != null && !result.isDirectory()) {
            throw IllegalStateException("Incorrect file type. Expected: 'Directory' Actual: 'File'")
        }
        return result
    }

    override fun getFileOrDir(path: String): AbstractEcosFile? {
        val filePath = getPathFromStr(path)
        if (filePath.nameCount == 0) {
            return getRoot()
        }
        val pathParent = if (!path.contains("/") && !path.contains("\\")) {
            this
        } else {
            getParentForPath(filePath, false)
        }
        return pathParent?.getChild(filePath.fileName.toString())
    }

    private fun tryToCreateFile(name: String, outWriter: (OutputStream) -> Unit): EcosFile {
        val child = getChild(name)
        if (child != null) {
            throw IllegalStateException("File with name '$name' already exists. Parent: ${getPath()}")
        }
        return createFileImpl(name, outWriter)
    }

    protected abstract fun createFileImpl(name: String, outWriter: (OutputStream) -> Unit): EcosFile

    private fun tryToCreateDir(name: String): AbstractEcosFile {
        val child = getChild(name)
        if (child != null) {
            throw IllegalStateException("Directory with name '$name' already exists. Parent: ${getPath()}")
        }
        return createDirImpl(name)
    }

    protected abstract fun createDirImpl(name: String): AbstractEcosFile

    protected abstract fun getChild(name: String): AbstractEcosFile?

    override fun readAsBytes(): ByteArray = read {
        IOUtils.readAsBytes(it)
    }!!

    override fun readAsString(): String = read {
        IOUtils.readAsString(it)
    }!!

    private fun getPathFromStr(path: String): Path {
        return Paths.get(path.replace("\\", "/"))
    }

    override fun setLastModifiedTime(time: Long) {
    }

    override fun setCreationTime(time: Long) {
    }

    override fun setLastAccessTime(time: Long) {
    }
}
