package ru.citeck.ecos.commons.json

import ecos.com.fasterxml.jackson210.core.JsonParseException
import ecos.com.fasterxml.jackson210.core.JsonProcessingException
import ecos.com.fasterxml.jackson210.databind.*
import ecos.com.fasterxml.jackson210.databind.node.ArrayNode
import ecos.com.fasterxml.jackson210.databind.node.NullNode
import ecos.com.fasterxml.jackson210.databind.node.ObjectNode
import ecos.com.fasterxml.jackson210.databind.node.TextNode
import ecos.com.fasterxml.jackson210.databind.type.TypeFactory
import mu.KotlinLogging
import org.apache.commons.beanutils.PropertyUtils
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.std.EcosStdFile
import ru.citeck.ecos.commons.json.serialization.annotation.IncludeNonDefault
import ru.citeck.ecos.commons.utils.DataUriUtil
import ru.citeck.ecos.commons.utils.LibsUtils
import ru.citeck.ecos.commons.utils.StringUtils
import java.beans.PropertyDescriptor
import java.io.*
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import com.fasterxml.jackson.databind.JsonNode as JacksonJsonNode
import com.fasterxml.jackson.databind.node.NullNode as JacksonNullNode

class JsonMapperImpl(
    private val context: MappingContext,
    private val options: JsonOptions
) : JsonMapper {

    companion object {

        private val log = KotlinLogging.logger {}

        private const val MIME_OCTET_STREAM = "application/octet-stream"
        private const val MIME_APP_YAML = "application/x-yaml"
        private const val MIME_APP_JSON = "application/json"
        private const val MIME_TEXT_PLAIN = "text/plain"
    }

    private var mapper = ObjectMapper()
    private var prettyWriter = mapper.writer().withDefaultPrettyPrinter()

    @Volatile
    private var ctxVersion: Int = -1

    private val defaultValueByClass = ConcurrentHashMap<Class<*>, ObjectNode>()

    private fun getDefaultValueByClass(clazz: Class<*>): ObjectNode {
        return defaultValueByClass.computeIfAbsent(clazz) {
            convert(
                read("{}", it),
                ObjectNode::class.java
            ) ?: newObjectNode()
        }
    }

    private fun getMapper(): ObjectMapper {

        while (ctxVersion != context.getVersion()) {
            ctxVersion = context.getVersion()
            mapper = createMapper(options)
            prettyWriter = mapper.writer().withDefaultPrettyPrinter()
        }

        return mapper
    }

    private fun createMapper(options: JsonOptions): ObjectMapper {

        val mapper = if (options.factory != null) {
            ObjectMapper(options.factory)
        } else {
            ObjectMapper()
        }

        options.deserFeatures.forEach { (feature, enabled) ->
            if (enabled) {
                mapper.enable(feature.feature)
            } else {
                mapper.disable(feature.feature)
            }
        }
        options.serialFeatures.forEach { (feature, enabled) ->
            if (enabled) {
                mapper.enable(feature.feature)
            } else {
                mapper.disable(feature.feature)
            }
        }

        mapper.registerModules(context.getModules())

        return mapper
    }

    override fun applyData(to: Any?, from: Any?) {
        if (to == null || from == null) {
            return
        }
        val node = convert(from, ObjectNode::class.java)
        try {
            getMapper().readerForUpdating(to).readValue<Any>(node)
        } catch (e: IOException) {
            throw RuntimeException("Exception", e)
        }
    }

    override fun <T : Any> readNotNull(value: String, type: Class<T>): T {
        return read(value, type)!!
    }

    override fun write(resultFile: File, value: Any) {
        getMapper().writeValue(resultFile, value)
    }

    override fun write(out: OutputStream, value: Any) {
        getMapper().writeValue(out, value)
    }

    override fun write(out: DataOutput, value: Any) {
        getMapper().writeValue(out, value)
    }

    override fun write(w: Writer, value: Any) {
        getMapper().writeValue(w, value)
    }

    override fun read(json: String?): JsonNode? {
        return read(json, JsonNode::class.java)
    }

    override fun <T : Any> read(file: File?, type: Class<T>): T? {
        val notNullFile = file ?: return getDefaultValueForType(type)
        return read(EcosStdFile(notNullFile), type)
    }

    override fun <T : Any> read(file: EcosFile?, type: Class<T>): T? {

        file ?: return getDefaultValueForType(type)

        return try {
            val name = file.getName()
            if (name.endsWith(".yml") || name.endsWith(".yaml")) {
                convert(YamlUtils.read(file, context), type)
            } else if (name.endsWith(".json")) {
                read(file.readAsString(), type)
            } else {
                file.read { read(it, type) }
            }
        } catch (e: Exception) {
            log.error(e) { "Error while file reading. File: ${file.getPath()}" }
            null
        }
    }

    override fun <T : Any> read(json: ByteArray?, type: Class<T>): T? {
        return if (json == null) {
            getDefaultValueForType(type)
        } else {
            read(ByteArrayInputStream(json), type)
        }
    }

    override fun <T : Any> read(inputStream: InputStream?, type: Class<T>): T? {
        return if (inputStream == null) {
            getDefaultValueForType(type)
        } else {
            getMapper().readValue(inputStream, type)
        }
    }

    override fun <T : Any> read(reader: Reader?, type: Class<T>): T? {
        return if (reader == null) {
            getDefaultValueForType(type)
        } else {
            read(reader.readText(), type)
        }
    }

    override fun <T : Any> read(value: String?, type: Class<T>): T? {
        return read(value, type, getDefaultValueForType(type))
    }

    override fun <T : Any> readList(value: String?, elementType: Class<T>): List<T> {
        return read(value, getListType(elementType), emptyList()) ?: emptyList()
    }

    override fun <K : Any, V : Any> readMap(value: String?, keyType: Class<K>, valueType: Class<V>): Map<K, V> {
        return read(value, getMapType(keyType, valueType), emptyMap()) ?: emptyMap()
    }

    override fun <T : Any> read(value: String?, type: Class<T>, deflt: T?): T? {
        return read(value, getMapper().typeFactory.constructType(type), deflt)
    }

    override fun <T : Any> read(value: String?, type: JavaType, deflt: T?): T? {

        if (value == null || value.isBlank()) {
            return deflt
        }
        val valueToRead = if (value[0] == '\uFEFF') {
            value.substring(1)
        } else {
            value
        }

        if (valueToRead.startsWith(DataUriUtil.DATA_PREFIX)) {
            val parsedData = DataUriUtil.parseData(valueToRead)
            val dataBytes = parsedData.data
            if (dataBytes != null) {
                var mimeType = parsedData.mimeType.split(";")[0]
                if (mimeType == MIME_OCTET_STREAM || mimeType == MIME_TEXT_PLAIN) {
                    tryToGuessMimetype(dataBytes)?.let { mimeType = it }
                }
                return try {
                    when (mimeType) {
                        MIME_APP_JSON -> {
                            getMapper().readValue<T>(dataBytes, type)
                        }
                        MIME_APP_YAML,
                        "text/yaml",
                        "text/vnd.yaml",
                        "text/x-yaml" -> {
                            convert(YamlUtils.read(String(dataBytes, Charsets.UTF_8), context), type)
                        }
                        else -> {
                            log.error { "Unknown mimetype: $mimeType" }
                            null
                        }
                    }
                } catch (e: Exception) {
                    log.error("Conversion error. Type: '$type' Value: '$valueToRead'", e)
                    null
                } ?: deflt
            }
        }

        val result: Any?
        if (type.rawClass == Boolean::class.java) {
            result = when (valueToRead) {
                "true" -> true
                "false" -> false
                else -> null
            }
        } else {

            val firstNotEmptyCharIdx = StringUtils.getNextNotEmptyCharIdx(valueToRead, 0)
            var firstNotEmptyChar: Char? = null
            var secondNotEmptyChar: Char? = null
            var lastNotEmptyChar: Char? = null

            if (firstNotEmptyCharIdx >= 0) {
                firstNotEmptyChar = valueToRead[firstNotEmptyCharIdx]
                val lastNotEmptyCharIdx = StringUtils.getLastNotEmptyCharIdx(valueToRead)
                if (lastNotEmptyCharIdx != -1) {
                    lastNotEmptyChar = valueToRead[lastNotEmptyCharIdx]
                }
                val secondNotEmptyCharIdx =
                    StringUtils.getNextNotEmptyCharIdx(valueToRead, firstNotEmptyCharIdx + 1)
                if (secondNotEmptyCharIdx > 0) {
                    secondNotEmptyChar = valueToRead[secondNotEmptyCharIdx]
                }
            }

            result = try {

                var res: Any? = null
                var resolved = false

                if ((
                    firstNotEmptyChar == '{' &&
                        (secondNotEmptyChar == '"' || secondNotEmptyChar == '}') && lastNotEmptyChar == '}'
                    ) ||
                    (firstNotEmptyChar == '[' && lastNotEmptyChar == ']') ||
                    (firstNotEmptyChar == '"' && lastNotEmptyChar == '"')
                ) {

                    res = try {
                        val parsed: Any? = getMapper().readValue(valueToRead, type)
                        resolved = true
                        parsed
                    } catch (e: JsonParseException) {
                        log.debug { "String is not a valid json:\n$valueToRead" }
                        resolved = false
                        null
                    }
                }

                if (!resolved) {

                    if (firstNotEmptyChar == '%') {

                        if (valueToRead.length > firstNotEmptyCharIdx + 5) {
                            val yamlPrefix = valueToRead.substring(firstNotEmptyCharIdx + 1, firstNotEmptyCharIdx + 5)
                            if (yamlPrefix == "YAML") {
                                res = convert(YamlUtils.read(valueToRead, context), type)
                                resolved = true
                            }
                        }
                    } else if (firstNotEmptyChar == '-') {

                        if (valueToRead.length > firstNotEmptyCharIdx + 4) {
                            val yamlPrefix = valueToRead.substring(firstNotEmptyCharIdx, firstNotEmptyCharIdx + 4)
                            if (yamlPrefix == "---\n") {
                                val yamlValue = YamlUtils.read(valueToRead, context)
                                if (yamlValue != valueToRead) {
                                    res = convert(yamlValue, type)
                                    resolved = true
                                }
                            }
                        }
                    }
                }

                if (resolved) {
                    res
                } else {
                    getMapper().readValue(TextNode.valueOf(valueToRead).toString(), type)
                }
            } catch (e: Exception) {
                log.error("Conversion error. Type: '$type' Value: '$valueToRead'", e)
                null
            }
        }
        @Suppress("UNCHECKED_CAST")
        val resultT = result as? T?
        return resultT ?: deflt
    }

    private fun tryToGuessMimetype(bytes: ByteArray): String? {

        if (bytes.size < 2) {
            return null
        }

        val firstNonWhitespaceChar = bytes.map { it.toInt().toChar() }
            .find { !it.isWhitespace() } ?: return null

        return when (firstNonWhitespaceChar) {
            '{' -> MIME_APP_JSON
            '-' -> MIME_APP_YAML
            // This class works with json and yaml. Other types are not expected
            else -> MIME_APP_YAML
        }
    }

    override fun <T : Any> convert(value: Any?, type: Class<T>): T? {
        return convert(value, type, getDefaultValueForType(type))
    }

    private fun normalizeJsonValue(value: Any?): Any? {

        if (value == null) {
            return null
        }

        return if (LibsUtils.isJacksonPresent()) {
            if (value is JacksonJsonNode) {
                getMapper().convertValue(value, JsonNode::class.java)
            } else {
                value
            }
        } else {
            value
        }
    }

    override fun <T : Any> convert(value: Any?, type: Class<T>, deflt: T?): T? {
        return convert(value, getMapper().typeFactory.constructType(type), deflt)
    }

    override fun <T : Any> convert(value: Any?, type: JavaType): T? {
        return convert(value, type, getDefaultValueForType(type))
    }

    override fun <T : Any> convert(value: Any?, type: JavaType, deflt: T?): T? {

        if (type.rawClass == Unit::class.java) {
            @Suppress("UNCHECKED_CAST")
            return Unit as T
        }

        var valueToConvert = value
        if (valueToConvert is Optional<*>) {
            valueToConvert = valueToConvert.orElse(null)
        }
        if (valueToConvert == null || "null" == valueToConvert) {
            return deflt
        }

        try {

            valueToConvert = normalizeJsonValue(valueToConvert)
            val result = if (valueToConvert == null ||
                valueToConvert is JsonNode && (valueToConvert.isNull || valueToConvert.isMissingNode) ||
                valueToConvert is DataValue && valueToConvert.isNull()
            ) {

                deflt
            } else {

                if (type.rawClass == valueToConvert::class.java) {

                    valueToConvert
                } else {

                    if (valueToConvert is DataValue) {
                        valueToConvert = valueToConvert.asJson()
                    }

                    if (type.rawClass == DataValue::class.java) {

                        DataValue.create(valueToConvert)
                    } else if (type.rawClass == JsonNode::class.java &&
                        valueToConvert is String &&
                        valueToConvert.isBlank()
                    ) {

                        TextNode.valueOf(valueToConvert)
                    } else if (type.rawClass == JsonNode::class.java && valueToConvert is JsonNode) {

                        valueToConvert
                    } else if (type.rawClass == String::class.java &&
                        (valueToConvert is JsonNode && valueToConvert.isTextual)
                    ) {

                        valueToConvert.asText()
                    } else {

                        when (valueToConvert) {
                            is String -> read(valueToConvert, type, deflt)
                            is TextNode -> read(valueToConvert.textValue(), type, deflt)
                            else -> getMapper().convertValue(valueToConvert, type)
                        }
                    }
                }
            }
            @Suppress("UNCHECKED_CAST")
            return result as? T ?: deflt
        } catch (e: Exception) {
            log.error("Conversion error. Type: '$type' Value: '$valueToConvert'", e)
        }
        return deflt
    }

    override fun <T : Any> copy(value: T?): T? {
        if (value == null) {
            return null
        }
        val result: Any? = when (value) {
            is String -> value
            is JsonNode -> value.deepCopy()
            is DataValue -> value.copy()
            is ObjectData -> value.deepCopy()
            else -> {
                val mapper = getMapper()
                try {
                    mapper.treeToValue(mapper.valueToTree(value), value.javaClass)
                } catch (e: JsonProcessingException) {
                    throw RuntimeException(e)
                }
            }
        }
        @Suppress("UNCHECKED_CAST")
        return result as T
    }

    override fun toBytes(value: Any?): ByteArray? {
        if (value == null) {
            return null
        }
        if (value is ByteArray) {
            return value
        }
        return if (value is String) {
            value.toByteArray(StandardCharsets.UTF_8)
        } else try {
            getMapper().writeValueAsBytes(value)
        } catch (e: JsonProcessingException) {
            log.error("Json write error", e)
            null
        }
    }

    override fun toString(value: Any?): String? {
        if (value == null) {
            return null
        }
        val data = convert(value, JsonNode::class.java)
        return getMapper().writeValueAsString(data)
    }

    override fun toPrettyString(value: Any?): String? {
        if (value == null) {
            return null
        }
        val data = convert(value, JsonNode::class.java)
        return prettyWriter.writeValueAsString(data)
    }

    override fun isEquals(value0: Any?, value1: Any?): Boolean {

        if (value0 === value1) {
            return true
        }
        if (value0 == null || value1 == null) {
            return false
        }
        if (value0.javaClass == value1.javaClass) {
            return value0 == value1
        }
        val json0 = toJson(value0)
        val json1 = toJson(value1)

        return json0 == json1
    }

    override fun toNonDefaultJson(value: Any?): JsonNode {

        if (value == null) {
            return NullNode.getInstance()
        }
        val data = convert(value, ObjectNode::class.java) ?: return NullNode.getInstance()

        return toNonDefaultJson(data, value::class.java, null)
    }

    private fun toNonDefaultJson(value: JsonNode, clazz: Class<*>?, desc: PropertyDescriptor?): JsonNode {

        if (clazz == null) {
            return value
        }
        if (value.isArray) {
            if (desc == null) {
                return value
            }
            val returnType = desc.readMethod.genericReturnType
            if (returnType !is ParameterizedType) {
                return value
            }
            if (returnType.actualTypeArguments.size != 1) {
                return value
            }
            val elementType = returnType.actualTypeArguments[0]
            if (elementType !is Class<*>) {
                return value
            }
            val resultNode = newArrayNode()
            for (node in value) {
                resultNode.add(toNonDefaultJson(node, elementType, null))
            }
            return resultNode
        }
        if (!value.isObject) {
            return value
        }
        if (!clazz.isAnnotationPresent(IncludeNonDefault::class.java)) {
            return value
        }

        val fieldTypes = if (Map::class.java.isAssignableFrom(clazz) ||
            Collection::class.java.isAssignableFrom(clazz) ||
            clazz.isArray
        ) {
            emptyMap()
        } else {
            PropertyUtils.getPropertyDescriptors(clazz).map { it.name to it }.toMap()
        }

        val defaultValue = getDefaultValueByClass(clazz)

        val resultData = newObjectNode()
        val keys = value.fieldNames()
        while (keys.hasNext()) {
            val key = keys.next()
            val dataValue = value.get(key)
            if (dataValue != defaultValue.get(key)) {
                val propDescriptor = fieldTypes[key]
                resultData.set<JsonNode>(
                    key,
                    toNonDefaultJson(dataValue, propDescriptor?.propertyType, propDescriptor)
                )
            }
        }
        return resultData
    }

    override fun toJson(value: Any?): JsonNode {
        return convert(value, JsonNode::class.java) ?: NullNode.getInstance()
    }

    override fun toJava(node: Any?): Any? {

        if (node == null) {
            return null
        }
        if (node is DataValue) {
            return node.asJavaObj()
        }
        if (node !is JsonNode) {
            return node
        }
        return if (node.isNull || node.isMissingNode) {
            null
        } else try {
            getMapper().treeToValue(node, Any::class.java)
        } catch (e: JsonProcessingException) {
            log.error("Tree to Object.class conversion failed. Tree: $node")
            null
        }
    }

    override fun newObjectNode(): ObjectNode {
        return getMapper().createObjectNode()
    }

    override fun newArrayNode(): ArrayNode {
        return getMapper().createArrayNode()
    }

    override fun getType(type: Type): JavaType {
        return getMapper().constructType(type)
    }

    override fun getListType(elementType: Class<*>): JavaType {
        return getTypeFactory().constructCollectionType(List::class.java, elementType)
    }

    override fun getSetType(elementType: Class<*>): JavaType {
        return getTypeFactory().constructCollectionType(Set::class.java, elementType)
    }

    override fun getMapType(keyType: Class<*>, valueType: Class<*>): JavaType {
        return getTypeFactory().constructMapType(Map::class.java, keyType, valueType)
    }

    override fun getTypeFactory(): TypeFactory {
        return getMapper().typeFactory
    }

    private fun <T : Any> getDefaultValueForType(type: JavaType): T? {
        @Suppress("UNCHECKED_CAST")
        return getDefaultValueForType(type.rawClass as Class<T>)
    }

    private fun <T : Any> getDefaultValueForType(type: Class<T>): T? {

        if (LibsUtils.isJacksonPresent()) {
            if (type == JacksonJsonNode::class.java) {
                @Suppress("UNCHECKED_CAST")
                return JacksonNullNode.getInstance() as T
            }
        }

        @Suppress("UNCHECKED_CAST")
        return when (type) {
            DataValue::class.java -> DataValue.NULL
            JsonNode::class.java -> NullNode.getInstance()
            else -> null
        } as T?
    }
}
