package ru.citeck.ecos.commons.utils.func

@FunctionalInterface
interface UncheckedConsumer<T> {

    @Throws(Exception::class)
    fun accept(arg: T)
}
