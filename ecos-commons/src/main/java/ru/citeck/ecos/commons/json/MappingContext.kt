package ru.citeck.ecos.commons.json

import ecos.com.fasterxml.jackson210.databind.JsonDeserializer
import ecos.com.fasterxml.jackson210.databind.JsonSerializer
import ecos.com.fasterxml.jackson210.databind.Module
import ecos.com.fasterxml.jackson210.databind.module.SimpleModule
import mu.KotlinLogging
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.collections.ArrayList

class MappingContext(
    private val parent: MappingContext? = null
) {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    private val deserializers: MutableMap<Class<Any>, JsonDeserializer<*>> = ConcurrentHashMap()
    private val serializers: MutableMap<Class<Any>, JsonSerializer<*>> = ConcurrentHashMap()
    private val excludedDeserializers: MutableSet<Class<*>> = Collections.newSetFromMap(ConcurrentHashMap())
    private val excludedSerializers: MutableSet<Class<*>> = Collections.newSetFromMap(ConcurrentHashMap())

    private val customModules: MutableList<Module> = CopyOnWriteArrayList()

    private var fileResolver: FileResolver? = null

    @Volatile
    private var version: Int = 0

    fun getVersion(): Int {
        return (parent?.getVersion() ?: 0) + version
    }

    fun addModule(module: Module) {
        customModules.add(module)
    }

    fun getFileResolver(): FileResolver? {
        return fileResolver ?: parent?.getFileResolver()
    }

    fun getModules(): List<Module> {

        val result = ArrayList<Module>()
        parent?.customModules?.forEach { result.add(it) }
        result.addAll(customModules)

        val module = SimpleModule("custom-ecos-module")

        forEachDeserializer { type, des ->
            if (!excludedDeserializers.contains(type)) {
                @Suppress("UNCHECKED_CAST")
                module.addDeserializer(type as Class<Any>, des)
            }
        }
        forEachSerializer { type, ser ->
            if (!excludedSerializers.contains(type)) {
                @Suppress("UNCHECKED_CAST")
                module.addSerializer(type, ser as JsonSerializer<Any>)
            }
        }
        result.add(module)

        return result
    }

    private fun forEachSerializer(action: (type: Class<*>, serializer: JsonSerializer<*>) -> Unit) {
        parent?.forEachSerializer(action)
        serializers.forEach(action)
    }

    private fun forEachDeserializer(action: (type: Class<*>, serializer: JsonDeserializer<*>) -> Unit) {
        parent?.forEachDeserializer(action)
        deserializers.forEach(action)
    }

    fun setFileResolver(fileResolver: FileResolver) {
        this.fileResolver = fileResolver
    }

    fun addDeserializer(deserializer: JsonDeserializer<*>) {
        @Suppress("UNCHECKED_CAST")
        val type = deserializer.handledType() as? Class<Any>
        if (type == null) {
            log.error("Deserializer doesn't have handled type. Skip it. Type: " + deserializer.javaClass)
        } else {
            deserializers[type] = deserializer
            version++
        }
    }

    fun addSerializer(serializer: JsonSerializer<*>) {
        @Suppress("UNCHECKED_CAST")
        val type = serializer.handledType() as? Class<Any>
        if (type == null) {
            log.error("Serializer doesn't have handled type. Skip it. Type: " + serializer.javaClass)
        } else {
            serializers[type] = serializer
            version++
        }
    }

    fun addExcludedSerializers(types: Set<Class<*>>) {
        this.excludedSerializers.addAll(types)
        version++
    }

    fun addExcludedDeserializers(types: Set<Class<*>>) {
        this.excludedDeserializers.addAll(types)
        version++
    }
}
