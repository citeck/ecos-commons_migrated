package ru.citeck.ecos.commons.json.path

import ecos.com.jayway.jsonpath.Configuration
import ecos.com.jayway.jsonpath.DocumentContext
import ecos.com.jayway.jsonpath.PathNotFoundException
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.json.Json
import ecos.com.jayway.jsonpath.JsonPath as JaywayJsonPath

class DataContextImpl(
    val data: Any,
    valuesConfig: Configuration,
    pathsConfig: Configuration,
    unlimitedConfig: Configuration
) : DataContext {

    private val valuesContext: DocumentContext by lazy {
        JaywayJsonPath.using(valuesConfig).parse(data)
    }
    private val pathsContext: DocumentContext by lazy {
        JaywayJsonPath.using(pathsConfig).parse(data)
    }
    private val unlimitedContext: DocumentContext by lazy {
        JaywayJsonPath.using(unlimitedConfig).parse(data)
    }

    override fun get(path: JaywayJsonPath): DataValue {
        return try {
            valuesContext.read(path) ?: DataValue.NULL
        } catch (e: PathNotFoundException) {
            DataValue.NULL
        }
    }

    override fun get(path: String): DataValue {
        return try {
            valuesContext.read(path) ?: DataValue.NULL
        } catch (e: PathNotFoundException) {
            DataValue.NULL
        }
    }

    override fun getPaths(path: String): List<String> {
        return try {
            pathsContext.read<DataValue>(path)?.toStrList() ?: emptyList()
        } catch (e: PathNotFoundException) {
            emptyList()
        }
    }

    override fun put(path: String, key: String, value: Any?) {
        valuesContext.put(path, key, toDataValue(value))
    }

    override fun renameKey(path: String, oldKey: String, newKey: String) {
        valuesContext.renameKey(path, oldKey, newKey)
    }

    override fun set(path: String, value: Any?) {
        set(JaywayJsonPath.compile(path), value)
    }

    override fun set(path: JaywayJsonPath, value: Any?) {
        if (path.isDefinite) {
            unlimitedContext.set(path, toDataValue(value))
        } else {
            valuesContext.set(path, toDataValue(value))
        }
    }

    override fun delete(path: String) {
        try {
            valuesContext.delete(path)
        } catch (e: PathNotFoundException) {
            // do nothing
        }
    }

    override fun add(path: String, element: Any?) {
        valuesContext.add(path, toDataValue(element))
    }

    override fun addAll(path: String, elements: Iterable<Any>) {
        elements.forEach {
            valuesContext.add(path, toDataValue(it))
        }
    }

    private fun toDataValue(value: Any?): DataValue {
        return Json.mapper.convert(value, DataValue::class.java) ?: DataValue.NULL
    }
}
