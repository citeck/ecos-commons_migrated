package ru.citeck.ecos.commons.utils.io

import java.io.*
import java.nio.charset.Charset

object IOUtils {

    /**
     * The default buffer size ({@value}) to use for copyLarge
     */
    private const val DEFAULT_BUFFER_SIZE = 1024 * 4

    /**
     * Represents the end-of-file (or stream)
     */
    const val EOF = -1

    @JvmStatic
    @JvmOverloads
    fun readAsString(input: InputStream?, charset: Charset = Charsets.UTF_8): String {
        if (input == null) {
            return ""
        }
        return StringBuilderWriter().use { output ->
            copy(input, output, charset)
            output.toString()
        }
    }

    @JvmStatic
    fun readAsBytes(input: InputStream?): ByteArray {
        if (input == null) {
            return ByteArray(0)
        }
        return ByteArrayOutputStream().use { output ->
            copy(input, output)
            output.toByteArray()
        }
    }

    @JvmStatic
    fun toInStream(array: ByteArray): InputStream {
        return ByteArrayInputStream(array)
    }

    // copy from InputStream
    // -----------------------------------------------------------------------
    /**
     * Copies bytes from an `InputStream` to an
     * `OutputStream`.
     *
     *
     * This method buffers the input internally, so there is no need to use a
     * `BufferedInputStream`.
     *
     *
     * Large streams (over 2GB) will return a bytes copied value of
     * `-1` after the copy has completed since the correct
     * number of bytes cannot be returned as an int. For large streams
     * use the `copyLarge(InputStream, OutputStream)` method.
     *
     * @param input the `InputStream` to read from
     * @param output the `OutputStream` to write to
     * @return the number of bytes copied, or -1 if &gt; Integer.MAX_VALUE
     * @throws NullPointerException if the input or output is null
     * @throws IOException          if an I/O error occurs
     * @since 1.1
     */
    @JvmStatic
    fun copy(input: InputStream, output: OutputStream): Int {
        val count: Long = copyLarge(input, output)
        return if (count > Int.MAX_VALUE) {
            -1
        } else {
            count.toInt()
        }
    }

    /**
     * Copies bytes from an `InputStream` to an `OutputStream` using an internal buffer of the
     * given size.
     *
     *
     * This method buffers the input internally, so there is no need to use a `BufferedInputStream`.
     *
     *
     *
     * @param input the `InputStream` to read from
     * @param output the `OutputStream` to write to
     * @param bufferSize the bufferSize used to copy from the input to the output
     * @return the number of bytes copied
     * @throws NullPointerException if the input or output is null
     * @throws IOException          if an I/O error occurs
     * @since 2.5
     */
    @JvmStatic
    fun copy(input: InputStream, output: OutputStream, bufferSize: Int): Long {
        return copyLarge(input, output, ByteArray(bufferSize))
    }

    /**
     * Copies bytes from an `InputStream` to chars on a
     * `Writer` using the specified character encoding.
     *
     *
     * This method buffers the input internally, so there is no need to use a
     * `BufferedInputStream`.
     *
     *
     * This method uses [InputStreamReader].
     *
     * @param input the `InputStream` to read from
     * @param output the `Writer` to write to
     * @param inputEncoding the encoding to use for the input stream, null means platform default
     * @throws NullPointerException if the input or output is null
     * @throws IOException          if an I/O error occurs
     * @since 2.3
     */
    @JvmStatic
    @JvmOverloads
    fun copy(input: InputStream, output: Writer, inputEncoding: Charset = Charsets.UTF_8) {
        copy(InputStreamReader(input, inputEncoding), output)
    }

    // copy from Reader
    // -----------------------------------------------------------------------
    /**
     * Copies chars from a `Reader` to a `Writer`.
     *
     * This method buffers the input internally, so there is no need to use a
     * `BufferedReader`.
     *
     * Large streams (over 2GB) will return a chars copied value of
     * `-1` after the copy has completed since the correct
     * number of chars cannot be returned as an int. For large streams
     * use the `copyLarge(Reader, Writer)` method.
     *
     * @param input the `Reader` to read from
     * @param output the `Writer` to write to
     * @return the number of characters copied, or -1 if &gt; Integer.MAX_VALUE
     * @throws NullPointerException if the input or output is null
     * @throws IOException          if an I/O error occurs
     * @since 1.1
     */
    @JvmStatic
    fun copy(input: Reader, output: Writer): Int {
        val count = copyLarge(input, output)
        return if (count > Int.MAX_VALUE) {
            -1
        } else count.toInt()
    }

    /**
     * Copies bytes from a large (over 2GB) `InputStream` to an
     * `OutputStream`.
     *
     * This method buffers the input internally, so there is no need to use a
     * `BufferedInputStream`.
     *
     * The buffer size is given by [.DEFAULT_BUFFER_SIZE].
     *
     * @param input the `InputStream` to read from
     * @param output the `OutputStream` to write to
     * @return the number of bytes copied
     * @throws NullPointerException if the input or output is null
     * @throws IOException          if an I/O error occurs
     * @since 1.3
     */
    @JvmStatic
    fun copyLarge(input: InputStream, output: OutputStream): Long {
        return copy(input, output, DEFAULT_BUFFER_SIZE)
    }

    /**
     * Copies bytes from a large (over 2GB) `InputStream` to an
     * `OutputStream`.
     *
     *
     * This method uses the provided buffer, so there is no need to use a
     * `BufferedInputStream`.
     *
     * @param input the `InputStream` to read from
     * @param output the `OutputStream` to write to
     * @param buffer the buffer to use for the copy
     * @return the number of bytes copied
     * @throws NullPointerException if the input or output is null
     * @throws IOException          if an I/O error occurs
     * @since 2.2
     */
    @JvmStatic
    fun copyLarge(input: InputStream, output: OutputStream, buffer: ByteArray): Long {
        var count: Long = 0
        var readCount: Int = input.read(buffer)
        while (readCount != EOF) {
            output.write(buffer, 0, readCount)
            count += readCount.toLong()
            readCount = input.read(buffer)
        }
        return count
    }

    /**
     * Copies chars from a large (over 2GB) `Reader` to a `Writer`.
     *
     *
     * This method buffers the input internally, so there is no need to use a
     * `BufferedReader`.
     *
     *
     * The buffer size is given by [.DEFAULT_BUFFER_SIZE].
     *
     * @param input the `Reader` to read from
     * @param output the `Writer` to write to
     * @return the number of characters copied
     * @throws NullPointerException if the input or output is null
     * @throws IOException          if an I/O error occurs
     * @since 1.3
     */
    @JvmStatic
    fun copyLarge(input: Reader, output: Writer): Long {
        return copyLarge(input, output, CharArray(DEFAULT_BUFFER_SIZE))
    }

    /**
     * Copies chars from a large (over 2GB) `Reader` to a `Writer`.
     *
     *
     * This method uses the provided buffer, so there is no need to use a
     * `BufferedReader`.
     *
     *
     *
     * @param input the `Reader` to read from
     * @param output the `Writer` to write to
     * @param buffer the buffer to be used for the copy
     * @return the number of characters copied
     * @throws NullPointerException if the input or output is null
     * @throws IOException          if an I/O error occurs
     * @since 2.2
     */
    @JvmStatic
    fun copyLarge(input: Reader, output: Writer, buffer: CharArray): Long {
        var count = 0L
        var readCount = input.read(buffer)
        while (readCount != EOF) {
            output.write(buffer, 0, readCount)
            count += readCount.toLong()
            readCount = input.read(buffer)
        }
        return count
    }
}
