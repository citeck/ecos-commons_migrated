package ru.citeck.ecos.commons.json.path

import ecos.com.jayway.jsonpath.Configuration
import ecos.com.jayway.jsonpath.JsonPath
import ecos.com.jayway.jsonpath.Option
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.commons.json.JsonMapper

class JsonPathsImpl(val mapper: JsonMapper) : JsonPaths {

    private val pathProvider = JsonPathProvider(mapper)
    private val unlimitedPathProvider = JsonPathProvider(mapper, true)

    private val pathsConfig = Configuration.builder()
        .jsonProvider(pathProvider)
        .mappingProvider(pathProvider)
        .options(Option.AS_PATH_LIST)
        .build()
    private val valuesConfig = Configuration.builder()
        .jsonProvider(pathProvider)
        .mappingProvider(pathProvider)
        .build()
    private val unlimitedConfig = Configuration.builder()
        .jsonProvider(unlimitedPathProvider)
        .mappingProvider(unlimitedPathProvider)
        .build()

    override fun forData(data: Any?): DataContext {
        val value = Json.mapper.convert(data, DataValue::class.java) ?: DataValue.NULL
        return DataContextImpl(value, valuesConfig, pathsConfig, unlimitedConfig)
    }

    override fun isDefinite(path: String): Boolean {
        return JsonPath.compile(path).isDefinite
    }
}
