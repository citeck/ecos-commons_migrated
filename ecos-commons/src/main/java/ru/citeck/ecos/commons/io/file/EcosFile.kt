package ru.citeck.ecos.commons.io.file

import ru.citeck.ecos.commons.io.file.filter.FileFilter
import ru.citeck.ecos.commons.utils.func.UncheckedConsumer
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Path

interface EcosFile {

    fun getParent(): EcosFile?

    fun getName(): String

    fun getPath(): Path

    fun copyFilesFrom(file: EcosFile)

    fun findFiles(pattern: String): List<EcosFile>

    fun findFiles(): List<EcosFile>

    fun findFilesWithDirs(): List<EcosFile>

    fun findFiles(filter: FileFilter): List<EcosFile>

    fun findFiles(filter: FileFilter, recursive: Boolean): List<EcosFile>

    fun createFile(path: String, data: String): EcosFile

    fun createFile(path: String, data: ByteArray): EcosFile

    fun createFile(path: String, data: EcosFile): EcosFile

    fun createFile(path: String, data: InputStream): EcosFile

    fun createFile(path: String, outWriter: (OutputStream) -> Unit): EcosFile

    fun createFileUch(path: String, outWriter: UncheckedConsumer<OutputStream>): EcosFile

    fun delete(): Boolean

    fun createDir(path: String): EcosFile

    fun getOrCreateDir(path: String): EcosFile

    fun getRoot(): EcosFile

    fun getFile(path: String): EcosFile?

    fun getDir(path: String): EcosFile?

    fun getFileOrDir(path: String): EcosFile?

    fun getChildren(): List<EcosFile>

    fun isDirectory(): Boolean

    fun readAsBytes(): ByteArray

    fun readAsString(): String

    fun <T : Any> read(handler: (InputStream) -> T?): T?

    fun getLastAccessTime(): Long

    fun setLastAccessTime(time: Long)

    fun getLastModifiedTime(): Long

    fun setLastModifiedTime(time: Long)

    fun getCreationTime(): Long

    fun setCreationTime(time: Long)
}
