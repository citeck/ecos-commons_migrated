package ru.citeck.ecos.commons.utils

object ExceptionUtils {

    @JvmStatic
    fun throwException(ex: Throwable) {
        throw ex
    }
}
