package ru.citeck.ecos.commons.json

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import ecos.com.fasterxml.jackson210.core.JsonGenerator
import ecos.com.fasterxml.jackson210.databind.SerializerProvider
import ecos.com.fasterxml.jackson210.databind.ser.std.StdSerializer

/**
 * Bridge between com.fasterxml and ecos.com.fasterxml
 */
class JsonNodeSerializer : StdSerializer<JsonNode>(JsonNode::class.java) {

    private val objectMapper = ObjectMapper()

    override fun serialize(
        node: JsonNode?,
        jsonGenerator: JsonGenerator,
        serializerProvider: SerializerProvider
    ) {

        val obj = objectMapper.treeToValue(node, Any::class.java)
        jsonGenerator.writeObject(obj)
    }
}
