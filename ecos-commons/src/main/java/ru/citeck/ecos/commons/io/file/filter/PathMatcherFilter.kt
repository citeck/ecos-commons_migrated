package ru.citeck.ecos.commons.io.file.filter

import ru.citeck.ecos.commons.io.file.EcosFile
import java.nio.file.*

class PathMatcherFilter(private val basePath: Path, pattern: String) : FileFilter {

    private val matcher: PathMatcher

    init {
        val validPattern = if (!pattern.contains(":")) {
            "glob:$pattern"
        } else {
            pattern
        }
        matcher = FileSystems.getDefault().getPathMatcher(validPattern)
    }

    override fun accept(file: EcosFile): Boolean {
        return matcher.matches(basePath.relativize(file.getPath()))
    }
}
