package ru.citeck.ecos.commons.json

import ru.citeck.ecos.commons.io.file.EcosFile

interface FileResolver {

    fun getFile(path: String): EcosFile?
}
