package ru.citeck.ecos.commons.utils.func

@FunctionalInterface
interface UncheckedSupplier<T> {

    @Throws(Exception::class)
    fun get(): T
}
