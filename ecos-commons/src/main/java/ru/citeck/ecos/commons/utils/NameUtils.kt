package ru.citeck.ecos.commons.utils

import java.util.concurrent.ConcurrentHashMap
import java.util.regex.Pattern

object NameUtils {

    const val DEFAULT_SPECIAL_CHARS = "`&*=%^#@|:;{}()[],.!?$-+\"' \n\t\\/<>"
    private val SPECIAL_CHAR_PATTERN = Pattern.compile("_u([A-Fa-f\\d]{4})_")

    private val escapers = ConcurrentHashMap<String, Escaper>()
    private val defaultEscaper: Escaper = getEscaper(DEFAULT_SPECIAL_CHARS)

    fun getEscaper(): Escaper {
        return defaultEscaper
    }

    fun getEscaper(specialChars: String): Escaper {
        return escapers.computeIfAbsent(specialChars) { Escaper(it) }
    }

    fun getEscaperWithAllowedChars(allowedChars: String): Escaper {
        var specialChars = DEFAULT_SPECIAL_CHARS
        for (ch in allowedChars) {
            specialChars = specialChars.replace(ch + "", "")
        }
        return escapers.computeIfAbsent(specialChars) { Escaper(it) }
    }

    private fun toHex(ch: Char): String {
        val code = Integer.toHexString(ch.code).uppercase()
        return "_u" + "0000".substring(code.length) + code + "_"
    }

    private fun toChar(hex: String): Char {
        return hex.toInt(16).toChar()
    }

    @JvmStatic
    fun escape(value: String): String {
        return defaultEscaper.escape(value)
    }

    @JvmStatic
    fun unescape(value: String): String {
        return defaultEscaper.unescape(value)
    }

    class Escaper(private val specialChars: String) {

        private val specialCharsHex = Array(specialChars.length) { toHex(specialChars[it]) }

        fun escape(str: String): String {
            var resStr = str
            for (i in specialChars.indices) {
                val ch = specialChars[i]
                val chStr = "" + ch
                if (resStr.contains(chStr)) {
                    resStr = resStr.replace(chStr, specialCharsHex[i])
                }
            }
            return resStr
        }

        fun unescape(str: String): String {
            var resStr = str
            val matcher = SPECIAL_CHAR_PATTERN.matcher(resStr)
            while (matcher.find()) {
                resStr = resStr.replace(
                    matcher.group(0),
                    toChar(matcher.group(1)).toString() + ""
                )
            }
            return resStr
        }
    }
}
