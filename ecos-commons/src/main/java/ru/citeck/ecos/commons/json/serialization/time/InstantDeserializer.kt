package ru.citeck.ecos.commons.json.serialization.time

import ecos.com.fasterxml.jackson210.core.JsonParser
import ecos.com.fasterxml.jackson210.core.JsonToken
import ecos.com.fasterxml.jackson210.databind.DeserializationContext
import ecos.com.fasterxml.jackson210.databind.deser.std.StdDeserializer
import java.time.Instant
import java.time.OffsetDateTime

class InstantDeserializer : StdDeserializer<Instant>(Instant::class.java) {

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Instant? {

        return when (p.currentToken) {
            JsonToken.VALUE_STRING -> {
                val text = p.text
                if (text.isEmpty()) {
                    null
                } else {
                    OffsetDateTime.parse(p.text).toInstant()
                }
            }
            JsonToken.VALUE_NUMBER_FLOAT, JsonToken.VALUE_NUMBER_INT -> {
                val secondsOrMs = p.valueAsDouble
                if (JsonTimeUtils.isNotTimeSeconds(secondsOrMs)) {
                    return Instant.ofEpochMilli(secondsOrMs.toLong())
                }
                Instant.ofEpochMilli((secondsOrMs * 1000).toLong())
            }
            else -> {
                ctxt.handleUnexpectedToken(Instant::class.java, p) as? Instant
            }
        }
    }
}
