package ru.citeck.ecos.commons.io.file.mem

import ru.citeck.ecos.commons.io.file.AbstractEcosFile
import java.time.Instant

abstract class MemFileBase(parent: AbstractEcosFile?, name: String) : AbstractEcosFile(parent, name) {

    private val createdTime = Instant.now().toEpochMilli()

    override fun getLastAccessTime(): Long = createdTime
    override fun getLastModifiedTime(): Long = createdTime
    override fun getCreationTime(): Long = createdTime

    override fun delete(): Boolean {
        val parent = getParent()
        if (parent !is EcosMemDir) {
            return false
        }
        return parent.removeChild(getName()) != null
    }
}
