package ru.citeck.ecos.commons.utils

import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.data.ObjectData

object TmplUtils {

    private const val CURLY_BRACE_OPEN = '{'
    private const val CURLY_BRACE_CLOSE = '}'

    private const val PH_START = "\$$CURLY_BRACE_OPEN"

    @JvmStatic
    fun getAtts(value: Any?): Set<String> {

        if (value == null) {
            return emptySet()
        }
        return getAtts(DataValue.create(value))
    }

    @JvmStatic
    @JvmOverloads
    fun getAtts(value: DataValue?, result: MutableSet<String> = HashSet()): Set<String> {

        if (value == null || value.isNull()) {
            return emptySet()
        }
        if (value.isArray()) {
            for (element in value) {
                getAtts(element, result)
            }
        } else if (value.isObject()) {
            value.forEach { _, v -> getAtts(v, result) }
        } else if (value.isTextual()) {
            getAtts(value.asText(), result)
        }
        return result
    }

    @JvmStatic
    @JvmOverloads
    fun getAtts(text: String?, result: MutableSet<String> = HashSet()): Set<String> {

        val firstPlaceholderIdx = text?.indexOf(PH_START) ?: -1
        if (text == null || firstPlaceholderIdx == -1) {
            return emptySet()
        }

        var idx = firstPlaceholderIdx
        while (idx > -1) {

            if (isEscapedChar(text, idx)) {
                idx = text.indexOf(PH_START, idx + 1)
                continue
            }

            val startIdx = idx + 2
            val endIdx = findPlaceholderEndIdx(startIdx, text)

            if (endIdx == -1) {
                break
            }
            val key = text.substring(startIdx, endIdx)
            if (key.isNotBlank()) {
                result.add(key)
            }
            idx = text.indexOf(PH_START, endIdx + 1)
        }

        return result
    }

    @JvmStatic
    fun <T : Any> applyAtts(value: T?, attributes: ObjectData): T? {

        if (value == null) {
            return value
        }

        val result: DataValue = applyAtts(DataValue.create(value), attributes)

        return if (result.isNull()) {
            value
        } else {
            result.getAs(value::class.java)
        }
    }

    @JvmStatic
    fun applyAtts(value: DataValue?, attributes: ObjectData): DataValue {

        if (value == null || value.isNull()) {
            return DataValue.NULL
        }
        if (value.isArray()) {
            val result = DataValue.createArr()
            for (element in value) {
                result.add(applyAtts(element, attributes))
            }
            return result
        }
        if (value.isObject()) {
            val result = DataValue.createObj()
            value.forEach { k, v ->
                result.set(k, applyAtts(v, attributes))
            }
            return result
        } else if (value.isTextual()) {
            return applyAtts(value.asText(), attributes)
        }
        return value.copy()
    }

    @JvmStatic
    fun applyAtts(value: String?, attributes: ObjectData): DataValue {

        if (value == null) {
            return DataValue.NULL
        }
        val firstPlaceholderIdx = value.indexOf(PH_START)
        if (value.isBlank() || firstPlaceholderIdx == -1) {
            return DataValue.createStr(value)
        }
        if (firstPlaceholderIdx == 0 &&
            findPlaceholderEndIdx(2, value) == value.length - 1
        ) {

            return attributes.get(value.substring(2, value.length - 1))
        }

        val sb = StringBuilder()
        var prevIdx = 0
        var idx = firstPlaceholderIdx
        while (idx >= 0) {
            if (isEscapedChar(value, idx)) {
                if (idx > prevIdx + 1) {
                    sb.append(value, prevIdx, idx - 1)
                    prevIdx = idx
                } else {
                    prevIdx++
                }
                idx = value.indexOf(PH_START, idx + 1)
                continue
            }
            if (idx > prevIdx) {
                sb.append(value, prevIdx, idx)
                prevIdx = idx
            }
            idx = findPlaceholderEndIdx(idx + 2, value)
            if (idx != -1) {
                val key = value.substring(prevIdx + 2, idx)
                if (key.isNotBlank()) {
                    sb.append(attributes.get(key).asText())
                }
                prevIdx = idx + 1
                idx = value.indexOf(PH_START, idx + 1)
            }
        }
        if (prevIdx < value.length - 1) {
            sb.append(value, prevIdx, value.length)
        }
        return DataValue.createStr(sb.toString())
    }

    private fun findPlaceholderEndIdx(fromIdx: Int, text: String): Int {

        var openInnerBraces = 0
        var currentIndex = fromIdx
        var endIdx = -1

        while (currentIndex < text.length) {
            val char = text[currentIndex]

            if (char == CURLY_BRACE_OPEN) {
                openInnerBraces++
            } else if (char == CURLY_BRACE_CLOSE) {
                if (openInnerBraces > 0) {
                    openInnerBraces--
                } else {
                    endIdx = currentIndex
                    break
                }
            }
            currentIndex++
        }

        return endIdx
    }

    private fun isEscapedChar(value: String, idx: Int): Boolean {
        var backSlashesCount = 0
        var backSlashesIdx = idx
        while (backSlashesIdx > 0 && value[backSlashesIdx - 1] == '\\') {
            backSlashesCount++
            backSlashesIdx--
        }
        return backSlashesCount % 2 == 1
    }
}
