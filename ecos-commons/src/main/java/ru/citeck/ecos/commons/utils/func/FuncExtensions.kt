package ru.citeck.ecos.commons.utils.func

fun <T, R> UncheckedFunction<T, R>.toFunc(): (T) -> R = { this.apply(it) }

fun <T> UncheckedConsumer<T>.toFunc(): (T) -> Unit = { this.accept(it) }

fun <T> UncheckedSupplier<T>.toFunc(): () -> T = { this.get() }
