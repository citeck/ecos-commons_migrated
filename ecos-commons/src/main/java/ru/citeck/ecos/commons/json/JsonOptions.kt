package ru.citeck.ecos.commons.json

import ecos.com.fasterxml.jackson210.core.JsonFactory
import ecos.com.fasterxml.jackson210.databind.JsonDeserializer
import ecos.com.fasterxml.jackson210.databind.JsonSerializer
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

data class JsonOptions(
    val deserFeatures: Map<DeserFeature, Boolean>,
    val serialFeatures: Map<SerialFeature, Boolean>,
    val serializers: List<JsonSerializer<*>>,
    val deserializers: List<JsonDeserializer<*>>,
    val excludedSerializers: Set<Class<*>>,
    val excludedDeserializers: Set<Class<*>>,
    val factory: JsonFactory?
) {

    companion object {

        @JvmStatic
        fun create(init: Builder.() -> Unit): JsonOptions {
            return Builder().apply(init).build()
        }

        @JvmStatic
        fun create(): Builder {
            return Builder()
        }
    }

    class Builder {

        private val deserFeatures = HashMap<DeserFeature, Boolean>()
        private val serialFeatures = HashMap<SerialFeature, Boolean>()

        private val deserializers = ArrayList<JsonDeserializer<*>>()
        private val serializers = ArrayList<JsonSerializer<*>>()

        private val excludedSerializers = HashSet<Class<*>>()
        private val excludedDeserializers = HashSet<Class<*>>()

        private var factory: JsonFactory? = null

        fun build(): JsonOptions {

            return JsonOptions(
                deserFeatures = Collections.unmodifiableMap(deserFeatures),
                serialFeatures = Collections.unmodifiableMap(serialFeatures),
                serializers = Collections.unmodifiableList(serializers),
                deserializers = Collections.unmodifiableList(deserializers),
                excludedSerializers = Collections.unmodifiableSet(excludedSerializers),
                excludedDeserializers = Collections.unmodifiableSet(excludedDeserializers),
                factory = factory
            )
        }

        fun setFactory(factory: JsonFactory): Builder {
            this.factory = factory
            return this
        }

        fun add(serializer: JsonSerializer<*>): Builder {
            this.serializers.add(serializer)
            return this
        }

        fun add(deserializer: JsonDeserializer<*>): Builder {
            this.deserializers.add(deserializer)
            return this
        }

        fun excludeSerializers(type: Class<*>): Builder {
            this.excludedSerializers.add(type)
            return this
        }

        fun excludeDeserializers(type: Class<*>): Builder {
            this.excludedDeserializers.add(type)
            return this
        }

        fun enable(feature: DeserFeature): Builder {
            deserFeatures[feature] = true
            return this
        }

        fun enable(feature: SerialFeature): Builder {
            serialFeatures[feature] = true
            return this
        }

        fun disable(feature: DeserFeature): Builder {
            deserFeatures[feature] = false
            return this
        }

        fun disable(feature: SerialFeature): Builder {
            serialFeatures[feature] = false
            return this
        }
    }
}
