package ru.citeck.ecos.commons.io.file.filter

import ru.citeck.ecos.commons.io.file.EcosFile

@FunctionalInterface
interface FileFilter {

    fun accept(file: EcosFile): Boolean
}
