package ru.citeck.ecos.commons.data

import ecos.com.fasterxml.jackson210.annotation.JsonValue
import java.util.regex.Pattern

class Version(value: String) {

    companion object {
        private val pattern = Pattern.compile("^\\d+(\\.\\d+)*$")
    }

    private val values: IntArray
    private val strValue: String

    init {
        require(pattern.matcher(value).matches()) {
            "Incorrect version: $value"
        }
        strValue = value
        values = value.split(".").map { it.toInt() }.toIntArray()
    }

    fun isAfterOrEqual(other: Version): Boolean {
        val maxSize = other.values.size.coerceAtLeast(values.size)
        val first = getIntValue(maxSize)
        val second = other.getIntValue(maxSize)
        for (i in first.indices) {
            if (first[i] > second[i]) {
                return true
            } else if (first[i] < second[i]) {
                return false
            }
        }
        return true
    }

    private fun getIntValue(size: Int): IntArray {
        if (values.size == size) {
            return values
        }
        val result = IntArray(size)
        System.arraycopy(values, 0, result, 0, values.size)
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }
        val version = other as Version
        val maxSize = version.values.size.coerceAtLeast(values.size)
        return getIntValue(maxSize).contentEquals(version.getIntValue(maxSize))
    }

    override fun hashCode(): Int {
        var result = 0
        var lastIdx = values.size - 1
        while (lastIdx >= 0 && values[lastIdx] == 0) {
            lastIdx--
        }
        if (lastIdx == -1) {
            return 0
        }
        for (i in 0..lastIdx) {
            result = 31 * result + Integer.hashCode(values[i])
        }
        return result
    }

    @JsonValue
    override fun toString(): String {
        return strValue
    }
}
