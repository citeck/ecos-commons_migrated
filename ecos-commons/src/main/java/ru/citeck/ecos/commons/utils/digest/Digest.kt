package ru.citeck.ecos.commons.utils.digest

data class Digest(

    val hash: String,

    /**
     * Bytes count in digest
     */
    val size: Long = 0,

    val algorithm: DigestAlgorithm
)

enum class DigestAlgorithm(val code: String) {
    SHA256("SHA-256"),
    MD5("MD5")
}
