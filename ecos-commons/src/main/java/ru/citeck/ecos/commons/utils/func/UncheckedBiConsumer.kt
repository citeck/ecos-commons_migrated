package ru.citeck.ecos.commons.utils.func

@FunctionalInterface
interface UncheckedBiConsumer<T0, T1> {

    @Throws(Exception::class)
    fun accept(arg0: T0, arg1: T1)
}
