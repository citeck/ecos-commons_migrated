package ru.citeck.ecos.commons.json

class ObjectKeyGenerator {

    companion object {
        private const val CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        private const val CHARS_LEN = CHARS.length
    }

    private var numValue = -1
    private val value = StringBuilder()

    fun incrementAndGet(): String {

        value.setLength(0)
        var left = ++numValue

        do {
            value.append(CHARS[left % CHARS_LEN])
            left /= CHARS_LEN
        } while (left > 0)

        return value.toString()
    }
}
