package ru.citeck.ecos.commons.json

import ecos.com.fasterxml.jackson210.datatype.jsr310.JavaTimeModule
import ecos.com.fasterxml.jackson210.module.kotlin.KotlinModule
import mu.KotlinLogging
import ru.citeck.ecos.commons.json.path.JsonPaths
import ru.citeck.ecos.commons.json.path.JsonPathsImpl
import ru.citeck.ecos.commons.json.serialization.time.InstantDeserializer
import ru.citeck.ecos.commons.json.serialization.time.InstantSerializer
import ru.citeck.ecos.commons.utils.LibsUtils
import java.time.Instant
import java.util.function.Consumer
import com.fasterxml.jackson.databind.JsonNode as JksJsonNode
import com.fasterxml.jackson.databind.node.ArrayNode as JksArrayNode
import com.fasterxml.jackson.databind.node.BigIntegerNode as JksBigIntegerNode
import com.fasterxml.jackson.databind.node.BinaryNode as JksBinaryNode
import com.fasterxml.jackson.databind.node.BooleanNode as JksBooleanNode
import com.fasterxml.jackson.databind.node.DoubleNode as JksDoubleNode
import com.fasterxml.jackson.databind.node.FloatNode as JksFloatNode
import com.fasterxml.jackson.databind.node.IntNode as JksIntNode
import com.fasterxml.jackson.databind.node.LongNode as JksLongNode
import com.fasterxml.jackson.databind.node.ObjectNode as JksObjectNode
import com.fasterxml.jackson.databind.node.POJONode as JksPOJONode
import com.fasterxml.jackson.databind.node.ShortNode as JksShortNode
import com.fasterxml.jackson.databind.node.TextNode as JksTextNode

private val log = KotlinLogging.logger {}

object Json {

    private val DEFAULT_OPTIONS = JsonOptions.create {
        disable(DeserFeature.FAIL_ON_UNKNOWN_PROPERTIES)
    }

    @JvmStatic
    val context = MappingContext()
    @JvmStatic
    val mapper: JsonMapper = JsonMapperImpl(context, DEFAULT_OPTIONS)
    @JvmStatic
    val paths: JsonPaths = JsonPathsImpl(mapper)

    @JvmStatic
    fun newMapper(options: JsonOptions): JsonMapper {
        val context = MappingContext(context)
        options.serializers.forEach { context.addSerializer(it) }
        options.deserializers.forEach { context.addDeserializer(it) }
        context.addExcludedSerializers(options.excludedSerializers)
        context.addExcludedDeserializers(options.excludedDeserializers)
        return JsonMapperImpl(context, options)
    }

    @JvmStatic
    fun newMapperJ(build: Consumer<JsonOptions.Builder>): JsonMapper {
        val builder = JsonOptions.Builder()
        build.accept(builder)
        return newMapper(builder.build())
    }

    @JvmStatic
    fun newMapper(build: JsonOptions.Builder.() -> Unit): JsonMapper {
        return newMapper(JsonOptions.create(build))
    }

    @JvmStatic
    fun newMapper(): JsonMapper {
        return JsonMapperImpl(MappingContext(context), DEFAULT_OPTIONS)
    }

    init {
        if (LibsUtils.isJacksonPresent()) {

            context.addSerializer(JsonNodeSerializer())

            context.addDeserializer(JsonNodeDeserializer(JksJsonNode::class.java))
            context.addDeserializer(JsonNodeDeserializer(JksArrayNode::class.java))
            context.addDeserializer(JsonNodeDeserializer(JksObjectNode::class.java))
            context.addDeserializer(JsonNodeDeserializer(JksBinaryNode::class.java))
            context.addDeserializer(JsonNodeDeserializer(JksBigIntegerNode::class.java))
            context.addDeserializer(JsonNodeDeserializer(JksBooleanNode::class.java))
            context.addDeserializer(JsonNodeDeserializer(JksDoubleNode::class.java))
            context.addDeserializer(JsonNodeDeserializer(JksFloatNode::class.java))
            context.addDeserializer(JsonNodeDeserializer(JksIntNode::class.java))
            context.addDeserializer(JsonNodeDeserializer(JksLongNode::class.java))
            context.addDeserializer(JsonNodeDeserializer(JksPOJONode::class.java))
            context.addDeserializer(JsonNodeDeserializer(JksShortNode::class.java))
            context.addDeserializer(JsonNodeDeserializer(JksTextNode::class.java))
        } else {
            log.trace { "Jackson library is not found. Bridge converters won't be registered." }
        }
        context.addModule(KotlinModule())

        val timeModule = JavaTimeModule()

        timeModule.addSerializer(InstantSerializer())
        timeModule.addDeserializer(Instant::class.java, InstantDeserializer())

        context.addModule(timeModule)
    }
}
