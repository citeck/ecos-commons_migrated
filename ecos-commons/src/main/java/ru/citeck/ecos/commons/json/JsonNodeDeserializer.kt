package ru.citeck.ecos.commons.json

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import ecos.com.fasterxml.jackson210.core.JsonParser
import ecos.com.fasterxml.jackson210.databind.DeserializationContext
import ecos.com.fasterxml.jackson210.databind.deser.std.StdDeserializer
import ru.citeck.ecos.commons.json.Json.mapper

/**
 * Bridge between com.fasterxml and ecos.com.fasterxml
 */
class JsonNodeDeserializer<T : JsonNode>(type: Class<T>) : StdDeserializer<T>(type) {

    private val objectMapper = ObjectMapper()

    override fun deserialize(
        jsonParser: JsonParser,
        deserializationContext: DeserializationContext
    ): T {

        val node = jsonParser.readValueAsTree<ecos.com.fasterxml.jackson210.databind.JsonNode>()
        val nodeObj = mapper.toJava(node)

        return objectMapper.valueToTree(nodeObj)
    }
}
