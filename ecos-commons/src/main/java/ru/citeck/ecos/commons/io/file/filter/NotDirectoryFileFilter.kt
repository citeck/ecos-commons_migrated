package ru.citeck.ecos.commons.io.file.filter

import ru.citeck.ecos.commons.io.file.EcosFile

class NotDirectoryFileFilter private constructor() : FileFilter {

    companion object {
        val INSTANCE = NotDirectoryFileFilter()
    }

    override fun accept(file: EcosFile) = !file.isDirectory()
}
