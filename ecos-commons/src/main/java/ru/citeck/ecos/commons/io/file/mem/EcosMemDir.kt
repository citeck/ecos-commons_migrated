package ru.citeck.ecos.commons.io.file.mem

import ru.citeck.ecos.commons.io.file.AbstractEcosFile
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.filter.FileFilter
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.lang.UnsupportedOperationException

class EcosMemDir @JvmOverloads constructor(parent: EcosMemDir?, name: String = "") : MemFileBase(parent, name) {

    constructor() : this(null)

    private val children: MutableMap<String, MemFileBase> = HashMap()

    override fun createFileImpl(name: String, outWriter: (OutputStream) -> Unit): EcosFile {

        val stream = ByteArrayOutputStream()
        stream.use(outWriter)

        val file = EcosMemFile(this, name, stream.toByteArray())
        children[name] = file

        return file
    }

    override fun createDirImpl(name: String): AbstractEcosFile {
        val dir = EcosMemDir(this, name)
        children[name] = dir
        return dir
    }

    override fun getChild(name: String): AbstractEcosFile? {
        return children[name]
    }

    override fun findFiles(filter: FileFilter, recursive: Boolean): List<EcosFile> {
        val result = ArrayList<EcosFile>()
        for (file in children.values) {
            if (filter.accept(file)) {
                result.add(file)
            }
            if (file.isDirectory() && recursive) {
                result.addAll(file.findFiles(filter, recursive))
            }
        }
        return result
    }

    override fun getChildren(): List<EcosFile> {
        return children.values.toList()
    }

    override fun isDirectory(): Boolean = true

    override fun <T : Any> read(handler: (InputStream) -> T?): T? {
        throw UnsupportedOperationException("read")
    }

    fun removeChild(name: String): MemFileBase? {
        return children.remove(name)
    }
}
