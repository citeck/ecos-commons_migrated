package ru.citeck.ecos.commons.utils

import java.util.*

object DataUriUtil {

    const val DATA_PREFIX = "data:"

    private const val BASE64_EXT = ";base64"

    private val EMPTY_INFO = DataUriInfo(null, "")

    /**
     * URI Example:
     * - data:application/json;base64,e30=
     *
     * Return null if parsing is not successful
     */
    fun parseData(fullData: String): DataUriInfo {

        if (!fullData.startsWith(DATA_PREFIX)) {
            return EMPTY_INFO
        }

        val commaIdx = fullData.indexOf(",")
        if (commaIdx == -1) {
            return EMPTY_INFO
        }

        val dataPrefix = fullData.substring(DATA_PREFIX.length, commaIdx)
        if (!dataPrefix.endsWith(BASE64_EXT)) {
            return EMPTY_INFO
        }
        val mimeType = dataPrefix.substring(0, dataPrefix.length - BASE64_EXT.length)

        val dataBytes = if (commaIdx == fullData.length - 1) {
            ByteArray(0)
        } else {
            Base64.getDecoder().decode(fullData.substring(commaIdx + 1))
        }

        return DataUriInfo(dataBytes, mimeType)
    }

    class DataUriInfo(
        val data: ByteArray?,
        val mimeType: String
    )
}
