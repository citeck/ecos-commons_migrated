package ru.citeck.ecos.commons.data

import ecos.com.fasterxml.jackson210.annotation.JsonCreator
import ecos.com.fasterxml.jackson210.annotation.JsonValue
import ecos.com.fasterxml.jackson210.databind.node.ObjectNode
import ru.citeck.ecos.commons.json.Json.mapper
import java.util.*
import java.util.function.BiConsumer

class ObjectData private constructor(
    private val data: DataValue = DataValue.createObj()
) {

    companion object {

        @JvmStatic
        fun deepCopy(data: ObjectData?): ObjectData? {
            return data?.deepCopy()
        }

        @JvmStatic
        fun deepCopyOrNew(data: ObjectData?): ObjectData {
            return data?.deepCopy() ?: ObjectData()
        }

        @JvmStatic
        @JsonCreator
        @com.fasterxml.jackson.annotation.JsonCreator
        fun create(content: Any?): ObjectData {
            val jsonObj = mapper.convert(content, ObjectNode::class.java)
            return if (jsonObj == null || !jsonObj.isObject) {
                ObjectData()
            } else {
                ObjectData(DataValue.create(jsonObj))
            }
        }

        @JvmStatic
        fun create(): ObjectData {
            return ObjectData()
        }
    }

    fun <T> map(func: (String, DataValue) -> T): List<T> {
        val result = ArrayList<T>()
        forEach { k, v -> result.add(func.invoke(k, v)) }
        return result
    }

    fun forEach(consumer: (String, DataValue) -> Unit) {
        val names = this.data.fieldNames()
        while (names.hasNext()) {
            val name = names.next()
            consumer.invoke(name, data.get(name))
        }
    }

    fun forEachJ(consumer: BiConsumer<String, DataValue>) {
        forEach { k, v -> consumer.accept(k, v) }
    }

    fun renameKey(path: String, oldKey: String, newKey: String): ObjectData {
        data.renameKey(path, oldKey, newKey)
        return this
    }

    fun setStr(path: String, value: String): ObjectData {
        data.setStr(path, value)
        return this
    }

    fun set(path: String, key: String, value: Any?): ObjectData {
        data.set(path, key, value)
        return this
    }

    fun set(path: String, value: Any?): ObjectData {
        data.set(path, value)
        return this
    }

    fun insert(path: String, idx: Int, value: Any): ObjectData {
        data.insert(path, idx, value)
        return this
    }

    fun insertAll(path: String, idx: Int, values: Iterable<Any>): ObjectData {
        data.insertAll(path, idx, values)
        return this
    }

    fun addAll(path: String, values: Iterable<Any>) {
        data.addAll(path, values)
    }

    fun add(path: String, value: Any?) {
        data.add(path, value)
    }

    /**
     * @param path json path or object key
     */
    fun remove(path: String): ObjectData {
        data.remove(path)
        return this
    }

    fun <T : Any> get(path: String, type: Class<T>, orElse: T?): T? {
        return data.get(path, type, orElse)
    }

    fun <T : Any> get(path: String, type: Class<T>): T? {
        return get(path, type, null)
    }

    fun <T : Any> get(path: String, orElse: T): T {
        return get(path, orElse.javaClass, orElse) ?: orElse
    }

    fun get(path: String): DataValue {
        return data.get(path)
    }

    fun getFirst(field: String): DataValue {
        return data.getFirst(field)
    }

    fun getPaths(path: String): List<String> {
        return data.getPaths(path)
    }

    fun fieldNamesList(): List<String> {
        return data.fieldNamesList()
    }

    fun fieldNames(): Iterator<String> {
        return data.fieldNames()
    }

    fun <T : Any> getAs(type: Class<T>): T? {
        return mapper.convert(data, type)
    }

    fun size(): Int {
        return data.size()
    }

    fun deepCopy(): ObjectData {
        return create(mapper.copy(data))
    }

    @com.fasterxml.jackson.annotation.JsonValue
    fun asJavaObj(): Any? {
        return data.asJavaObj()
    }

    fun has(name: String): Boolean {
        return data.has(name)
    }

    @JsonValue
    fun getData(): DataValue {
        return data
    }

    fun toPrettyString(): String {
        return mapper.toPrettyString(data) ?: ""
    }

    override fun toString(): String {
        return mapper.toString(data) ?: ""
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }
        val that = other as ObjectData
        return data == that.data
    }

    override fun hashCode(): Int {
        return Objects.hash(data)
    }
}
