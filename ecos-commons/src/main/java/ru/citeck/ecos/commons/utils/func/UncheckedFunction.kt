package ru.citeck.ecos.commons.utils.func

@FunctionalInterface
interface UncheckedFunction<T, R> {

    @Throws(Exception::class)
    fun apply(arg: T): R
}
