package ru.citeck.ecos.commons.utils

import jdk.nashorn.api.scripting.ScriptObjectMirror
import jdk.nashorn.internal.objects.NativeArray
import jdk.nashorn.internal.runtime.ScriptObject
import mu.KotlinLogging
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.json.Json
import java.lang.IllegalArgumentException
import java.util.*
import javax.script.Bindings
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager
import javax.script.SimpleBindings
import kotlin.collections.HashMap
import kotlin.collections.LinkedHashMap

object ScriptUtils {

    private val log = KotlinLogging.logger {}
    private val context = ThreadLocal.withInitial { ThreadContext() }

    @JvmStatic
    @JvmOverloads
    fun eval(script: String, engine: String? = null, bindings: Bindings? = null): DataValue {

        var engineId = engine ?: "js"
        if (engineId.isBlank()) {
            engineId = "js"
        }

        val scriptEngine = context.get().getEngine(engineId).orElseThrow {
            return@orElseThrow IllegalArgumentException("Engine with id '$engineId' is not registered")
        }

        var bindingsToEval = bindings
        if (bindingsToEval == null) {
            bindingsToEval = SimpleBindings()
        }
        bindingsToEval["log"] = log

        val rawResult = scriptEngine.eval(prepareScript(script, engineId), bindingsToEval)
        return Json.mapper.convert(convertToJava(rawResult), DataValue::class.java) ?: DataValue.NULL
    }

    fun convertToJava(jsObj: Any?): Any? {

        if (jsObj is ScriptObjectMirror) {
            return if (jsObj.isArray) {
                val list = ArrayList<Any?>()
                for (entry in jsObj.entries) {
                    list.add(convertToJava(entry.value))
                }
                list
            } else {
                val map = LinkedHashMap<String, Any?>()
                for (entry in jsObj.entries) {
                    map[entry.key] = convertToJava(entry.value)
                }
                map
            }
        } else {
            return jsObj
        }
    }

    fun convertToScript(javaObj: Any?): Any? {

        javaObj ?: return null
        if (javaObj is ScriptObject) {
            return javaObj
        }

        var obj = javaObj

        if (obj is DataValue) {
            obj = obj.asJavaObj()
        }

        if (obj is Collection<*>) {
            val jArray = Array<Any?>(obj.size) { null }
            for ((idx, element) in obj.withIndex()) {
                jArray[idx] = convertToScript(element)
            }
            return NativeArray.construct(true, null, *jArray)
        }

        if (obj is Map<*, *>) {

            val result = LinkedHashMap<Any?, Any?>()
            obj.forEach { (k, v) -> result[k] = convertToScript(v) }
            return result
        }

        return obj
    }

    @JvmStatic
    fun eval(script: String, model: Map<String, Any?>): DataValue {
        return eval(script, null, SimpleBindings(LinkedHashMap(model)))
    }

    private fun prepareScript(script: String, engine: String): String {
        if (engine == "js") {
            if (script.contains("return ")) {
                return "(function(){$script})()"
            }
        }
        return script
    }

    private class ThreadContext {

        val manager = ScriptEngineManager()
        val engines: MutableMap<String, Optional<ScriptEngine>> = HashMap()

        fun getEngine(id: String): Optional<ScriptEngine> {
            val innerId = if (id == "js") {
                "JavaScript"
            } else {
                id
            }
            return engines.computeIfAbsent(innerId) {
                Optional.ofNullable(manager.getEngineByName(it))
            }
        }
    }
}
