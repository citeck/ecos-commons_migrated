package ru.citeck.ecos.commons.io.file.filter

import ru.citeck.ecos.commons.io.file.EcosFile

class AlwaysTrueFileFilter private constructor() : FileFilter {

    companion object {
        val INSTANCE = AlwaysTrueFileFilter()
    }

    override fun accept(file: EcosFile) = true
}
