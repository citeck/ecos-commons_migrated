package ru.citeck.ecos.commons.utils.script

import mu.KLogger

class ScriptLogger(private val log: KLogger) {

    fun warn(msg: Any?) {
        msg ?: return
        log.warn(msg.toString())
    }

    fun debug(msg: Any?) {
        msg ?: return
        log.debug(msg.toString())
    }

    fun error(msg: Any?) {
        msg ?: return
        log.error(msg.toString())
    }

    fun trace(msg: Any?) {
        msg ?: return
        log.trace(msg.toString())
    }

    fun info(msg: Any?) {
        msg ?: return
        log.info(msg.toString())
    }
}
