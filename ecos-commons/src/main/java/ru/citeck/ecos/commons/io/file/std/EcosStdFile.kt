package ru.citeck.ecos.commons.io.file.std

import ru.citeck.ecos.commons.io.file.AbstractEcosFile
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.filter.FileFilter
import java.io.*
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes

class EcosStdFile @JvmOverloads constructor(
    val file: File,
    parent: EcosStdFile? = null
) : AbstractEcosFile(parent, file.name) {

    private val filePath: Path = file.toPath()
    private var attributesData: BasicFileAttributes? = null

    private val attributes: BasicFileAttributes
        get() {
            val data = attributesData
            return if (data == null) {
                val atts = Files.readAttributes(file.toPath(), BasicFileAttributes::class.java)
                attributesData = atts
                atts
            } else {
                data
            }
        }

    override fun findFiles(filter: FileFilter, recursive: Boolean): List<EcosFile> {

        val result = ArrayList<EcosFile>()

        file.walkTopDown()
            .maxDepth(100)
            .filter { it != file }
            .forEach {
                val file = getFileOrDir(filePath.relativize(it.toPath()).normalize().toString())
                if (file != null && filter.accept(file)) {
                    result.add(file)
                }
            }

        return result
    }

    override fun createFileImpl(name: String, outWriter: (OutputStream) -> Unit): EcosFile {

        val newFile = File(file, name)
        newFile.createNewFile()
        FileOutputStream(newFile).use(outWriter)

        return EcosStdFile(newFile, this)
    }

    override fun createDirImpl(name: String): AbstractEcosFile {
        val newFile = File(file, name)
        newFile.mkdir()
        return EcosStdFile(newFile, this)
    }

    override fun getChild(name: String): AbstractEcosFile? {
        val childFile = File(file, name)
        if (!childFile.exists()) {
            return null
        }
        return EcosStdFile(childFile, this)
    }

    override fun getChildren(): List<EcosFile> {
        val children = file.listFiles() ?: emptyArray()
        return children.map { EcosStdFile(it, this) }.toList()
    }

    override fun isDirectory(): Boolean = file.isDirectory

    override fun <T : Any> read(handler: (InputStream) -> T?): T? {
        return FileInputStream(file).use(handler)
    }

    override fun getLastModifiedTime(): Long {
        return file.lastModified()
    }

    override fun setLastModifiedTime(time: Long) {
        file.setLastModified(time)
        attributesData = null
    }

    override fun getLastAccessTime(): Long {
        return attributes.lastAccessTime().toMillis()
    }

    override fun getCreationTime(): Long {
        return attributes.creationTime().toMillis()
    }

    override fun delete(): Boolean {
        return if (file.isFile) {
            file.delete()
        } else {
            file.deleteRecursively()
        }
    }
}
