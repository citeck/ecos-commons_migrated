package ru.citeck.ecos.commons.utils

import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.mem.EcosMemDir
import ru.citeck.ecos.commons.utils.io.IOUtils
import java.io.*
import java.lang.IllegalArgumentException
import java.nio.charset.StandardCharsets
import java.nio.file.attribute.FileTime
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream

object ZipUtils {

    @JvmStatic
    fun extractZip(data: ByteArray?): EcosMemDir {
        if (data == null) {
            throw IllegalArgumentException("Cannot extract data from null.")
        } else {
            return extractZip(ByteArrayInputStream(data))
        }
    }

    @JvmStatic
    fun extractZip(zipStream: InputStream): EcosMemDir {
        return extractZip(zipStream, EcosMemDir())
    }

    @JvmStatic
    fun <T : EcosFile> extractZip(zipStream: InputStream, target: T): T {
        return extractZip(zipStream, target, ExtractConfig())
    }

    @JvmStatic
    fun <T : EcosFile> extractZip(zipStream: InputStream, target: T, config: ExtractConfig): T {

        ZipInputStream(zipStream, StandardCharsets.UTF_8).use { zis ->
            var ze = zis.nextEntry
            while (ze != null) {
                try {
                    if (!ze.isDirectory) {
                        val fileName = ze.name
                        val file = target.createFile(fileName) { out -> IOUtils.copy(zis, out) }
                        if (config.withFileTime) {
                            file.setCreationTime(ze.creationTime.toMillis())
                            file.setLastModifiedTime(ze.lastModifiedTime.toMillis())
                            file.setLastAccessTime(ze.lastAccessTime.toMillis())
                        }
                    }
                } finally {
                    zis.closeEntry()
                }
                ze = zis.nextEntry
            }
            zis.closeEntry()
        }
        return target
    }

    @JvmStatic
    fun writeZipAsBytes(directory: EcosFile): ByteArray {
        val outBytes = ByteArrayOutputStream()
        writeZip(directory, outBytes)
        return outBytes.toByteArray()
    }

    @JvmStatic
    fun writeZipAsBytes(directory: EcosFile, config: WriteConfig): ByteArray {
        val outBytes = ByteArrayOutputStream()
        writeZip(directory, outBytes, config)
        return outBytes.toByteArray()
    }

    @JvmStatic
    fun writeZip(directory: EcosFile, resultOut: OutputStream) {
        writeZip(directory, resultOut, WriteConfig())
    }

    @JvmStatic
    fun writeZipWithFileTime(directory: EcosFile, resultOut: OutputStream) {
        writeZip(directory, resultOut, WriteConfig(withFileTime = true))
    }

    @JvmStatic
    fun writeZip(directory: EcosFile, resultOut: OutputStream, config: WriteConfig) {

        val basePath = directory.getPath()

        ZipOutputStream(resultOut).use { out ->
            for (file in directory.findFiles()) {
                val localPath = basePath.relativize(file.getPath())
                val entry = ZipEntry(localPath.toString())
                if (config.withFileTime) {
                    entry.lastModifiedTime = FileTime.fromMillis(file.getLastModifiedTime())
                    entry.creationTime = FileTime.fromMillis(file.getCreationTime())
                    entry.lastAccessTime = FileTime.fromMillis(file.getLastAccessTime())
                } else {
                    val zeroTime = FileTime.fromMillis(0)
                    entry.lastModifiedTime = zeroTime
                    entry.creationTime = zeroTime
                    entry.lastAccessTime = zeroTime
                }
                out.putNextEntry(entry)
                file.read { IOUtils.copy(it, out) }
                out.closeEntry()
            }
        }
    }

    data class WriteConfig(
        val withFileTime: Boolean = false
    )

    data class ExtractConfig(
        val withFileTime: Boolean = false
    )
}
