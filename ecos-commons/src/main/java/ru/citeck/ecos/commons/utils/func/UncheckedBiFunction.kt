package ru.citeck.ecos.commons.utils.func

@FunctionalInterface
interface UncheckedBiFunction<T, K, R> {

    @Throws(Exception::class)
    fun apply(arg0: T, arg1: K): R
}
