package ru.citeck.ecos.commons.json

import ecos.org.snakeyaml.engine21.v2.api.*
import ecos.org.snakeyaml.engine21.v2.common.FlowStyle
import mu.KotlinLogging
import ru.citeck.ecos.commons.io.file.EcosFile
import java.lang.StringBuilder
import java.util.ArrayList

object YamlUtils {

    private val log = KotlinLogging.logger {}

    private const val YAML_INCLUDE_DIRECTIVE = "#include"
    private const val YAML_HEADER_KEY = "___header___"
    private val YAML_INCLUDE_PATTERN =
        "^([\\t ]*)(\\S+): (\\{\\}|\\[\\]|\"\"|'') (&\\S+ )?#include (.+)".toPattern()

    @JvmStatic
    fun toNonDefaultString(data: Any): String {
        return toString(Json.mapper.toNonDefaultJson(data))
    }

    fun toString(data: Any): String {
        val dump = Dump(
            DumpSettings.builder()
                .setExplicitStart(true)
                .setDefaultFlowStyle(FlowStyle.BLOCK)
                .setIndicatorIndent(2)
                .setIndent(2)
                .setIndentWithIndicator(true)
                .build()
        )
        val javaData = Json.mapper.toJava(Json.mapper.toJson(data))
        return dump.dumpToString(javaData)
    }

    fun read(file: EcosFile, context: MappingContext): Any? {
        return read(file.readAsString(), context, file)
    }

    fun read(yaml: String, context: MappingContext): Any {
        return read(yaml, context, null)
    }

    private fun read(yaml: String, context: MappingContext, file: EcosFile? = null): Any {

        val yamlLoad = Load(LoadSettings.builder().build())

        var value = yamlLoad.loadFromString(readYamlFileContent(yaml, context, file))

        if (value != null) {
            value = yamlPostProcess(value)
        }

        return value
    }

    private fun readYamlFileContent(yaml: String, context: MappingContext, yamlFile: EcosFile? = null): String {

        val result = StringBuilder()
        val lines = yaml.split("\n")

        lines.forEach { line ->

            val importIdx = line.indexOf(YAML_INCLUDE_DIRECTIVE)

            if (importIdx > 0) {

                val matcher = YAML_INCLUDE_PATTERN.matcher(line)

                if (matcher.matches()) {

                    val indent = matcher.group(1)
                    val field = matcher.group(2)
                    val valueType = matcher.group(3)
                    val anchor = matcher.group(4)
                    val filePath = matcher.group(5).trim()

                    result.append(indent).append(field).append(": ")
                    val errorInfo = "File: '$filePath'. Base file: '${yamlFile?.getPath()}'"

                    val importFileContent: String? = if (filePath.contains(":")) {
                        val fileResolver = context.getFileResolver()
                        if (fileResolver == null) {
                            log.error {
                                "File resolver is not defined. " +
                                    "Yaml include can't be resolved. " +
                                    "Line: '$line'"
                            }
                            null
                        } else {
                            val importFile = fileResolver.getFile(filePath)
                            when {
                                importFile == null -> {
                                    log.error { "File not found: '$filePath'. $errorInfo" }
                                    null
                                }
                                importFile.isDirectory() -> {
                                    log.error { "Include file should not be a directory. $errorInfo" }
                                    null
                                }
                                else -> {
                                    try {
                                        importFile.readAsString()
                                    } catch (e: Exception) {
                                        log.error { "Include file reading error. $errorInfo" }
                                        null
                                    }
                                }
                            }
                        }
                    } else {
                        val importFile = yamlFile?.getFile("../$filePath")
                        if (importFile == null) {
                            log.error { "File not found. $errorInfo" }
                            null
                        } else {
                            importFile.readAsString()
                        }
                    }

                    if (importFileContent == null) {
                        result.append(valueType)
                        if (anchor != null) {
                            result.append(" ").append(anchor)
                        }
                        result.append("\n")
                    } else {
                        if (anchor != null) {
                            result.append(anchor)
                        }
                        if (valueType == "\"\"" || valueType == "''") {
                            result.append(" |")
                        }
                        result.append("\n")

                        val importContentIndent = "$indent  "
                        importFileContent.split("\n").forEach {
                            result.append(importContentIndent).append(it).append("\n")
                        }
                    }
                } else {
                    result.append(line).append("\n")
                }
            } else {
                result.append(line).append("\n")
            }
        }

        return result.toString()
    }

    private fun yamlPostProcess(value: Any?): Any? {

        if (value == null) {
            return null
        }

        if (value is Map<*, *>) {
            val result = LinkedHashMap<String, Any?>()
            value.forEach { (k, v) ->
                if (k == "<<") {
                    var mergeValue = v
                    if (mergeValue is List<*>) {
                        val expandedMap = LinkedHashMap<String, Any>()
                        mergeValue.forEach { item ->
                            if (item is Map<*, *>) {
                                item.forEach { (innerK, innerV) ->
                                    expandedMap[innerK as String] = innerV as Any
                                }
                            }
                        }
                        mergeValue = expandedMap
                    }
                    if (mergeValue is Map<*, *>) {
                        mergeValue.forEach { (innerK, innerV) ->
                            innerK as String
                            if (!result.containsKey(innerK)) {
                                result[innerK] = yamlPostProcess(innerV as Any)
                            }
                        }
                    }
                } else if (k != YAML_HEADER_KEY) {
                    result[k as String] = yamlPostProcess(v)
                }
            }
            return result
        }
        if (value is List<*>) {
            val result = ArrayList<Any?>()
            for (elem in value) {
                result.add(yamlPostProcess(elem))
            }
            return result
        }

        return value
    }
}
