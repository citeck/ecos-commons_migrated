package ru.citeck.ecos.commons.test.data;

import org.junit.jupiter.api.Test;
import ru.citeck.ecos.commons.data.Version;

import static org.junit.jupiter.api.Assertions.*;

public class VersionTest {

    @Test
    void test() {

        Version version100 = new Version("1.0.0");
        Version version100_2 = new Version("1.0.0");

        assertEquals(version100, version100_2);
        assertTrue(version100.isAfterOrEqual(version100_2));
        assertTrue(version100.isAfterOrEqual(version100));
        assertTrue(version100_2.isAfterOrEqual(version100));

        Version version1000 = new Version("1.0.0.0");
        assertEquals(version100, version1000);

        assertTrue(version1000.isAfterOrEqual(version100));
        assertTrue(version100.isAfterOrEqual(version1000));

        Version version10001 = new Version("1.0.0.0.1");

        assertTrue(version10001.isAfterOrEqual(version100));
        assertTrue(version10001.isAfterOrEqual(version1000));
        assertFalse(version1000.isAfterOrEqual(version10001));
        assertFalse(version100.isAfterOrEqual(version10001));

        assertEquals(version100.hashCode(), version1000.hashCode());
        assertNotEquals(version100.hashCode(), version10001.hashCode());

        Version version1001000 = new Version("1.0.0.1.0.0.0");
        Version version1001 = new Version("1.0.0.1");

        assertEquals(version1001000.hashCode(), version1001.hashCode());
        assertEquals(version1001000, version1001);

        assertTrue(version1001.isAfterOrEqual(version1001000));
    }
}
