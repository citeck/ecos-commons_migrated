package ru.citeck.ecos.commons.test.json

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.json.Json

class TypeFactoryTest {

    @Test
    fun test() {

        val rawCollectionValue = Json.mapper.read("[{\"field\":\"value\"}]")

        val listType = Json.mapper.getListType(CustomDto::class.java)
        val listValue = Json.mapper.convert<List<CustomDto>>(rawCollectionValue, listType)!!

        assertThat(listValue).isInstanceOf(List::class.java)
        assertThat(listValue).hasSize(1)
        assertThat(listValue[0]).isInstanceOf(CustomDto::class.java)
        assertThat(listValue[0].field).isEqualTo("value")

        val setType = Json.mapper.getSetType(CustomDto::class.java)
        val setValue = Json.mapper.convert<Set<CustomDto>>(rawCollectionValue, setType)!!

        assertThat(setValue).isInstanceOf(Set::class.java)
        assertThat(setValue).hasSize(1)
        assertThat(setValue.first()).isInstanceOf(CustomDto::class.java)
        assertThat(setValue.first().field).isEqualTo("value")

        val rawMapValue = Json.mapper.read("{\"someKey\": {\"field\":\"value\"}}")
        val mapType = Json.mapper.getMapType(String::class.java, CustomDto::class.java)
        val mapValue = Json.mapper.convert<Map<String, CustomDto>>(rawMapValue, mapType)!!

        assertThat(mapValue).isInstanceOf(Map::class.java)
        assertThat(mapValue).hasSize(1)
        assertThat(mapValue.entries.first().key).isEqualTo("someKey")
        assertThat(mapValue.entries.first().value).isInstanceOf(CustomDto::class.java)
        assertThat(mapValue.entries.first().value.field).isEqualTo("value")
    }

    data class CustomDto(
        val field: String
    )
}
