package ru.citeck.ecos.commons.test.utils

import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.utils.ReflectUtils
import kotlin.test.assertEquals

class ReflectUtilsTest {

    @Test
    fun test() {

        val args = ReflectUtils.getGenericArgs(TestClass0::class.java, TestInterface0::class.java)
        assertEquals(1, args.size)
        assertEquals(String::class.java, args[0])

        val args1 = ReflectUtils.getGenericArgs(TestClass1::class.java, TestInterface10::class.java)
        assertEquals(1, args1.size)
        assertEquals(String::class.java, args1[0])
    }

    class TestClass0 : TestInterface2<String>

    interface TestInterface2<K> : TestInterface1<K>

    interface TestInterface1<E> : TestInterface0<E>

    interface TestInterface0<T>

    class TestClass1 : TestClass2()

    open class TestClass2 : TestInterface10<String>

    interface TestInterface10<T>
}
