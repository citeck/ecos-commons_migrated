package ru.citeck.ecos.commons.test.digest

import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.test.testutils.TestUtils
import ru.citeck.ecos.commons.utils.digest.DigestAlgorithm
import ru.citeck.ecos.commons.utils.digest.DigestUtils
import kotlin.test.assertEquals

class DigestTest {

    @Test
    fun test() {

        val input = TestUtils.getResourceAsBytes("digest/digest-input.json")!!
        val controlValues = TestUtils.getJsonResource("digest/digest-result.json", ControlValues::class.java)

        controlValues!!.forEach { algorithm, result ->
            val actual = DigestUtils.getDigest(input, algorithm)
            assertEquals(result, actual.hash)
        }
    }

    class ControlValues : HashMap<DigestAlgorithm, String>()
}
