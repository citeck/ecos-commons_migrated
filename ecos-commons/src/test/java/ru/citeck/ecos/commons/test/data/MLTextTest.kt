package ru.citeck.ecos.commons.test.data

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import ru.citeck.ecos.commons.data.MLText
import ru.citeck.ecos.commons.json.Json.mapper
import java.util.*
import kotlin.test.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MLTextTest {

    @Test
    fun test() {

        val enValue = "EN_VALUE"
        val ruValue = "RU_VALUE"
        val mlAsObject = "{\"en\": \"$enValue\", \"ru\":\"$ruValue\"}"
        val withInnerAsObj = "{\"textField\": $mlAsObject}"
        val withInnerAsStr = "{\"textField\": \"$enValue\"}"

        val strTxt0 = mapper.readNotNull(withInnerAsStr, WithInnerField::class.java)
        val objTxt0 = mapper.readNotNull(withInnerAsObj, WithInnerField::class.java)

        assertEquals(strTxt0.textField!!.get(Locale.ENGLISH), objTxt0.textField!!.get(Locale.ENGLISH))
        assertNotEquals(strTxt0, objTxt0)

        strTxt0.textField = strTxt0.textField!!.withValue(Locale("ru"), ruValue)
        assertEquals(strTxt0, objTxt0)

        val withInnerFromObj = mapper.toString(objTxt0)
        val withInnerObjStrObj = mapper.read(withInnerFromObj, WithInnerField::class.java)

        assertEquals(objTxt0, withInnerObjStrObj)

        val testValue = "TestValue"
        val mlText = mapper.readNotNull("{\"ru\":\"$testValue\"}", MLText::class.java)

        assertEquals(testValue, mlText.getClosestValue(Locale.ENGLISH))
        assertTrue(mlText.get(Locale.ENGLISH).isEmpty())
        assertEquals(testValue, mlText.get(Locale("ru")))

        val rawJacksonMapper = ObjectMapper()

        val strTxt00 = mapper.readNotNull(withInnerAsStr, SimpleWithField::class.java)
        val objTxt00 = mapper.readNotNull(withInnerAsObj, SimpleWithField::class.java)

        val strTxt01 = rawJacksonMapper.readValue(withInnerAsStr, SimpleWithField::class.java)
        val objTxt01 = rawJacksonMapper.readValue(withInnerAsObj, SimpleWithField::class.java)

        assertEquals(strTxt00, strTxt01)
        assertEquals(objTxt00, objTxt01)

        val strTxt00txt = mapper.toString(strTxt00)
        val objTxt00txt = mapper.toString(objTxt00)

        val strTxt01txt = rawJacksonMapper.writeValueAsString(strTxt01)
        val objTxt01txt = rawJacksonMapper.writeValueAsString(objTxt01)

        assertEquals(strTxt00txt, strTxt01txt)
        assertEquals(objTxt00txt, objTxt01txt)

        val staticMl0 = MLText.getClosestValue(null, null)
        assertEquals("", staticMl0)
        val staticMl1 = MLText.getClosestValue(null, null, "123")
        assertEquals("123", staticMl1)
        var text = MLText(Locale.CANADA to "abc")
        val staticMl2 = MLText.getClosestValue(text, null)
        assertEquals("abc", staticMl2)
        text = text.withValue(Locale.ENGLISH, "def")
        val staticMl3 = MLText.getClosestValue(text, null)
        assertEquals("def", staticMl3)
        val staticMl4 = MLText.getClosestValue(text, Locale.CANADA)
        assertEquals("abc", staticMl4)
        val staticMl5 = MLText.getClosestValue(text, Locale.ENGLISH)
        assertEquals("def", staticMl5)
        val staticMl6 = MLText.getClosestValue(null, Locale.ENGLISH)
        assertEquals("", staticMl6)

        val testText = MLText(
            Locale("ru") to "",
            Locale.ENGLISH to "abc",
            Locale.CANADA to "   ",
            Locale.GERMANY to "def"
        )
        assertEquals(2, testText.getValues().size)
        assertFalse(testText.getClosest(Locale.CANADA).isBlank())
        assertEquals("", testText.get(Locale.FRANCE))
        assertFalse(testText.getClosest(Locale.FRANCE).isBlank())
        assertEquals("abc", testText.get(Locale.ENGLISH))
        assertEquals("abc", testText.getClosest(Locale.ENGLISH))
        assertEquals("abc", testText.getClosestValue(Locale.ENGLISH))

        val testText2 = MLText(Locale.ENGLISH to "  ")
        assertEquals(0, testText2.getValues().size)

        val testText3 = MLText("  ")
        assertEquals(0, testText3.getValues().size)

        val valueWithMlText = mapper.read("{\"textField\":{\"ru\":\"value\",\"en\":null}}", WithInnerField::class.java)
        assertEquals(MLText.EMPTY.withValue(Locale("ru"), "value"), valueWithMlText?.textField)
    }

    data class WithInnerField(var textField: MLText? = null)

    class SimpleWithField {

        var textField: MLText? = null

        override fun equals(other: Any?): Boolean {
            if (this === other) {
                return true
            }
            if (javaClass != other?.javaClass) {
                return false
            }
            other as SimpleWithField

            if (textField != other.textField) {
                return false
            }
            return true
        }

        override fun hashCode(): Int {
            return textField?.hashCode() ?: 0
        }
    }
}
