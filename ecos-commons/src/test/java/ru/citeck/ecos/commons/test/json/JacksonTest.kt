package ru.citeck.ecos.commons.test.json

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.TextNode
import ecos.com.fasterxml.jackson210.databind.JsonNode
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.json.Json
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNull

class JacksonTest {

    @Test
    fun yamlTest() {
        val v0 = Json.mapper.read("---\"\"", DataValue::class.java)
        assertThat(v0).isEqualTo(DataValue.createStr("---\"\""))
        val v1 = Json.mapper.read("---\"---\"", DataValue::class.java)
        assertThat(v1).isEqualTo(DataValue.createStr("---\"---\""))
        val v2 = Json.mapper.read("---\n\"---\"", DataValue::class.java)
        assertThat(v2).isEqualTo(DataValue.createStr("---"))
        val v3 = Json.mapper.read("---\n---", DataValue::class.java)
        assertThat(v3).isEqualTo(DataValue.NULL)
    }

    @Test
    fun test() {

        var arrayFromData = Json.mapper.convert(DataValue.create(emptyList<String>()), ArrayNode::class.java)
        val arrayNode = JsonNodeFactory.instance.arrayNode()

        assertEquals(arrayNode, arrayFromData)

        arrayFromData = Json.mapper.convert(DataValue.create(listOf("one", "two")), ArrayNode::class.java)
        arrayNode.add("one").add("two")
        assertEquals(arrayNode, arrayFromData)

        val objFromData = Json.mapper.convert(DataValue.create(Collections.singletonMap("key", "value")), ObjectNode::class.java)
        val objNode = JsonNodeFactory.instance.objectNode()
        objNode.put("key", "value")

        assertEquals(objNode, objFromData)

        val comFasterxmlObj = JsonNodeFactory.instance.objectNode()
        comFasterxmlObj.put("a", "b")
        comFasterxmlObj.put("c", 1)
        comFasterxmlObj.put("d", true)
        comFasterxmlObj.put("e", JsonNodeFactory.instance.objectNode())

        val ecosFasterxmlObj = Json.mapper.convert(comFasterxmlObj, JsonNode::class.java)
        assertEquals("b", ecosFasterxmlObj!!.get("a").asText())
        assertEquals(true, ecosFasterxmlObj.get("d").asBoolean())

        val comFasterxmlObj2 = Json.mapper.convert(ecosFasterxmlObj, com.fasterxml.jackson.databind.JsonNode::class.java)
        assertEquals(comFasterxmlObj, comFasterxmlObj2)

        val baseDto = TestDto("b")
        val dtoFromStr = Json.mapper.convert("{\"a\":\"b\"}", TestDto::class.java)
        assertEquals(baseDto, dtoFromStr)
        val dtoFromStr1 = Json.mapper.convert(TextNode.valueOf("{\"a\":\"b\"}"), TestDto::class.java)
        assertEquals(baseDto, dtoFromStr1)
        val dtoFromStr2 = Json.mapper.convert(ecos.com.fasterxml.jackson210.databind.node.TextNode.valueOf("{\"a\":\"b\"}"), TestDto::class.java)
        assertEquals(baseDto, dtoFromStr2)

        val textData = "TYPE:\"{http://www.alfresco.org/model/bpm/1.0}task\" AND (@{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/GROUP_ALFRESCO_ADMINISTRATORS\" OR @{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/GROUP_ALFRESCO_MODEL_ADMINISTRATORS\" OR @{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/GROUP_ALFRESCO_SEARCH_ADMINISTRATORS\" OR @{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/3cf46435-77d6-4a59-b4b3-540b66599532\" OR @{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/GROUP_EMAIL_CONTRIBUTORS\" OR @{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/GROUP_SITE_ADMINISTRATORS\" OR @{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/GROUP__orgstruct_home_\" OR @{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/b84f97ef-ef09-4593-b498-770d971f25da\" OR @{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/8583fa53-cd06-45b7-8621-2ec551663036\" OR @{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/31fd41f6-5b20-4f99-8f55-32a0c3cd50a6\" OR @{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/89668ae1-1047-4e79-8555-143179929be1\" OR @{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/3ec3752b-4efb-455a-bf08-c013190e35bc\" OR @{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/df4d7fcf-7fbd-49b1-8578-8e3ef9bd7473\" OR @{http://www.citeck.ru/model/workflow-mirror/1.0}actors:\"workspace://SpacesStore/ccb1be1c-746c-40c2-8200-9b3ccbad16de\") AND (ISNULL:\"{http://www.alfresco.org/model/bpm/1.0}completionDate\" OR ISUNSET:\"{http://www.alfresco.org/model/bpm/1.0}completionDate\")"
        val dataValueFromText = Json.mapper.convert(textData, DataValue::class.java)!!.asText()
        assertEquals(textData, dataValueFromText)

        val defaultTest = Json.mapper.read("", DataValue::class.java)
        assertEquals(DataValue.NULL, defaultTest)

        assertNull(Json.mapper.read("", TestDto::class.java))

        val textDataWithSlash = "abc\\asda"
        val dvWithSlash = Json.mapper.convert(textDataWithSlash, DataValue::class.java)!!.asText()
        assertEquals(textDataWithSlash, dvWithSlash)

        val textDataWithNewLine = "abc\ndea"
        val dvWithNewLine = Json.mapper.convert(textDataWithNewLine, DataValue::class.java)!!.asText()
        assertEquals(textDataWithNewLine, dvWithNewLine)

        val objData = ObjectData.create()
        objData.set("a", textDataWithNewLine)
        assertEquals(textDataWithNewLine, objData.get("a", String::class.java))

        val originalMapper = ObjectMapper()
        val data = ObjectData.create()
        data.set("aaa", "bbb")
        val newObjData = originalMapper.treeToValue(originalMapper.valueToTree(data), ObjectData::class.java)
        assertEquals(data, newObjData)

        val newInnerData = originalMapper.treeToValue(originalMapper.valueToTree(data.getData()), DataValue::class.java)
        assertEquals(data.getData(), newInnerData)
    }

    data class TestDto(val a: String)
}
