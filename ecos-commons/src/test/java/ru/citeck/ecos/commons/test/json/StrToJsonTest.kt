package ru.citeck.ecos.commons.test.json

import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.json.Json
import kotlin.test.assertEquals

class StrToJsonTest {

    @Test
    fun test() {

        val qNameStr = "{http://www.citeck.ru/model/1.0}localName"
        val value = DataValue.create(qNameStr)

        assertEquals(qNameStr, value.asText())

        val qNameStr2 = "{\"http://www.citeck.ru/model/1.0}localName"
        val value2 = DataValue.create(qNameStr2)

        assertEquals(qNameStr2, value2.asText())

        assertEquals(Json.mapper.newObjectNode(), DataValue.create("{}").asJson())
    }
}
