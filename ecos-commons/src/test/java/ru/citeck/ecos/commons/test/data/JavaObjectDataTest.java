package ru.citeck.ecos.commons.test.data;

import kotlin.Pair;
import kotlin.jvm.functions.Function2;
import org.junit.jupiter.api.Test;
import ru.citeck.ecos.commons.data.ObjectData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JavaObjectDataTest {

    @Test
    public void test() {

        Function2<String, Integer, Object> aaa = (key, num) -> "test";

        ObjectData obj = ObjectData.create("{\"aa\":\"bb\",\"cc\":\"dd\"}");
        List<Pair<String, Object>> elementsList = obj.map((k, v) -> new Pair<>(k + "-postfix", v.asJavaObj()));

        Map<String, Object> elements = new HashMap<>();
        elementsList.forEach(pair -> elements.put(pair.getFirst(), pair.getSecond()));

        assertEquals("bb", elements.get("aa-postfix"));
        assertEquals("dd", elements.get("cc-postfix"));

        obj.forEachJ((k, v) -> {
            System.out.println(k + " " + v);
        });

        obj.getData().forEach(System.out::println);
        obj.forEachJ((k, v) -> System.out.println(v));

        obj.getData().forEachJ((k, v) -> System.out.println(v));

        obj.getData().map(v -> v.asText() + "aa");
        obj.getData().mapKV((k, v) -> v.asText() + "aa");
    }
}
