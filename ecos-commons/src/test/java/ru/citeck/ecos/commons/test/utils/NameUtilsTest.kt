package ru.citeck.ecos.commons.test.utils

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.utils.NameUtils
import kotlin.test.assertEquals

class NameUtilsTest {

    @Test
    fun test() {

        val escaper = NameUtils.getEscaper()

        assertEsc(escaper, "_u002E_disp", ".disp")
        assertEsc(escaper, "_u002E__u002E_disp", "..disp")
        assertEsc(escaper, "_u002E__u002E_di_u002E_sp", "..di.sp")
        assertEsc(escaper, "_u002E__u002E_d_u002D__u002D_i_u002E_s_p", "..d--i.s_p")
        assertEsc(escaper, "a_u0020__u0020__u0009__u0020__u0020_c", "a  \t  c")
        assertEsc(escaper, "a_u0020__u0020__u000A__u0020__u0020_c", "a  \n  c")

        assertEsc(NameUtils.getEscaper("\n"), "a  _u000A_  c", "a  \n  c")
        assertEsc(NameUtils.getEscaperWithAllowedChars("\n"), "a_u0020__u0020_\n_u0020__u0020_c", "a  \n  c")
    }

    private fun assertEsc(escaper: NameUtils.Escaper, expected: String, source: String) {
        val res: String = escaper.escape(source)
        Assertions.assertEquals(expected, res)
        assertEquals(source, escaper.unescape(res))
    }
}
