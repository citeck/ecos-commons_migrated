package ru.citeck.ecos.commons.test.data

import ecos.com.fasterxml.jackson210.databind.node.TextNode
import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.json.Json
import java.time.Instant
import java.util.*
import kotlin.collections.ArrayList
import kotlin.test.assertEquals
import kotlin.test.assertSame

class DataValueTest {

    @Test
    fun test() {

        val value = DataValue.create("{\"aa\": [\"bbb\", \"ccc\"]}")
        val convertedValue = Json.mapper.convert(value, DataValue::class.java)

        assertSame(value, convertedValue)

        val inner0 = value.get("aa").asJson()
        val inner1 = value.get("aa").asJson()

        assertSame(inner0, inner1)
        assertSame(value.get("aa").asJson(), inner0)

        val inner2 = value.get("\$.aa").asJson()
        val inner3 = value.get("\$.aa").asJson()

        assertSame(inner2, inner3)
        assertSame(value.get("aa").asJson(), inner2)

        val inner4 = value.get("/aa").asJson()
        val inner5 = value.get("/aa").asJson()

        assertSame(inner4, inner5)
        assertSame(value.get("aa").asJson(), inner4)

        val list = ArrayList(listOf("first", "second"))
        val arr = DataValue.create("[\"first\", \"second\"]")

        assertEquals(list, ArrayList(arr.asStrList()))

        arr.add("third")
        list.add("third")

        assertEquals(list, ArrayList(arr.asStrList()))

        arr.addAll(listOf("new", "new2"))
        list.addAll(listOf("new", "new2"))

        assertEquals(list, ArrayList(arr.asStrList()))

        arr.insert(1, "new3")
        list.add(1, "new3")
        assertEquals(list, ArrayList(arr.asStrList()))

        arr.insertAll(3, listOf("new4", "new5", "new6"))
        list.addAll(3, listOf("new4", "new5", "new6"))
        assertEquals(list, ArrayList(arr.asStrList()))

        val data = DataValue.createObj()
        data.set("aa", "bb")
        assertEquals(ArrayList(listOf("bb")), ArrayList(data.get("aa").toStrList()))
        assertEquals(ArrayList(emptyList()), ArrayList(data.get("aa").asStrList()))

        val date = Date()
        val instantDataValue = DataValue.create(Instant.ofEpochMilli(date.time))
        val instantDate = instantDataValue.getAs(Date::class.java)

        assertEquals(date, instantDate)

        val value2 = DataValue.create("[\"aa\',\"bb\"]")
        value2.forEach { println(it) }
        value2.forEach { _, v -> println(v) }
    }

    @Test
    fun testStr() {

        val strValue = "{\"a\":\"b\"}"
        val value = DataValue.createStr(strValue)

        assertEquals(TextNode.valueOf(strValue), value.asJson())

        val obj = DataValue.createObj()
        obj.setStr("key", strValue)

        assertEquals(TextNode.valueOf(strValue), obj.get("key").asJson())
    }

    @Test
    fun testEmptyStr() {

        val dataValue = DataValue.create("")

        assertEquals("", dataValue.asJson().asText())
    }
}
