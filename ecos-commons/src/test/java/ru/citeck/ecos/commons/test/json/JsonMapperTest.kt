package ru.citeck.ecos.commons.test.json

import ecos.com.fasterxml.jackson210.databind.node.ArrayNode
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.jupiter.api.fail
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.json.Json
import java.time.Instant
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertSame

class JsonMapperTest {

    @Test
    fun testTextRead() {

        val strValue = "\"first\"-second"
        assertEquals(strValue, Json.mapper.read(strValue)?.asText())
    }

    @Test
    fun test() {

        val data = Json.mapper.newArrayNode()
        data.add("123")

        val arrFromConvert = Json.mapper.convert(data, ArrayNode::class.java)

        assertSame(data, arrFromConvert)

        val timeDto = TestTimeDto()
        val timeDtoJson = Json.mapper.toPrettyString(timeDto)

        println(timeDtoJson)

        assertEquals(timeDto, Json.mapper.read(timeDtoJson, TestTimeDto::class.java))
        val dateDto = Json.mapper.read(timeDtoJson, TestTimeDate::class.java)!!

        assertEquals(timeDto.date.time, dateDto.date.time)
        assertEquals(timeDto.instant.toEpochMilli(), dateDto.instant.time)
        assertEquals(timeDto.instantWithoutMs.toEpochMilli(), dateDto.instantWithoutMs.time)
        val dateTimeDto = DateDto(Date())
        val instantTimeDto = Json.mapper.convert(dateTimeDto, InstantTimeDto::class.java)!!

        assertEquals(dateTimeDto.time.time, instantTimeDto.time.toEpochMilli())

        val date1 = Date()
        val date1Instant = Json.mapper.convert(date1, Instant::class.java)
        val date1InstantDate1 = Json.mapper.convert(date1Instant, Date::class.java)

        assertEquals(date1, date1InstantDate1)

        val date1Instant2 = Json.mapper.convert(date1.time / 1000.0, Instant::class.java)
        val date1Instant2Date1 = Json.mapper.convert(date1Instant2, Date::class.java)

        assertEquals(date1, date1Instant2Date1)

        val date1Instant3 = Json.mapper.convert(date1.toInstant().toString(), Instant::class.java)
        val date1Instant3Date1 = Json.mapper.convert(date1Instant3, Date::class.java)

        assertEquals(date1, date1Instant3Date1)
    }

    @Test
    fun yamlTest() {

        assertEquals(
            ObjectData.create("""{"abc":"def"}"""),
            ObjectData.create(
                Json.mapper.read(
                    """
                ---
                abc: def
                    """.trimIndent()
                )
            )
        )
    }

    @Test
    fun dataUriTest() {

        val jsonText = """
            {
                "abc": "def",
                "aa": 123
            }
        """.trimIndent()

        val yamlText = """
            abc: def
            aa: 123
        """.trimIndent()

        val yamlTextWithDashes = """
            ---
            abc: def
            aa: 123
        """.trimIndent()

        val jsonData = ObjectData.create(jsonText)
        val jsonDataUri = "data:application/json;charset=UTF-8;base64," +
            Base64.getEncoder().encodeToString(jsonText.toByteArray())

        assertEquals(jsonData, Json.mapper.read(jsonDataUri, ObjectData::class.java))

        val yamlDataUri = "data:application/x-yaml;base64," +
            Base64.getEncoder().encodeToString(yamlText.toByteArray())

        assertEquals(jsonData, Json.mapper.read(yamlDataUri, ObjectData::class.java))

        val testWithMimetype = { expected: ObjectData?, data: ByteArray, mimetype: String ->
            val utf8uri = "data:$mimetype;charset=UTF-8;base64," + Base64.getEncoder().encodeToString(data)
            assertEquals(expected, Json.mapper.read(utf8uri, ObjectData::class.java))
            val unknownCodePageUri = "data:$mimetype;base64," + Base64.getEncoder().encodeToString(data)
            assertEquals(expected, Json.mapper.read(unknownCodePageUri, ObjectData::class.java))
        }

        val testWithUnknownMimetypes = { expected: ObjectData?, data: ByteArray ->
            testWithMimetype(expected, data, "application/octet-stream")
            testWithMimetype(expected, data, "text/plain")
        }

        testWithUnknownMimetypes(jsonData, jsonText.toByteArray())
        testWithUnknownMimetypes(jsonData, yamlText.toByteArray())
        testWithUnknownMimetypes(jsonData, yamlTextWithDashes.toByteArray())

        testWithUnknownMimetypes(null, byteArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
    }

    @Test
    fun dtoWithInstantTest() {

        val dto = DtoWithInstant()
        val instant = Instant.now().toString()

        Json.mapper.applyData(
            dto,
            ObjectData.create(
                """
            {
            "instant0": "",
            "instant1": null,
            "instant2": "$instant",
            "text": "text"
            }
                """.trimIndent()
            )
        )

        assertThat(dto.text).isEqualTo("text")
        assertThat(dto.instant0).isNull()
        assertThat(dto.instant1).isNull()
        assertThat(dto.instant2).isEqualTo(instant)
    }

    class DtoWithInstant {
        var instant0: Instant? = null
        var instant1: Instant? = null
        var instant2: Instant? = null
        var text: String? = null
    }

    @Test
    fun testYaml() {
        val result1 = Json.mapper.read(
            """
            %YAML
            value: %abc
        """,
            DataValueDto::class.java
        ) ?: fail("must be non-null")
        assertEquals("%abc", result1.value.asText())

        val result2 = Json.mapper.read(
            """
            ---
            value: -abc
        """,
            DataValueDto::class.java
        ) ?: fail("must be non-null")
        assertEquals("-abc", result2.value.asText())
    }

    @Test
    fun testStartsWithSpecialCharacter() {
        val result1 = Json.mapper.read(
            """
            {
                "value": "%11"
            }
        """,
            DataValueDto::class.java
        ) ?: fail("must be non-null")
        assertEquals("%11", result1.value.asText())

        val result2 = Json.mapper.read(
            """
            {
                "value": "-1"
            }
        """,
            DataValueDto::class.java
        ) ?: fail("must be non-null")
        assertEquals("-1", result2.value.asText())
    }

    data class DataValueDto(
        val value: DataValue
    )

    data class TestTimeDate(
        val date: Date,
        val instant: Date,
        val instantWithoutMs: Date
    )

    data class InstantTimeDto(val time: Instant)

    data class DateDto(val time: Date)

    class TestTimeDto {

        val date: Date = Date()

        val instant: Instant
        val instantWithoutMs: Instant

        init {
            instant = Instant.ofEpochMilli(date.time)
            instantWithoutMs = Instant.ofEpochMilli((date.time / 1000) * 1000)
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) {
                return true
            }
            if (javaClass != other?.javaClass) {
                return false
            }
            other as TestTimeDto
            return instant == other.instant &&
                instantWithoutMs == other.instantWithoutMs &&
                date == other.date
        }

        override fun hashCode(): Int {
            return Objects.hash(instant, instantWithoutMs, date)
        }
    }
}
