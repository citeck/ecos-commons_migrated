package ru.citeck.ecos.commons.test.utils

import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.utils.StringUtils.escapeDoubleQuotes
import kotlin.test.assertEquals

class StringUtilsTest {

    @Test
    fun test() {

        var result = escapeDoubleQuotes("abc\"de")
        assertEquals("abc\\\"de", result)

        result = escapeDoubleQuotes("abc\\\"de")
        assertEquals("abc\\\"de", result)

        result = escapeDoubleQuotes("abc\\\"de\"")
        assertEquals("abc\\\"de\\\"", result)

        result = escapeDoubleQuotes("\"abc")
        assertEquals("\\\"abc", result)

        result = escapeDoubleQuotes("\\\"abc")
        assertEquals("\\\"abc", result)

        result = escapeDoubleQuotes("\\\\abc")
        assertEquals("\\\\abc", result)
    }
}
