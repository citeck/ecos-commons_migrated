package ru.citeck.ecos.commons.test.io.file

import org.junit.Assert.assertArrayEquals
import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.mem.EcosMemDir
import ru.citeck.ecos.commons.io.file.std.EcosStdFile
import ru.citeck.ecos.commons.utils.ZipUtils
import java.io.ByteArrayInputStream
import java.io.File
import java.time.Instant
import java.time.temporal.ChronoUnit
import kotlin.test.*

class EcosFileTest {

    private fun fileTest(root: EcosFile) {

        val content = "TestContent с латиницей"
        val contentFile = root.createFile("testFile", content)

        assertEquals(1, root.getChildren().size)
        assertEquals(content, contentFile.readAsString())

        val deepDirPath = "inner0/inner2/inner3"
        val deepDir = root.createDir(deepDirPath)
        val rootFromDeep = deepDir.getDir("/")

        assertSame(root, rootFromDeep)

        val deepFileContent = "Deep file content"
        val deepFileName = "deepFile"
        deepDir.createFile(deepFileName, deepFileContent)

        assertEquals(deepFileContent, root.getFile("$deepDirPath/$deepFileName")!!.readAsString())

        val allFiles = root.findFilesWithDirs()
        assertEquals(5, allFiles.size)

        deepDir.createFile("somefile.json", "content")
        deepDir.createFile("somefile2.json", "content")
        root.createFile("somefile3.json", "content")

        findFilesTest(root, deepDir)

        val zipBytes = ZipUtils.writeZipAsBytes(root)
        val rootFromZip = ZipUtils.extractZip(zipBytes)

        assertEquals(deepFileContent, rootFromZip.getFile("$deepDirPath/$deepFileName")!!.readAsString())

        val rootCopy = EcosMemDir()
        rootCopy.copyFilesFrom(root)

        val deepDirFromCopy = rootCopy.getDir(deepDirPath)!!
        findFilesTest(rootCopy, deepDirFromCopy)

        val inner11testDir0 = "inner11test0"
        val inner11testDir1 = "inner11test1"
        val inner11Dir = root.getOrCreateDir("$inner11testDir0/$inner11testDir1")
        val inner11testFileName = "testFileName.txt"
        inner11Dir.createFile(inner11testFileName, "test")

        val test11InnerFiles = root.getDir(inner11testDir0)!!.findFiles("*/*.txt")
        assertEquals(1, test11InnerFiles.size)

        val innerInnerDir = root.getOrCreateDir("inner0/inner2")
        assertTrue(innerInnerDir.getChildren().isNotEmpty())

        root.createFile("workflow\\module.yml", "content")
        val str0 = root.getFile("workflow\\module.yml")!!.readAsString()
        val str1 = root.getFile("workflow/module.yml")!!.readAsString()

        assertEquals(str0, str1)
    }

    fun findFilesTest(root: EcosFile, deepDir: EcosFile) {

        val filesByPattern = root.findFiles("**.json")
        assertEquals(3, filesByPattern.size)

        val filesByPattern2 = root.getDir("inner0")!!.findFiles("**.json")
        assertEquals(2, filesByPattern2.size)

        val filesByPattern3 = deepDir.findFiles("**.json")
        assertEquals(2, filesByPattern3.size)

        val filesByPattern4 = root.findFiles("*.json")
        assertEquals(1, filesByPattern4.size)
    }

    @Test
    fun memTest() {
        fileTest(EcosMemDir())
    }

    @Test
    fun stdTest() {
        val file = File("ecos-file-std-test-dir")
        if (file.exists()) {
            file.deleteRecursively()
        }
        file.mkdir()
        fileTest(EcosStdFile(file))
        file.deleteRecursively()
        assertFalse(file.exists())
    }

    @Test
    fun copyFromTest() {

        val src = EcosMemDir()
        src.createFile("first.txt", "abc")
        src.createFile("second.txt", "def")
        src.createFile("dir/third.txt", "ghi")
        src.createFile("dir/fourth.txt", "jkl")

        val srcBytes = ZipUtils.writeZipAsBytes(src)
        val emptyTarget = EcosMemDir()
        emptyTarget.copyFilesFrom(src)

        assertArrayEquals(srcBytes, ZipUtils.writeZipAsBytes(emptyTarget))

        val targetWithReplace = EcosMemDir()
        targetWithReplace.createFile("second.txt", "123")
        targetWithReplace.createFile("dir/fourth.txt", "456")
        targetWithReplace.copyFilesFrom(src)

        assertArrayEquals(srcBytes, ZipUtils.writeZipAsBytes(targetWithReplace))
    }

    @Test
    fun fileTimeTest() {

        val now = Instant.now().truncatedTo(ChronoUnit.SECONDS)
        val creationTime1 = now.minus(3, ChronoUnit.DAYS).toEpochMilli()
        val modifiedTime1 = now.minus(3, ChronoUnit.HOURS).toEpochMilli()
        val accessTime1 = now.minus(2, ChronoUnit.HOURS).toEpochMilli()
        val creationTime2 = now.minus(2, ChronoUnit.DAYS).toEpochMilli()
        val modifiedTime2 = now.minus(2, ChronoUnit.HOURS).toEpochMilli()
        val accessTime2 = now.minus(1, ChronoUnit.HOURS).toEpochMilli()

        val src = EcosMemDir()
        val fileName = "fileName.txt"
        val file1 = src.createFile(fileName, "abc")
        file1.setCreationTime(creationTime1)
        file1.setLastModifiedTime(modifiedTime1)
        file1.setLastAccessTime(accessTime1)
        val file2 = src.createFile("dir/$fileName", "ghi")
        file2.setCreationTime(creationTime2)
        file2.setLastModifiedTime(modifiedTime2)
        file2.setLastAccessTime(accessTime2)

        val srcBytes = ZipUtils.writeZipAsBytes(src, ZipUtils.WriteConfig(withFileTime = true))
        val srcFromZip = ZipUtils.extractZip(ByteArrayInputStream(srcBytes), EcosMemDir(), ZipUtils.ExtractConfig(withFileTime = true))

        var fileFromZip = srcFromZip.getFile("/$fileName")
        assertNotNull(fileFromZip)
        assertEquals(creationTime1, file1.getCreationTime())
        assertEquals(file1.getCreationTime(), fileFromZip.getCreationTime())
        assertEquals(modifiedTime1, file1.getLastModifiedTime())
        assertNotNull(fileFromZip.getLastModifiedTime())
        assertEquals(file1.getLastModifiedTime(), fileFromZip.getLastModifiedTime())
        assertEquals(accessTime1, file1.getLastAccessTime())
        assertEquals(file1.getLastAccessTime(), fileFromZip.getLastAccessTime())

        fileFromZip = srcFromZip.getFile("/dir/$fileName")
        assertNotNull(fileFromZip)
        assertEquals(creationTime2, file2.getCreationTime())
        assertEquals(file2.getCreationTime(), fileFromZip.getCreationTime())
        assertEquals(modifiedTime2, file2.getLastModifiedTime())
        assertNotNull(fileFromZip.getLastModifiedTime())
        assertEquals(file2.getLastModifiedTime(), fileFromZip.getLastModifiedTime())
        assertEquals(accessTime2, file2.getLastAccessTime())
        assertEquals(file2.getLastAccessTime(), fileFromZip.getLastAccessTime())
    }
}
