package ru.citeck.ecos.commons.test.json.serialization.time

import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.json.Json
import java.time.*
import kotlin.test.assertEquals

class InstantSerializationTest {

    @Test
    fun test() {

        val time = Wrapper(Instant.now())

        assertEquals("{\"time\":${time.time.toEpochMilli()}}", Json.mapper.toString(time))

        assertEquals(time, Json.mapper.read("{\"time\":${time.time.toEpochMilli()}}", Wrapper::class.java))
        assertEquals(
            Wrapper(Instant.ofEpochSecond(time.time.epochSecond)),
            Json.mapper.read("{\"time\":${time.time.epochSecond}}", Wrapper::class.java)
        )

        val isoTime = time.time.toString()
        assertEquals(time, Json.mapper.read("{\"time\":\"${isoTime}\"}", Wrapper::class.java))

        var timeWithOffset = OffsetDateTime.parse(isoTime)
        timeWithOffset = timeWithOffset.withOffsetSameInstant(ZoneOffset.ofHours(7))

        assertEquals(time.time, timeWithOffset.toInstant())

        val isoTimeWithOffset = timeWithOffset.toString()
        assertEquals(time, Json.mapper.read("{\"time\":\"${isoTimeWithOffset}\"}", Wrapper::class.java))
    }

    data class Wrapper(val time: Instant)
}
