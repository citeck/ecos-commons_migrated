package ru.citeck.ecos.commons.test.data

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.json.Json.mapper
import java.util.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ObjectDataTest {

    companion object {
        private const val JSON = "{\"a\":\"b\"}"
    }

    @Test
    fun test() {
        val data = mapper.readNotNull(JSON, ObjectData::class.java)
        assertEquals("b", data.get("a", ""))

        val intFieldName = "intField"

        val data2 = ObjectData.create()
        data2.set("SomeKey", "some_value")
        data2.set(intFieldName, 123)
        data2.set("innerMap", Collections.singletonMap("test", "inner"))
        data2.set("innerArr", listOf("12", "4213", "213"))

        assertEquals("4213", data2.get("/innerArr/1").asText())
        assertEquals(DataValue.NULL, data2.get("/unknown/path/is/not/exists"))

        val data2Copy = data2.deepCopy()

        assertEquals(data2, data2Copy)
        assertEquals(123, data2Copy.get(intFieldName).asInt())

        val valueStr = "21312312"
        val value = DataValue.create(valueStr)

        assertEquals(valueStr, value.getAs(String::class.java))

        val data3 = mapper.convert("{\"test\":null}", ObjectData::class.java)!!
        val result = HashMap<String, DataValue>()
        data3.forEach { k, v -> result[k] = v }

        assertEquals(1, data3.size())
        assertEquals(result["test"], DataValue.NULL)

        val obj = ObjectData.create("{\"aa\":\"bb\",\"cc\":\"dd\",\"ee\":\"ff\"}")
        val elements = obj.map { k, v -> if (k == "ee") null else Pair("$k-postfix", v.asJavaObj()) }.filterNotNull().toMap()
        assertEquals("bb", elements["aa-postfix"])
        assertEquals("dd", elements["cc-postfix"])
        assertNull(elements["ee-postfix"])
    }
}
