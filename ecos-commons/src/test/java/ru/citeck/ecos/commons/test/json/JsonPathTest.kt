package ru.citeck.ecos.commons.test.json

import ecos.com.fasterxml.jackson210.databind.node.TextNode
import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.json.Json
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class JsonPathTest {

    @Test
    fun test() {

        val data = ObjectData.create()
        data.set("\$aaa", "value")
        assertEquals("value", data.get("\$aaa").asText())
        data.set("\$abc.def", "value1")
        assertEquals("value1", data.get("\$abc.def").asText())

        val actions = listOf(
            "uiserv/action@upload-new-version",
            "uiserv/action@edit",
            "uiserv/action@content-download",
            "uiserv/action@record-actions",
            "uiserv/action@record-24313123",
            "uiserv/action@edit-in-office"
        )

        val name = ObjectData.create(
            """
            {
                "ru": "Базовый тип",
                "en": "Base type"
            }
            """.trimIndent()
        )

        val assocs = DataValue.create(
            """
            [
                {
                    "id": "assoc:associatedWith",
                    "name": "Связан с",
                    "direction": "BOTH",
                    "target": "emodel/type@base"
                },
                {
                    "id": "assoc:associatedWith22",
                    "name": "Связан с 222",
                    "direction": "SOURCE",
                    "target": "emodel/type@user-base"
                }
            ]
            """.trimIndent()
        )

        val odata = ObjectData.create(
            """
                {
                    "id": "base",
                    "name": {
                        "ru": "Базовый тип",
                        "en": "Base type"
                    },
                    "dashboardType": "case-details",
                    "actions": ${Json.mapper.toString(actions)},
                    "journal": "uiserv/journal@base",
                    "associations": ${Json.mapper.toString(assocs)},
                    "testArr": [
                        {
                            "key": "include",
                            "arr": ["a", "b"]
                        },
                        {
                            "key": "include",
                            "arr": ["a", "b"]
                        },
                        {
                            "key": "exclude",
                            "arr": ["a", "b"]
                        }
                    ]
                }
            """.trimIndent()
        )

        // ==== GET ====

        // by jsonPath

        assertEquals(actions, odata.get("\$.actions").toStrList())
        assertEquals(name.getData(), odata.get("\$.name"))
        assertEquals(name.get("ru"), odata.get("\$.name.ru"))
        assertEquals(assocs.get(0).get("target"), odata.get("\$.associations[0].target"))
        val assocsTargets = DataValue.create(
            listOf(
                assocs.get(0).get("target"),
                assocs.get(1).get("target")
            )
        )
        assertEquals(assocsTargets, odata.get("\$.associations.*.target"))
        assertEquals(assocsTargets, odata.get("\$..target"))
        val ids = DataValue.create(
            listOf(
                odata.get("id").asText(),
                *assocs.map { it.get("id").asText() }.toTypedArray()
            )
        )
        assertEquals(ids, odata.get("\$..id"))

        // by simple path

        assertEquals(actions, odata.get("actions").toStrList())
        assertEquals(actions, odata.get("/actions").toStrList())
        assertEquals(name.getData(), odata.get("name"))
        assertEquals(name.get("ru"), odata.get("/name/ru"))
        assertEquals(assocs.get(0).get("target"), odata.get("/associations/0/target"))

        // ==== ADD ====

        val customActionName = "custom-action"
        odata.add("\$.actions", customActionName)
        val mutActions = ArrayList(actions)
        mutActions.add(customActionName)
        assertEquals(mutActions, ArrayList(odata.get("\$.actions").toStrList()))
        assertEquals(mutActions, ArrayList(odata.get("/actions").toStrList()))
        assertEquals(mutActions, ArrayList(odata.get("actions").toStrList()))

        val newActionsList = listOf("action0", "action1")
        mutActions.addAll(newActionsList)
        odata.addAll("\$.actions", newActionsList)
        assertEquals(mutActions, ArrayList(odata.get("\$.actions").toStrList()))

        odata.add("\$.testArr[?(@.key == \"include\")].arr", "c")
        assertEquals(
            DataValue.create(
                """
            [ {
                "key" : "include",
                "arr" : [ "a", "b", "c" ]
              }, {
                "key" : "include",
                "arr" : [ "a", "b", "c" ]
              }, {
                "key" : "exclude",
                "arr" : [ "a", "b" ]
          } ]
                """.trimIndent()
            ),
            odata.get("testArr")
        )

        odata.insertAll("\$.testArr[?(@.key == \"exclude\")].arr", 1, listOf("d", "e"))
        assertEquals(
            DataValue.create(
                """
            [ {
                "key" : "include",
                "arr" : [ "a", "b", "c" ]
              }, {
                "key" : "include",
                "arr" : [ "a", "b", "c" ]
              }, {
                "key" : "exclude",
                "arr" : [ "a", "d", "e", "b" ]
          } ]
                """.trimIndent()
            ),
            odata.get("testArr")
        )

        // ==== DELETE ====

        val toRemove = listOf("action0", "uiserv/action@content-download")
        mutActions.removeIf { toRemove.contains(it) }
        odata.remove("\$.actions.[?(@ == \"action0\" || @ == \"uiserv/action@content-download\")]")
        assertEquals(mutActions, ArrayList(odata.get("\$.actions").toStrList()))

        // ==== RENAME KEY ====

        odata.renameKey("\$.associations[0]", "id", "custom_id")
        assertEquals(assocs.get(0).get("id").asText(), odata.get("\$.associations[0].custom_id").asText())

        // ==== SET ====

        val newDirection = "UNKNOWN"
        odata.set("\$.associations[0].direction", newDirection)
        assertEquals(odata.get("\$.associations[0].direction").asText(), newDirection)
        odata.set("\$.actions", "[]")
        assertEquals(ArrayList(), ArrayList(odata.get("\$.actions").toStrList()))
        odata.set("\$.actions", listOf("test"))
        assertEquals(ArrayList(listOf("test")), ArrayList(odata.get("\$.actions").toStrList()))

        odata.set("\$.newField", "text-value")
        assertEquals("text-value", odata.get("\$.newField").asText())

        odata.set("\$.inner0", "{}")
        odata.set("\$.inner0.inner1", "{}")
        odata.set("\$.inner0.inner1.newField", "text-value")

        assertEquals("text-value", odata.get("\$.inner0.inner1.newField").asText())

        odata.set("\$.inner2.inner3.newField", "text-value")
        assertEquals("text-value", odata.get("\$.inner2.inner3.newField").asText())
        val innerObj = odata.get("\$.inner2.inner3")
        assertTrue(innerObj.isObject())
        assertEquals(DataValue.create("{\"newField\":\"text-value\"}"), innerObj)

        odata.set("arr", "[]")
        odata.get("arr").add("test")
        odata.get("arr").add("value")

        assertEquals(2, odata.get("arr").size())
        assertEquals("test", odata.get("/arr/0").asText())
        assertEquals("test", odata.get("\$.arr[0]").asText())

        odata.set("aaa", "\"{}\"")
        assertEquals(TextNode.valueOf("{}"), odata.get("aaa").asJson())

        odata.set("assocs", assocs)
        odata.set("\$..[?(@.id == \"${assocs.get(0).get("id").asText()}\")]", "someKey", "someValue")
        assertEquals("someValue", odata.get("\$.assocs[0].someKey").asText())

        val actions2 = DataValue.create("[\"first\", \"second\"]")
        val actions3 = DataValue.create("[\"first\", \"second\"]")
        odata.set("actions", actions2)

        odata.add("\$.actions", "third")
        actions3.add("third")

        assertEquals(actions3, odata.get("\$.actions"))

        odata.insertAll("\$.actions", 0, listOf("one", "two"))
        actions3.insertAll(0, listOf("one", "two"))
        assertEquals(actions3, odata.get("\$.actions"))

        odata.add("new-array", "element")
        assertEquals(ArrayList(listOf("element")), ArrayList(odata.get("new-array").asStrList()))

        odata.addAll("\$.innerarr0.innerarr1.innerarr2", listOf("el0", "el1"))
        assertEquals(listOf("el0", "el1"), odata.get("innerarr0").get("innerarr1").get("innerarr2").asStrList())
    }
}
