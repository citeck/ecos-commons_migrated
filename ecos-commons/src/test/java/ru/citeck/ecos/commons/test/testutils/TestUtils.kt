package ru.citeck.ecos.commons.test.testutils

import ecos.com.fasterxml.jackson210.databind.JsonNode
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.commons.utils.io.IOUtils
import java.io.InputStream

object TestUtils {

    fun <T : Any> getJsonResource(path: String, type: Class<T>): T? {
        return Json.mapper.read(getResource(path), type)
    }

    fun getJsonResource(path: String): JsonNode? {
        return Json.mapper.read(IOUtils.readAsString(getResource(path)))
    }

    fun getResourceAsBytes(path: String): ByteArray? {
        val stream = getResource(path) ?: return null
        return IOUtils.readAsBytes(stream)
    }

    fun getResource(path: String): InputStream? {
        val classLoader = TestUtils::class.java.classLoader
        return classLoader.getResourceAsStream(path)
    }
}
