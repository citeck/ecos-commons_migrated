package ru.citeck.ecos.commons.test.utils

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.data.MLText
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.utils.TmplUtils
import java.util.*
import kotlin.collections.HashSet
import kotlin.collections.LinkedHashSet
import kotlin.test.assertEquals

class TmplUtilsTest {

    @Test
    fun getAttsTest() {

        var atts = TmplUtils.getAtts("\${first} \${second}")
        assertEquals(hashSetOf("first", "second"), HashSet(atts))

        atts = TmplUtils.getAtts("\\\${first} \${second}")
        assertEquals(hashSetOf("second"), HashSet(atts))

        atts = TmplUtils.getAtts("\${first} \${  }")
        assertEquals(hashSetOf("first"), HashSet(atts))

        atts = TmplUtils.getAtts("\${first} \${  }  \${}")
        assertEquals(hashSetOf("first"), HashSet(atts))

        atts = TmplUtils.getAtts("\${first} \${  }  \${")
        assertEquals(hashSetOf("first"), HashSet(atts))

        atts = TmplUtils.getAtts("}\${first} \${  }  \${")
        assertEquals(hashSetOf("first"), HashSet(atts))

        atts = TmplUtils.getAtts("}\\\${first} \${  }  \${")
        assertEquals(hashSetOf(), HashSet(atts))
    }

    @Test
    fun testWitEscaping() {

        val model = ObjectData.create(
            """{
               "escaped": "value"
            }
            """.trimIndent()
        )

        assertEquals("\${escaped}-value", TmplUtils.applyAtts("\\\${escaped}-\${escaped}", model).asText())
        assertEquals("some prefix \\\\value-value", TmplUtils.applyAtts("some prefix \\\\\${escaped}-\${escaped}", model).asText())
        assertEquals("some prefix \\\\\${escaped}-value", TmplUtils.applyAtts("some prefix \\\\\\\${escaped}-\${escaped}", model).asText())
    }

    @Test
    fun testWithDto() {

        val model = ObjectData.create(
            """{
                "num": 123,
                "bool": true,
                "str": "str",
                "array": [1, 2, 3],
                "obj": {"key": "value"}
            }
            """.trimIndent()
        )

        val dto = TestDto("first-\${num}", "second", DtoInner("\${str}"))

        assertEquals(
            TestDto("first-123", "second", DtoInner("str")),
            TmplUtils.applyAtts(dto, model)
        )
    }

    @Test
    fun testWithFullReplace() {

        val model = ObjectData.create(
            """{
                "num": 123,
                "bool": true,
                "str": "str",
                "array": [1, 2, 3],
                "obj": {"key": "value"}
            }
            """.trimIndent()
        )

        assertEquals(DataValue.createStr("}\${esc}123\${"), TmplUtils.applyAtts("}\\\${esc}\${num}\${", model))

        assertEquals(DataValue.create(123), TmplUtils.applyAtts("\${num}", model))
        assertEquals(DataValue.create(true), TmplUtils.applyAtts("\${bool}", model))
        assertEquals(DataValue.create("str"), TmplUtils.applyAtts("\${str}", model))
        val array = DataValue.createArr()
        array.add(1)
        array.add(2)
        array.add(3)
        assertEquals(array, TmplUtils.applyAtts("\${array}", model))
        val obj = DataValue.createObj()
        obj.set("key", "value")
        assertEquals(obj, TmplUtils.applyAtts("\${obj}", model))

        assertEquals("true-123", TmplUtils.applyAtts("\${bool}-\${num}", model).asText())
        assertEquals(DataValue.createStr("true"), TmplUtils.applyAtts("\${bool}\${}", model))

        val res = TmplUtils.applyAtts(
            ObjectData.create(
                """{
            "inner": {"inner": {"value": "${"\${array}"}"}}
        }
                """.trimIndent()
            ),
            model
        )!!.get("/inner/inner/value")
        assertEquals(array, res)
    }

    @Test
    fun testWithInnerBraces() {

        val att0 = "\$att{?str|presuf('arg')}"
        val template0 = "constant-prefix-\${$att0}"
        val atts0 = TmplUtils.getAtts(template0)
        assertThat(atts0).hasSize(1)
        assertThat(atts0.first()).isEqualTo(att0)

        val att1 = "\$att{inner{?str|presuf('arg')}}"
        val template1 = "constant-prefix-\${$att1}"
        val atts1 = TmplUtils.getAtts(template1)
        assertThat(atts1).hasSize(1)
        assertThat(atts1.first()).isEqualTo(att1)

        val template2 = "constant-prefix-\${$att0}-\${$att1}"
        val atts2 = TmplUtils.getAtts(template2, LinkedHashSet()).toList()
        assertThat(atts2).hasSize(2)
        assertThat(atts2[0]).isEqualTo(att0)
        assertThat(atts2[1]).isEqualTo(att1)

        val model = ObjectData.create(
            mapOf(
                att0 to "att0-value",
                att1 to "att1-value"
            )
        )

        val result0 = TmplUtils.applyAtts(template0, model).asText()
        assertThat(result0).isEqualTo("constant-prefix-att0-value")

        val result1 = TmplUtils.applyAtts(template1, model).asText()
        assertThat(result1).isEqualTo("constant-prefix-att1-value")

        val result2 = TmplUtils.applyAtts(template2, model).asText()
        assertThat(result2).isEqualTo("constant-prefix-att0-value-att1-value")
    }

    @Test
    fun test() {

        assertEquals(hashSetOf("abc"), TmplUtils.getAtts("\${abc}"))

        val template = "\${abc}----\${def}"
        assertEquals(hashSetOf("abc", "def"), TmplUtils.getAtts(template))
        assertEquals(
            DataValue.createStr("test0----test1"),
            TmplUtils.applyAtts(
                template,
                ObjectData.create(
                    """
            {
                "abc": "test0",
                "def": "test1"
            }""".trimMargin()
                )
            )
        )
        assertEquals(
            DataValue.createStr("test0----"),
            TmplUtils.applyAtts(
                template,
                ObjectData.create(
                    """
            {
                "abc": "test0"
            }""".trimMargin()
                )
            )
        )

        val mlTest = MLText(
            Locale("ru") to "Договор №\${_docNum}",
            Locale.ENGLISH to "Contract №\${_docNum}"
        )

        assertEquals(hashSetOf("_docNum"), TmplUtils.getAtts(mlTest))

        val expectedMlTest = MLText(
            Locale("ru") to "Договор №10",
            Locale.ENGLISH to "Contract №10"
        )

        assertEquals(expectedMlTest, TmplUtils.applyAtts(mlTest, ObjectData.create("{\"_docNum\": 10}")))

        val list = arrayListOf("one \${test0}", "two \${test1}")
        assertEquals(hashSetOf("test0", "test1"), TmplUtils.getAtts(list))

        val set = hashSetOf("one \${test0}", "two \${test1}")
        assertEquals(hashSetOf("test0", "test1"), TmplUtils.getAtts(set))

        val complex = hashMapOf(
            "key0" to arrayListOf("abc \${test0}", "def \${test1}"),
            "key1" to "aaaa \${test2}"
        )
        assertEquals(hashSetOf("test0", "test1", "test2"), TmplUtils.getAtts(complex))
    }

    data class TestDto(
        val first: String,
        val second: String,
        val inner: DtoInner
    )

    data class DtoInner(
        val innerValue: String
    )
}
