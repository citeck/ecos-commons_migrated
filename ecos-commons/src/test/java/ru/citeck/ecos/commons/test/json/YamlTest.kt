package ru.citeck.ecos.commons.test.json

import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.io.file.EcosFile
import ru.citeck.ecos.commons.io.file.mem.EcosMemDir
import ru.citeck.ecos.commons.json.FileResolver
import ru.citeck.ecos.commons.json.Json
import ru.citeck.ecos.commons.json.YamlUtils
import ru.citeck.ecos.commons.json.serialization.annotation.IncludeNonDefault
import kotlin.test.assertEquals

class YamlTest {

    private val importStr = "someImportString\nandNewLine\n"
    private val importStr2 = importStr + "2\n"

    @Test
    fun test() {

        val data = """
            ---
            ___header___:
                anchorValue: &anchor
                    anchorStr: anchorStr
                    anchorTrue: true
                    anchorNum: 5
                anchorValue2: &anchor2
                    anchorNum: 10
                anchorWithInclude: {} &anchorWithInclude #include resolver:resolver2.yaml
                strAnchorWithInclude: '' &strAnchorWithInclude #include resolver:importStr2.js
            anchorField: *anchor
            anchorField2:
                <<: *anchor
                anchorStr: anchorStr2
            anchorField3:
                <<: [*anchor, *anchor2]

            field: &test '' #import

            strValue: "value"
            num10: 10
            boolTrue: true
            inner:
                strAaa: aaa
                num20: 20
            importStr: '' #include import.txt
            strList:
                - strElem0
                - strElem1
            inner2: {} #include resolver:resolver.yaml

            fieldForAnchorWithInclude: *anchorWithInclude
            strFieldForAnchorWithInclude: *strAnchorWithInclude

        """.trimIndent()

        val dir = EcosMemDir()
        dir.createFile("import.txt", importStr)
        dir.createDir("resolver").createFile(
            "resolver.yaml",
            """
            inner2Str: inner2Str
            inner2Num: 2
            """.trimIndent()
        )
        dir.getDir("resolver")!!.createFile(
            "resolver2.yaml",
            """
            inner2Str: inner2Str2
            inner2Num: 5
            """.trimIndent()
        )
        dir.getDir("resolver")!!.createFile("importStr2.js", importStr2)

        Json.context.setFileResolver(object : FileResolver {
            override fun getFile(path: String): EcosFile? {
                return dir.getFile("resolver/${path.replace("resolver:", "")}")
            }
        })

        checkDto(Json.mapper.read(dir.createFile("test.yaml", data), TestData::class.java)!!)
        checkDto(Json.mapper.read(dir.createFile("test.yml", data), TestData::class.java)!!)

        val yamlFromString = Json.mapper.read(
            """
            ---
            testKey: key
            testInner:
                - aaa
                - bbb
            """.trimIndent(),
            DataValue::class.java
        )

        assertEquals(DataValue.create("{\"testKey\":\"key\", \"testInner\": [\"aaa\", \"bbb\"]}"), yamlFromString)
    }

    private fun checkDto(data: TestData) {
        assertEquals(
            TestData(
                strValue = "value",
                boolTrue = true,
                num10 = 10,
                strList = listOf("strElem0", "strElem1"),
                inner = InnerData(
                    strAaa = "aaa",
                    num20 = 20
                ),
                importStr = importStr,
                inner2 = Inner2Data(
                    inner2Str = "inner2Str",
                    inner2Num = 2
                ),
                anchorField = AnchorData(
                    anchorStr = "anchorStr",
                    anchorTrue = true,
                    anchorNum = 5
                ),
                anchorField2 = AnchorData(
                    anchorStr = "anchorStr2",
                    anchorTrue = true,
                    anchorNum = 5
                ),
                anchorField3 = AnchorData(
                    anchorStr = "anchorStr",
                    anchorTrue = true,
                    anchorNum = 10
                ),
                fieldForAnchorWithInclude = Inner2Data(
                    inner2Str = "inner2Str2",
                    inner2Num = 5
                ),
                strFieldForAnchorWithInclude = importStr2
            ),
            data
        )
    }

    @Test
    fun dumpTest() {

        val dumpTestData = DumpTestData(
            "aa",
            123,
            listOf(Point(1.0, 2.0), Point(3.0, 4.0), Point(5.0, 6.0)),
            DumpTestData(
                "bb",
                456,
                listOf(Point(1.0, 2.0), Point(y = 4.0), Point(5.0, 6.0)),
                DumpTestData("", 12356)
            ),
            listOf(
                DumpTestData(
                    "bb",
                    456,
                    listOf(Point(1.0, 2.0), Point(y = 4.0), Point(5.0, 6.0)),
                    DumpTestData("", 12356)
                ),
                DumpTestData(
                    "bb",
                    456,
                    listOf(Point(1.0, 2.0), Point(y = 4.0), Point(5.0, 6.0)),
                    DumpTestData("", 12356)
                )
            )
        )

        val str = YamlUtils.toNonDefaultString(dumpTestData)

        // println(str)

        val dataFromStr = Json.mapper.read(str, DumpTestData::class.java)

        assertEquals(dumpTestData, dataFromStr)

/*        val test2 = """
            ---
            id: download-base64-data-yml
            name: {ru: Скачать, en: Download}
            type: download
            config: {downloadType: base64, extension: yml}
        """.trimIndent()

        val test22 = Json.mapper.read(test2, ObjectData::class.java)!!
        println(YamlUtils.toNonDefaultString(test22))*/
    }

    @Test
    fun dataUriTest() {

        val jsonWithUri = """
            {
              "storage" : "base64",
              "name" : "contracts-supplementary-agreement-684b366e-80d3-47b0-a708-802ee24b499c.yml",
              "url" : "data:application/x-yaml;base64,CmlkOiBjb250cmFjdHMtc3VwcGxlbWVudGFyeS1hZ3JlZW1lbnQKbGFiZWw6IHsgcnU6INCU0L7Qvy4g0YHQvtCz0LvQsNGI0LXQvdC40Y8sIGVuOiBBZGQnbCBhZ3JlZW1lbnRzIH0KCmFjdGlvbnM6CiAgLSB1aXNlcnYvYWN0aW9uQGVkaXQKICAtIHVpc2Vydi9hY3Rpb25Admlldy1kYXNoYm9hcmQKICAtIHVpc2Vydi9hY3Rpb25Admlldy1kYXNoYm9hcmQtaW4tYmFja2dyb3VuZAoKcHJlZGljYXRlOgogIHQ6IGFuZAogIHZhbDoKICAgIC0geyB0OiBlcSwgYXR0OiBfdHlwZSwgdmFsOiAnJ30KICAgIC0geyB0OiBlcSwgYXR0OiBUWVBFLCB2YWw6IGNvbnRyYWN0czpzdXBwbGVtZW50YXJ5QWdyZWVtZW50IH0KCmNvbHVtbnM6CgogIC0gbmFtZTogY206Y3JlYXRlZAogIC0gbmFtZTogY206dGl0bGUKICAtIG5hbWU6IGNvbnRyYWN0czptYWluQWdyZWVtZW50CiAgLSBuYW1lOiBjb250cmFjdHM6YWdyZWVtZW50TnVtYmVyCiAgLSBuYW1lOiBpY2FzZTpjYXNlU3RhdHVzQXNzb2MKICAtIG5hbWU6IGNvbnRyYWN0czphZ3JlZW1lbnRMZWdhbEVudGl0eQogIC0gbmFtZTogY29udHJhY3RzOmNvbnRyYWN0b3IKICAtIG5hbWU6IGNvbnRyYWN0czphZ3JlZW1lbnRTdWJqZWN0CiAgLSBuYW1lOiBjb250cmFjdHM6YWdyZWVtZW50QW1vdW50CiAgLSBuYW1lOiBjb250cmFjdHM6YWdyZWVtZW50Q3VycmVuY3kKICAtIG5hbWU6IGNvbnRyYWN0czphZ3JlZW1lbnREYXRlCiAgLSBuYW1lOiBjb250cmFjdHM6ZHVyYXRpb24KCiAgLSBuYW1lOiBjbTpuYW1lCiAgICB2aXNpYmxlOiBmYWxzZQogIC0gbmFtZTogaWRvY3M6cmVnaXN0cmF0aW9uTnVtYmVyCiAgICB2aXNpYmxlOiBmYWxzZQogIC0gbmFtZTogaWRvY3M6cmVnaXN0cmF0aW9uRGF0ZQogICAgdmlzaWJsZTogZmFsc2UKICAtIG5hbWU6IGlkb2NzOnN1bW1hcnkKICAgIHZpc2libGU6IGZhbHNlCiAgLSBuYW1lOiBpZG9jczpwYWdlc051bWJlcgogICAgdmlzaWJsZTogZmFsc2UKICAtIG5hbWU6IGlkb2NzOmFwcGVuZGl4UGFnZXNOdW1iZXIKICAgIHZpc2libGU6IGZhbHNlCiAgLSBuYW1lOiBpZG9jczpzaWduYXRvcnkKICAgIHZpc2libGU6IGZhbHNlCiAgLSBuYW1lOiBpZG9jczpwZXJmb3JtZXIKICAgIHZpc2libGU6IGZhbHNlCiAgLSBuYW1lOiBpZG9jczpub3RlCiAgICB2aXNpYmxlOiBmYWxzZQo=",
              "size" : 1166,
              "type" : "application/x-yaml",
              "originalName" : "contracts-supplementary-agreement.yml"
            }
        """.trimIndent()

        val data = ObjectData.create(jsonWithUri)

        val urlDataFromJson = data.getData().asJson().get("url").asText()
        val urlDataFromObj = data.get("url", "")

        assertEquals(urlDataFromJson, urlDataFromObj)
    }

    @IncludeNonDefault
    data class DumpTestData(
        val field0: String = "",
        val field1: Int = 0,
        val field2: List<Point> = emptyList(),
        val inner: DumpTestData? = null,
        val innerList: List<DumpTestData> = emptyList()
    )

    @IncludeNonDefault
    data class Point(val x: Double = 0.0, val y: Double = 0.0)

    data class TestData(
        val strValue: String,
        val boolTrue: Boolean,
        val num10: Int,
        val inner: InnerData,
        val strList: List<String>,
        val importStr: String,
        val inner2: Inner2Data,
        val anchorField: AnchorData,
        val anchorField2: AnchorData,
        val anchorField3: AnchorData,
        val fieldForAnchorWithInclude: Inner2Data,
        val strFieldForAnchorWithInclude: String
    )

    data class AnchorData(
        val anchorStr: String,
        val anchorTrue: Boolean,
        val anchorNum: Int
    )

    data class InnerData(
        val strAaa: String,
        val num20: Int
    )

    data class Inner2Data(
        val inner2Str: String,
        val inner2Num: Int
    )
}
