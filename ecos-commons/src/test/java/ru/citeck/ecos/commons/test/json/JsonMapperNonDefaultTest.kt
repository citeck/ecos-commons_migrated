package ru.citeck.ecos.commons.test.json

import ecos.com.fasterxml.jackson210.databind.annotation.JsonDeserialize
import org.junit.Test
import ru.citeck.ecos.commons.data.ObjectData
import ru.citeck.ecos.commons.json.Json
import kotlin.test.assertEquals

class JsonMapperNonDefaultTest {

    @Test
    fun toNonDefaultJsonTest() {

        // simple

        val simple = SimpleClass()
        val simpleRes = Json.mapper.toNonDefaultJson(simple)
        assertEquals(ObjectData.create().getData().asJson(), simpleRes)

        val simple2 = SimpleClass(
            field1 = 999
        )
        val simple2Res = Json.mapper.toNonDefaultJson(simple2)
        assertEquals(
            ObjectData.create(
                """{
                    "field1": 999
                 }
                """.trimIndent()
            ).getData().asJson(),
            simple2Res
        )

        val simple3 = SimpleClass(
            field0 = "abc"
        )
        val simple3Res = Json.mapper.toNonDefaultJson(simple3)
        assertEquals(
            ObjectData.create(
                """{
                    "field0": "abc"
                }
                """.trimIndent()
            ).getData().asJson(),
            simple3Res
        )

        // with builder

        val withBuilder = ClassWithBuilder.Builder().build()
        val withBuilderRes = Json.mapper.toNonDefaultJson(withBuilder)
        assertEquals(ObjectData.create().getData().asJson(), withBuilderRes)

        val withBuilder2 = ClassWithBuilder.Builder()
            .withField1(999)
            .build()

        val withBuilder2Res = Json.mapper.toNonDefaultJson(withBuilder2)
        assertEquals(
            ObjectData.create(
                """{ "field1": 999 }""".trimIndent()
            ).getData().asJson(),
            withBuilder2Res
        )

        val withBuilder3 = ClassWithBuilder.Builder()
            .withField0("abc")
            .build()

        val withBuilder3Res = Json.mapper.toNonDefaultJson(withBuilder3)
        assertEquals(
            ObjectData.create(
                """{"field0": "abc"}""".trimIndent()
            ).getData().asJson(),
            withBuilder3Res
        )

        val withBuilder4 = ClassWithBuilder.Builder()
            .withField0("abc")
            .withEnumField(EnumType.TEST)
            .withInner(
                CustomInner(
                    text = "111"
                )
            )
            .build()

        val withBuilder4Res = Json.mapper.toNonDefaultJson(withBuilder4)
        assertEquals(
            ObjectData.create(
                """{
            "field0": "abc",
            "enumField": "TEST",
            "inner": {
                "text": "111",
                "enum": "NONE"
            }
        }
                """.trimIndent()
            ).getData().asJson(),
            withBuilder4Res
        )
    }

    class SimpleClass(
        val field0: String = "test",
        val field1: Int = 1,
        val inner: CustomInner? = null
    )

    @JsonDeserialize(builder = ClassWithBuilder.Builder::class)
    class ClassWithBuilder(
        val field0: String,
        val field1: Int,
        val enumField: EnumType,
        val inner: CustomInner
    ) {

        class Builder {

            var field0: String = "field0-default"
            var field1: Int = 123
            var enumField: EnumType = EnumType.NONE
            var inner: CustomInner = CustomInner()

            fun withField0(field0: String): Builder {
                this.field0 = field0
                return this
            }

            fun withField1(field1: Int): Builder {
                this.field1 = field1
                return this
            }

            fun withEnumField(enumField: EnumType): Builder {
                this.enumField = enumField
                return this
            }

            fun withInner(inner: CustomInner): Builder {
                this.inner = inner
                return this
            }

            fun build(): ClassWithBuilder {
                return ClassWithBuilder(
                    field0, field1, enumField, inner
                )
            }
        }
    }

    class CustomInner(
        val text: String = "DEFAULT",
        val enum: EnumType = EnumType.NONE
    )

    enum class EnumType {
        NONE, TEST
    }
}
