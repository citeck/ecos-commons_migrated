package ru.citeck.ecos.commons.test.script

import org.junit.jupiter.api.Test
import ru.citeck.ecos.commons.data.DataValue
import ru.citeck.ecos.commons.utils.ScriptUtils
import kotlin.test.assertEquals

class ScriptUtilsTest {

    @Test
    fun test() {

        val strRes = ScriptUtils.eval("return \"abc\";")
        assertEquals("abc", strRes.asText())

        val intRes = ScriptUtils.eval("return 0;")
        assertEquals(0, intRes.asInt())

        val doubleRes = ScriptUtils.eval("return 0.0;")
        assertEquals(0.0, doubleRes.asDouble())

        val boolRes = ScriptUtils.eval("return true;")
        assertEquals(true, boolRes.asBoolean())

        val objRes = ScriptUtils.eval("return {\"aa\":\"bb\"};")
        assertEquals(DataValue.create("{\"aa\":\"bb\"}"), objRes)

        val arrRes = ScriptUtils.eval("return [\"aa\",\"bb\"];")
        assertEquals(DataValue.create("[\"aa\",\"bb\"]"), arrRes)
        val arrRes1 = ScriptUtils.eval("[\"aa\",\"bb\"]")
        assertEquals(DataValue.create("[\"aa\",\"bb\"]"), arrRes1)

        val mlObjRes = ScriptUtils.eval(
            """
            var someVar = "someValue";
            var result = {};
            result["ru"] = 'Русский';
            result["en"] = 'English';
            return result;
            """.trimIndent()
        )

        assertEquals(DataValue.create("{\"ru\":\"Русский\", \"en\":\"English\"}"), mlObjRes)

        val complexObj = ScriptUtils.eval(
            """
            var someVar = "someValue";
            var result = {};
            result["ru"] = 'Русский';
            result["en"] = 'English';
            result.innerArray = ['abc', 'def'];
            result.int = 10;
            result.float = 10.0;
            result.str = "str";
            result.obj = {"aa": "bb"};
            return result;
            """.trimIndent()
        )

        assertEquals(
            DataValue.create(
                """
            {
                "ru": "Русский",
                "en": "English",
                "int": 10,
                "float": 10.0,
                "str": "str",
                "obj": {"aa": "bb"},
                "innerArray": ["abc", "def"]
            }
                """.trimIndent()
            ),
            complexObj
        )

        ScriptUtils.eval("log.warn('one two three');")

        assertEquals(
            DataValue.create("[\"abc-post\",\"def-post\"]"),
            ScriptUtils.eval("return test.getArray().map(function(it) {return it + '-post';});", mapOf(Pair("test", TestClassInScript())))
        )

        assertEquals(
            DataValue.create("[\"abc\",\"jkl\"]"),
            ScriptUtils.eval(
                """

                var first = test['mapOfLists']['first'];
                var second = test['mapOfLists']['second'];

                log.info("first list: " + first);
                log.info("second list: " + first);

                return [first[0], second[1]];

                """.trimIndent(),
                mapOf(Pair("test", TestClassInScript()))
            )
        )
    }

    @Test
    fun convertToScriptTest() {
        val value = ScriptUtils.convertToScript(DataValue.createStr("text"))
        assertEquals("text", value)
    }

    class TestClassInScript {

        fun getArray(): Any? {
            return ScriptUtils.convertToScript(listOf("abc", "def"))
        }

        fun getMapOfLists(): Any? {
            return ScriptUtils.convertToScript(
                mapOf(
                    Pair("first", listOf("abc", "def")),
                    Pair("second", listOf("ghi", "jkl"))
                )
            )
        }
    }
}
